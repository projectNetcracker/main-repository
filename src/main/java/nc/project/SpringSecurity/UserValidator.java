package nc.project.SpringSecurity;


import nc.project.Hibernate.model.User;
import nc.project.Hibernate.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Pattern;

/**
 * Created by 1 on 06.11.2016.
 */
@Component
public class UserValidator implements Validator {
    private static Log logger = LogFactory.getLog(UserValidator.class);

    private static final String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";



    @Autowired
    private UserService userService;

    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    public void validate(Object o, Errors errors) {
        User userRegist = (User) o;
        Pattern pattern = Pattern.compile(regex);

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "Required");
        if (userRegist.getLogin().length() < 4)
            errors.rejectValue("login", "Size.userForm.login");

        if (userService.getUserByLogin(userRegist.getLogin()) != null)
            errors.rejectValue("login", "Duplicate.userForm.login");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "pass", "Required");

        logger.error("one"+ userRegist.getConfirmPassword());
        logger.error("one"+ userRegist.getPass());
        logger.error("one"+ userRegist.getFirstname());

        if (!userRegist.getConfirmPassword().equals(userRegist.getPass())) {
            errors.rejectValue("confirmPassword", "Different.userForm.password");
        }

        if (userRegist.getPass().length() < 4 || userRegist.getPass().length() > 100) {
            errors.rejectValue("pass", "Size.userForm.password");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mail", "Required");
        if (!pattern.matcher(userRegist.getMail()).matches()){
            errors.rejectValue("mail", "incorrect.usertForm.email");
        }
    }


}
