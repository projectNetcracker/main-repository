package nc.project.SpringSecurity;

import nc.project.Hibernate.DAO.UserDao;
import nc.project.Hibernate.model.User;
import nc.project.Hibernate.service.UserService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * Created by 1 on 05.11.2016.
 */

@Service
@Transactional(readOnly=true)
public class CustomUserDetailsService implements UserDetailsService {
    final static Logger logger = Logger.getLogger(CustomUserDetailsService.class);

    @Autowired
    private UserService userService;

    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        logger.error("userAdmin start "+login); // Log
        User domainUser = userService.getUserByLogin(login);
        logger.error("userAdmin: "+domainUser); // Log
        boolean enabled = true;
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean accountNonLocked = true;

        return new org.springframework.security.core.userdetails.User(domainUser.getLogin(), String.valueOf(domainUser.getPass()), enabled, accountNonExpired,
                credentialsNonExpired, accountNonLocked, getAuthorities(domainUser.getId_role()));
    }

    public Collection<? extends GrantedAuthority> getAuthorities(Integer roleId) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(roleId));
        return authList;
    }

    public List<String> getRoles(Integer roleId) {

        List<String> roles = new ArrayList<String>();

        if (roleId == 1) {
            roles.add("ROLE_USER");
        } else if (roleId == 2) {
            roles.add("ROLE_ADMIN");
            roles.add("ROLE_USER");
        }
        return roles;
    }

    public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }
}
