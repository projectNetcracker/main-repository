package nc.project.SpringSecurity;

import nc.project.Hibernate.model.Course;
import nc.project.Hibernate.model.Course;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Created by 1 on 25.11.2016.
 */
@Component
public class CourseValidator implements Validator{
    public boolean supports(Class<?> aClass) {
        return Course.class.equals(aClass);
    }

    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"description","Required");
    }
}
