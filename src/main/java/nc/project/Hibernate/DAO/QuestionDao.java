package nc.project.Hibernate.DAO;

import nc.project.Hibernate.model.Question;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by 1 on 20.11.2016.
 */
@Repository
public interface QuestionDao extends CrudRepository<Question, Integer> {
    List<Question> findByIdChapterCourse(Integer idChapterCourse);
}
