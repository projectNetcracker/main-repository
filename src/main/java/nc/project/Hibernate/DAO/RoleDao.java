package nc.project.Hibernate.DAO;
import nc.project.Hibernate.model.Role;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by 1 on 05.11.2016.
 */
@Repository
public interface RoleDao extends CrudRepository<Role, Integer> {
}
