package nc.project.Hibernate.DAO;

import nc.project.Hibernate.model.Answer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by 1 on 23.11.2016.
 */
@Repository
public interface AnswerDao extends CrudRepository<Answer, Integer> {
}
