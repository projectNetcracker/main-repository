package nc.project.Hibernate.DAO;

import nc.project.Hibernate.model.Submission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author eugene
 */
@Repository
public interface SubmissionDao extends CrudRepository<Submission, Integer> {
    List<Submission> findByIdAttempt(Integer idAttempt);
    List<Submission> findByIdStepic(Integer idStepic);
}
