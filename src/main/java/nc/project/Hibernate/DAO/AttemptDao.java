package nc.project.Hibernate.DAO;

import nc.project.Hibernate.model.Attempt;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author eugene
 */
@Repository
public interface AttemptDao extends CrudRepository<Attempt, Integer> {
    List<Attempt> findByIdStep(Integer idStep);
    List<Attempt> findByIdStepic(Integer idStepic);
    List<Attempt> findByIdUser(Integer idUSer);
}
