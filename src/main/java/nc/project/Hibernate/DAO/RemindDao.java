package nc.project.Hibernate.DAO;

import nc.project.Hibernate.model.Remind;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Created by 1 on 29.11.2016.
 */
@Repository
public interface RemindDao extends CrudRepository<Remind, Integer> {
    List<Remind> findByIdUser(Integer idUser);
    Remind findByIdLessonAndIdUser(Integer idLesson, Integer idUser);
}
