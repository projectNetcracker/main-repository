package nc.project.Hibernate.DAO;

import nc.project.Hibernate.model.UserLesson;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by 1 on 10.12.2016.
 */
public interface UserLessonDao extends CrudRepository<UserLesson, Integer> {
    List<UserLesson> findByIdChapterCourseAndIdUser (Integer idLesson, Integer idUser);
    List<UserLesson> findByIdChapterCourse(Integer idLesson);
    List<UserLesson> findByIdUser(Integer idUser);
}
