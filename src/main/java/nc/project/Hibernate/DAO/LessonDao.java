package nc.project.Hibernate.DAO;

import nc.project.Hibernate.model.Lesson;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LessonDao extends CrudRepository<Lesson, Integer> {
    List<Lesson> findByIdCourse(Integer idCourse);
    List<Lesson> findByIdCourseOrderByIdAsc(Integer idCourse);
    List<Lesson> findByIdLessonStepic(Integer idStepic);
}
