package nc.project.Hibernate.DAO;

import nc.project.Hibernate.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by 1 on 06.11.2016.
 */
@Repository
public interface UserDao extends JpaRepository<User, Integer> {
    List<User> findUserByLogin(String login);
    List<User> findAll();
}
