package nc.project.Hibernate.DAO;

import nc.project.Hibernate.model.Course;
import nc.project.Hibernate.model.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

/**
 * Created by 1 on 15.11.2016.
 */
@Repository
public interface CourseDao extends JpaRepository<Course, Integer> {
    List<Course> findByIdStepic(Integer idStepic);
    List<Course> findAll();
    List<Course> findByIdStepicIsNull();
//    List<Course> findIdByOrderByIdAsc();      // Почему то не работает
}
