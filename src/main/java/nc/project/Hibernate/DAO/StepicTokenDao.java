package nc.project.Hibernate.DAO;

import nc.project.Hibernate.model.StepicToken;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author eugene
 */
@Repository
public interface StepicTokenDao extends CrudRepository<StepicToken, Integer> {
    List<StepicToken> findByIdUser(Integer idUser);
}
