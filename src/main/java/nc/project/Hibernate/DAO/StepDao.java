package nc.project.Hibernate.DAO;

import nc.project.Hibernate.model.Step;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author eugene
 */
@Repository
public interface StepDao extends CrudRepository<Step, Integer> {
    List<Step> findByIdStepic(Integer idStepic);
    List<Step> findByIdLesson(Integer idLesson);
}
