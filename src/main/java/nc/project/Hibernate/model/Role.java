package nc.project.Hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

/**
 * Created by 1 on 03.11.2016.
 */
@Entity
@Getter
@Setter
@Table (name = "role", catalog = "Course")
public class Role {


    @Id
    @Column(name = "id")
    @GeneratedValue (strategy = GenerationType.IDENTITY )
    private Integer id;

    @Column(name = "role")
    private String role;

    @OneToMany (mappedBy = "role",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<User> userSet;

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", role='" + role + '\'' +
                '}';
    }

    @Override
    public int hashCode() {
        return id * 13 + role.hashCode();
    }
}
