package nc.project.Hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by 1 on 25.10.2016.
 */
@Entity
@Getter
@Setter
@Table(name = "users", catalog = "Course")
public class User {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "id_user_stepic")
    private Integer idUserStepic;

    @Column (name = "login")
    private String login;

    @Column (name = "firstname")
    private String firstname;

    @Column (name = "secondname")
    private String secondname;

    @Column (name = "mail")
    private String mail;

    @Column  (name = "pass")
    private String pass;

    @Column (name = "id_role")
    private Integer id_role;

    @Transient
    private String confirmPassword;

    @ManyToOne
    @JoinColumn(name = "id_role", referencedColumnName ="id" ,insertable=false, updatable=false)
    private Role role;

    @OneToMany(mappedBy = "user",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<StepicResultQuestion> stepicResultQuestions;

    @OneToMany (mappedBy = "user",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Remind> remindList;

    @ManyToMany(fetch = FetchType.LAZY, cascade =
            CascadeType.ALL)
    @JoinTable(name = "user_course", catalog = "Course", joinColumns = {
            @JoinColumn(name = "id_user", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "id_chapter_course",
                    nullable = false, updatable = false) })
    private List<Lesson> lessons;
}
