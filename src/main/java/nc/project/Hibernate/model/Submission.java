package nc.project.Hibernate.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

/**
 * @author eugene
 */
@Entity
@Getter
@Setter
@Table(name = "submission", catalog = "Course")
public class Submission {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "id_attempt")
    private Integer idAttempt;

    @Column(name = "id_stepic")
    private Integer idStepic;

    @Column(name = "is_correct")
    private String isCorrect;

    @Column(name = "timestamp")
    private Date timestamp;

    @ManyToOne
    @JoinColumn(name = "id_attempt", referencedColumnName ="id" ,insertable=false, updatable=false)
    private Attempt attempt;
}
