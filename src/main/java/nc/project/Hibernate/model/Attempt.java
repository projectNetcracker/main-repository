package nc.project.Hibernate.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;

/**
 * @author eugene
 */
@Entity
@Getter
@Setter
@Table(name = "attempt", catalog = "Course")
public class Attempt {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    @Column (name = "id_stepic")
    private Integer idStepic;

    @Column (name = "id_step")
    private Integer idStep;

    @Column (name = "id_user")
    private Integer idUser;

    @ManyToOne
    @JoinColumn(name = "id_step",referencedColumnName = "id",insertable=false, updatable=false)
    private Step step;

}
