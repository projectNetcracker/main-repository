package nc.project.Hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 1 on 27.11.2016.
 */
@Entity
@Getter
@Setter
@Table(name = "stepic_token", catalog = "Course")
public class StepicToken {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "id_user")
    private Integer idUser;

    @Column(name = "access_token")
    private String accessToken;

    @Column(name = "refresh_token")
    private String refreshToken;

    @Column(name = "get_time")
    private Date getTime;

    @Column(name = "expired_time")
    private Date expiredTime;

    @Column(name = "scope")
    private String scope;

    @Column(name = "token_type")
    private String tokenType;

    @Column(name = "raw_response")
    private String rawResponse;
/*
    @OneToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "id_user", referencedColumnName ="id" ,insertable=false, updatable=false)
    private User user;*/
}
