package nc.project.Hibernate.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by 1 on 06.11.2016.
 */
@Entity
@Getter
@Setter
@Table(name = "answer", catalog = "Course")
public class Answer {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column(name = "answer")
    private String answer;

    @Column(name = "rights", nullable = true)
    private String right;

    @Column(name = "id_question_chapter")
    private Integer idQuestionChapter;

    @Column(name = "is_select")
    private Integer isSelect;

    @ManyToOne
    @JoinColumn(name = "id_question_chapter",referencedColumnName = "id",insertable=false, updatable=false, nullable = true)
    private Question question;

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (answer != null ? answer.hashCode() : 0);
        result = 31 * result + (right != null ? right.hashCode() : 0);
        result = 31 * result + idQuestionChapter.hashCode();
        result = 31 * result + (isSelect != null ? isSelect.hashCode() : 0);
        result = 31 * result + (question != null ? question.hashCode() : 0);
        return result;
    }

}
