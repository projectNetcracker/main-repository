package nc.project.Hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

/**
 * Created by 1 on 06.11.2016.
 */
@Entity
@Getter
@Setter
@Table(name = "questions_chapter", catalog = "Course")
public class Question {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "description")
    private String description;

    @Column (name = "id_chapter_course")
    private Integer idChapterCourse;

    @ManyToOne
    @JoinColumn(name = "id_chapter_course",referencedColumnName = "id",insertable=false, updatable=false)
    private Lesson lesson;

    @OneToMany (mappedBy = "question",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List <Answer> answerList;


}
