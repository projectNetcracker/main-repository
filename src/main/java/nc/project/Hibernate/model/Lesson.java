package nc.project.Hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.json.JSONObject;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by 1 on 06.11.2016.
 */
@Entity
@Getter
@Setter
@Table(name = "chapter_course", catalog = "Course")
public class Lesson {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name_chapter")
    private String title;

    @Column(name = "text", nullable = true)
    private String text;

    @Column(name = "id_courses")
    private Integer idCourse;

    @Column(name = "is_stepic", nullable = true)
    private Integer isStepic;

    @Column(name = "id_lesson_stepic")
    private Integer idLessonStepic;

    @OneToMany (mappedBy = "lesson",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Question> questionList;

    @OneToMany (mappedBy = "lesson",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Set<Step> stepSet;

    @ManyToOne
    @JoinColumn(name = "id_courses", referencedColumnName ="id" ,insertable=false, updatable=false)
    private Course course;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "lessons")
    private List<User> usersList;

    @OneToMany (mappedBy = "lesson",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Remind> remindList;


    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", idCourse=" + idCourse +
                '}';
    }

    public String getExtendedTitle() {
        if (idLessonStepic == null) {
            return title;
        } else {
            JSONObject jsonObject = new JSONObject(title);
            return jsonObject.getString("sectionTitle") + ": " + jsonObject.getString("lessonTitle");
        }
    }
}
