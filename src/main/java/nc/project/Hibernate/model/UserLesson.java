package nc.project.Hibernate.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 1 on 06.11.2016.
 */
@Entity
@Getter
@Setter
@Table(name = "user_course", catalog = "Course")
public class UserLesson {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "id_user")
    private Integer idUser;

    @Column(name = "start_course")
    private Date startCourse;

    @Column(name = "finish_course")
    private Date finishCourse;

    @Column (name = "id_chapter_course")
    private Integer idChapterCourse;

    @ManyToOne
    @JoinColumn(name = "id_user", referencedColumnName ="id" ,insertable=false, updatable=false)
    private User user;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserLesson that = (UserLesson) o;

        if (!id.equals(that.id)) return false;
        if (!idUser.equals(that.idUser)) return false;
        if (startCourse != null ? !startCourse.equals(that.startCourse) : that.startCourse != null) return false;
        if (finishCourse != null ? !finishCourse.equals(that.finishCourse) : that.finishCourse != null) return false;
        return idChapterCourse.equals(that.idChapterCourse);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + idUser.hashCode();
        result = 31 * result + (startCourse != null ? startCourse.hashCode() : 0);
        result = 31 * result + (finishCourse != null ? finishCourse.hashCode() : 0);
        result = 31 * result + idChapterCourse.hashCode();
        return result;
    }
}
