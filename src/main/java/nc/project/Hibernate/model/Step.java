package nc.project.Hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by 1 on 27.11.2016.
 */
@Entity
@Getter
@Setter
@Table(name = "step", catalog = "Course")
public class Step {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "id_stepic")
    private Integer idStepic;

    @Column(name = "id_chapter")
    private Integer idLesson;

    @Column(name = "number_in_section")
    private Integer numberInSection;

    @Column(name = "description")
    private String description;

    @Column(name = "name_section")
    private String nameSection;

    @Column(name = "name_step")
    private String nameStep;

    @Column (name = "is_question")
    private String isQuestion;

    @ManyToOne
    @JoinColumn(name = "id_chapter", referencedColumnName ="id" ,insertable=false, updatable=false)
    private Lesson lesson;
}
