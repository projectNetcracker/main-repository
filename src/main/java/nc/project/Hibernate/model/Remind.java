package nc.project.Hibernate.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by 1 on 06.11.2016.
 */

@Getter
@Setter
@Entity
@Table(name = "remind", catalog = "Course")
public class Remind {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "result_test")
    private String resultTest;

    @Column(name = "data_remind")
    private Date dataRemind;

    @Column (name = "id_chapter_course")
    private Integer idLesson;

    @Column (name = "id_user")
    private Integer idUser;

    @Column (name = "is_display")
    private Integer isDisplay;

    @Column (name = "is_send_mail")
    private Integer isSendMail;

    @Column (name = "data_pass")
    private String dataPass;

    @ManyToOne
    @JoinColumn(name = "id_user", referencedColumnName = "id",insertable=false, updatable=false)
    private User user;

//    @Transient
//    private String formatDate;


    @ManyToOne
    @JoinColumn(name = "id_chapter_course",referencedColumnName = "id",insertable=false, updatable=false)
    private Lesson lesson;

}
