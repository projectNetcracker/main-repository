package nc.project.Hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Created by 1 on 06.11.2016.
 */
@Entity
@Getter
@Setter
@Table(name = "type_answer", catalog = "Course")
public class TypeAnswer {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    @Column(name = "is_writing")
    private boolean isWriting;

    @Column(name = "id_answer")
    private Integer idAnswer;
}
