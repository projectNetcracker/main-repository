package nc.project.Hibernate.model.wrapper;

import lombok.Data;
import nc.project.Hibernate.model.Lesson;
import nc.project.Hibernate.model.Course;
import nc.project.Hibernate.model.Lesson;

/**
 * Created by 1 on 15.11.2016.
 */
@Data
public class Course_ChapterCourse {
    private Course course;
    private Lesson lesson;

    @Override
    public String toString() {
        return "Course_ChapterCourse{" +
                "courses=" + course.getNameCourse() +
                ", chapterCourse=" + lesson.getTitle() +
                '}';
    }
}
