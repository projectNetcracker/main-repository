package nc.project.Hibernate.model.wrapper;

import nc.project.Hibernate.model.Answer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1 on 23.11.2016.
 */
public class AnswerListWrapper {
    private List<Answer> answerList;

    public AnswerListWrapper() {
        this.answerList = new ArrayList<Answer>();
    }
    public List<Answer> getAnswerList() {
        return answerList;
    }

    public void setAnswerList(List<Answer> answerList) {
        this.answerList = answerList;
    }

    public AnswerListWrapper addFourAnswer (){
        for (int i = 0; i<4; i++){
            answerList.add(new Answer());
        }
       return this;
    }

    private String textQuestion;

    public String getTextQuestion() {
        return textQuestion;
    }

    public void setTextQuestion(String textQuestion) {
        this.textQuestion = textQuestion;
    }

}
