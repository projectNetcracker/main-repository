package nc.project.Hibernate.model.wrapper;

import lombok.Getter;
import lombok.Setter;
import nc.project.Hibernate.model.Answer;
import nc.project.Hibernate.model.Question;

import java.util.*;

/**
 * Created by 1 on 06.12.2016.
 */
@Setter @Getter
public class QuestionListWrapper {
    private Map<Question, List<Answer>> questionMap;

   private Question question;
   private List<Answer> answerList;

//    private String answer;

    public QuestionListWrapper() {
    }

    public  QuestionListWrapper(List<Question> questionList){
        questionMap = new LinkedHashMap<>();
        for(Question question : questionList){
                questionMap.put(question,question.getAnswerList());
        }
    }
}
