package nc.project.Hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
/**
 * Created by 1 on 27.11.2016.
 */
@Entity
@Getter
@Setter
@Table(name = "stepic_result_question", catalog = "Course")
public class StepicResultQuestion {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "id_user")
    private Integer idUser;

    @Column(name = "id_step")
    private Integer idStep;

    @Column(name = "result")
    private String result;

    @Column(name = "time_step")
    private Date timeStep;

    @Column(name = "id_chapter")
    private Integer idChapter;

    @ManyToOne
    @JoinColumn(name = "id_user", referencedColumnName ="id" ,insertable=false, updatable=false)
    private User user;
}
