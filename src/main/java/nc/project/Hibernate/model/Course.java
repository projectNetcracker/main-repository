package nc.project.Hibernate.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

/**
 * Created by 1 on 06.11.2016.
 */
@Entity
@Getter
@Setter
@Table(name = "courses", catalog = "Course")
public class Course {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name_course")
    private String nameCourse;

    @Column(name = "description", nullable = true)
    private String description;

    @Column (name = "id_stepic", nullable = true)
    private Integer idStepic;

    @Column(name = "image")
    private String image;

    @OneToMany(mappedBy = "course",cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OrderBy("id")
    private List<Lesson> lessons;
}
