package nc.project.Hibernate.service;

import javafx.util.Pair;
import nc.project.Hibernate.model.Attempt;
import nc.project.Hibernate.model.Course;
import nc.project.Hibernate.model.Lesson;
import nc.project.Hibernate.model.Step;
import nc.project.Hibernate.model.Submission;
import nc.project.Hibernate.model.UserLesson;
import nc.project.integration.model.StepicAttempt;
import nc.project.integration.model.StepicCourse;
import nc.project.integration.model.StepicLesson;
import nc.project.integration.model.StepicSection;
import nc.project.integration.model.StepicStep;
import nc.project.integration.model.StepicSubmission;
import nc.project.integration.model.StepicUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author eugene
 */
@Service
public class StepicService {

    @Autowired
    CourseService courseService;

    @Autowired
    StepService stepService;

    @Autowired
    LessonService lessonService;

    @Autowired
    AttemptService attemptService;

    @Autowired
    SubmissionService submissionService;

    @Autowired
    UserCourseService userCourseService;

    @Autowired
    RemindService remindService;

    @Transactional
    public void saveCourse(StepicCourse stepicCourse) {
        if (courseService.existsCourse(stepicCourse.getId())) {
            return;
        }

        Course course = courseService.addCourse(stepicCourse.convert());
        for (StepicSection stepicSection : stepicCourse.getStepicSections()) {
            List<StepicLesson> stepicLessons = stepicSection.getUnits().stream().map(StepicUnit::getStepicLesson).collect(Collectors.toList());
            stepicLessons.forEach(stepicLesson -> saveLesson(course, stepicSection.getTitle(), stepicLesson));
        }
    }

    @Transactional
    private void saveLesson(Course course, String sectionTitle, StepicLesson stepicLesson) {
        Lesson lesson = stepicLesson.convert(sectionTitle, course.getId());
        lesson = lessonService.create(lesson);
        List<StepicStep> stepicSteps = stepicLesson.getStepicSteps();
        for (int i = 0; i < stepicSteps.size(); i++) {
            saveStep(lesson, stepicSteps.get(i), i);
        }
    }

    @Transactional
    private void saveStep(Lesson lesson, StepicStep stepicStep, int number) {
        Step step = stepicStep.convert(lesson.getTitle(), lesson.getId(), number + 1);
        stepService.create(step);
    }

    @Transactional
    public void saveAttempt(StepicAttempt stepicAttempt, Integer userId) {
        if (stepService.existsStepic(stepicAttempt.getStepId()) && !attemptService.existsStepic(stepicAttempt.getId())) {
            Attempt attempt = stepicAttempt.convert(userId, stepService.getByStepic(stepicAttempt.getStepId()).getId());
            attemptService.create(attempt);
        }
    }

    @Transactional
    public void saveSubmission(StepicSubmission stepicSubmission) {
        if (attemptService.existsStepic(stepicSubmission.getAttemptId()) && !submissionService.existsStepic(stepicSubmission.getId())) {
            Submission submission = stepicSubmission.convert(attemptService.getByStepic(stepicSubmission.getAttemptId()).getId());
            submissionService.create(submission);
        }
    }

    @Transactional
    public void refreshReminds(Integer userId) {
        List<Attempt> stepicAttempts = attemptService.getByUser(userId);
        Map<Integer, List<Attempt>> attemptMap = new HashMap<>();
        stepicAttempts.forEach(attempt -> {
            List<Attempt> attempts = attemptMap.getOrDefault(attempt.getIdStep(), new ArrayList<>());
            attempts.add(attempt);
            attemptMap.put(attempt.getIdStep(), attempts);
        });
        Map<Integer, List<Pair<Boolean, Integer>>> stepMap = new HashMap<>();
        Map<Integer, Date> lastStepSubmission = new HashMap<>();
        Set<Integer> stepSet = new HashSet<>();
        attemptMap.keySet().forEach(stepId -> {
            if (!stepSet.contains(stepId)) {
                Step step = stepService.get(stepId);
                if (step.getIsQuestion().equals("true")) {
                    List<Attempt> attempts = attemptMap.get(stepId);
                    List<Submission> submissions = new ArrayList<>();
                    boolean hasCorrect = false;
                    for (Attempt attempt : attempts) {
                        List<Submission> subm = submissionService.getByAttempt(attempt.getId());
                        boolean success = false;
                        for (Submission submission : subm) {
                            if (!lastStepSubmission.containsKey(stepId)) {
                                lastStepSubmission.put(stepId, submission.getTimestamp());
                            }
                            if (submission.getIsCorrect().equals("true")) {
                                if (success) {
                                    break;
                                }
                                success = true;
                                hasCorrect = true;
                            }
                            submissions.add(submission);
                        }
                    }
                    if (!submissions.isEmpty()) {
                        List<Pair<Boolean, Integer>> stepList = stepMap.getOrDefault(step.getIdLesson(), new ArrayList<>());
                        stepList.add(new Pair<>(hasCorrect && submissions.size() == 1, stepId));
                        stepMap.put(step.getIdLesson(), stepList);
                        stepSet.add(stepId);
                    }
                }
            }
        });
        stepMap.keySet().forEach(lessonId -> {
            if (remindService.getRemindListByUserAndLesson(userId, lessonId) == null) {
                List<Pair<Boolean, Integer>> stepList = stepMap.get(lessonId);
                List<Step> allSteps = stepService.getByLesson(lessonId).stream()
                        .filter(step -> step.getIsQuestion().equals("true"))
                        .collect(Collectors.toList());
                Date lastLessonTime = stepList.stream()
                        .map(pair -> lastStepSubmission.get(pair.getValue()))
                        .reduce(new Date(0), (d1, d2) -> d1.after(d2)? d1 : d2);
                if (stepList.size() == allSteps.size()) {
                    double score = stepList.stream().filter(pair -> pair.getKey()).count() * 100.0 / stepList.size();
                    remindService.dateRemindAndSave(score, lessonId, userId, lastLessonTime);
                }
            }
        });
    }
}
