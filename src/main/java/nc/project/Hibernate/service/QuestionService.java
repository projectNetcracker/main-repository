package nc.project.Hibernate.service;

import nc.project.Hibernate.DAO.QuestionDao;
import nc.project.Hibernate.model.Answer;
import nc.project.Hibernate.model.Question;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 1 on 20.11.2016.
 */
@Service
public class QuestionService {
    private static Log logger = LogFactory.getLog(QuestionService.class);

    @Autowired
    private QuestionDao questionDao;

    @Autowired
    private RemindService remindService;

    @Transactional
    public List questionListChapter(Integer idChapter) {
        return questionDao.findByIdChapterCourse(idChapter);
    }

    @Transactional
    public void deleteQuestion(Integer id) {
        questionDao.delete(id);
    }

    @Transactional
    public void saveQuestion(Question question) {
        questionDao.save(question);
    }

    @Transactional
    public Question getQuestionById(Integer id) {
        Question question = questionDao.findOne(id);
        question.getAnswerList().iterator();
        return question;
    }

    @Transactional
    public void updateQuestion(Integer id, String description) {
        Question question = getQuestionById(id);
        question.setDescription(description);
        questionDao.save(question);
    }

    public void detectedQuestion(HttpServletRequest request,
                                 Map<Question, List<Answer>> questionHashMap) {
        for (Map.Entry<Question, List<Answer>> entry : questionHashMap.entrySet()) {
            String idQuestion = entry.getKey().getId().toString();
            String sNumber = request.getParameter(idQuestion);
            if (sNumber != null) {
                Integer iNumber = Integer.parseInt(sNumber);
                entry.getValue().get(iNumber).setIsSelect(1);
            }
        }
       remindService.lessonEvaluation(questionHashMap);
    }

}



