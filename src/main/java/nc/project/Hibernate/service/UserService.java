package nc.project.Hibernate.service;

import nc.project.Hibernate.DAO.UserDao;
import nc.project.Hibernate.model.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1 on 31.10.2016.
 */
@Service
public class UserService {
    private static Log logger = LogFactory.getLog(UserService.class);

    @Autowired
    private UserDao userDao;

    @Transactional
    public void addUsers(User user) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(11);
        String pass = encoder.encode(user.getPass());
        user.setPass(pass);
        userDao.save(user);
    }

    @Transactional
    public List<User> listUsers() {
        List<User> result = userDao.findAll(new Sort(Sort.Direction.ASC, "login"));
//        userDao.findAllByOrderByLoginAsc().forEach(result::add);
        return result;
    }

    @Transactional
    public User getUserByLogin(String login) {
        List<User> users = userDao.findUserByLogin(login);
        if (users.isEmpty()) {
            return null;
        } else {
            return users.get(0);
        }
    }

    @Transactional
    public User getUserByID(Integer id) {
        return userDao.findOne(id);
    }

    @Transactional
    public void updateUser(User user) {
        userDao.save(user);
    }

    @Transactional
    public void deleteUser(Integer id) {
        userDao.delete(id);
    }


}
