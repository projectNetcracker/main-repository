package nc.project.Hibernate.service;



import nc.project.Hibernate.DAO.RoleDao;
import nc.project.Hibernate.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by 1 on 05.11.2016.
 */
@Service
public class RoleService {
    @Autowired
    private RoleDao roleDao;

    @Transactional
    public Role getRole(Integer id){
        return roleDao.findOne(id);
    }
}
