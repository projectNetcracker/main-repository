package nc.project.Hibernate.service;

import nc.project.Hibernate.DAO.StepDao;
import nc.project.Hibernate.model.Step;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author eugene
 */
@Service
public class StepService {
    @Autowired
    StepDao stepDao;

    @Transactional
    public Step create(Step step) {
        return stepDao.save(step);
    }

    @Transactional
    public Step getByStepic(Integer idStepic) {
        List<Step> steps = stepDao.findByIdStepic(idStepic);
        if (steps.isEmpty()) {
            return null;
        } else {
            return steps.get(0);
        }
    }

    @Transactional
    public boolean existsStepic(Integer idStepic) {
        return !stepDao.findByIdStepic(idStepic).isEmpty();
    }

    @Transactional
    public Step get(Integer id) {
        return stepDao.findOne(id);
    }

    @Transactional
    public List<Step> getByLesson(Integer idLesson) {
        return stepDao.findByIdLesson(idLesson);
    }

}
