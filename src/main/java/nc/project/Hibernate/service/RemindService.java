package nc.project.Hibernate.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import nc.project.Hibernate.DAO.RemindDao;
import nc.project.Hibernate.model.*;
;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.remoting.rmi.RmiBasedExporter;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by 1 on 29.11.2016.
 */
@Service
public class RemindService {
    private static Log logger = LogFactory.getLog(RemindService.class);

    @Autowired
    private RemindDao remindDao;

    @Autowired
    private SendMail sendMail;

    @Autowired
    private UserService usersService;

    @Autowired
    private UserCourseService userCourseService;

    @Data
    @AllArgsConstructor
    public static class ExtendedRemind {
        private Remind remind;
        private String lessonTitle;
    }

    @Transactional
    public List<Remind> getRemindsToShowForUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Integer idUser = usersService.getUserByLogin(username).getId();
        List<Remind> remindList = remindDao.findByIdUser(idUser);   // получение списка напоминалок пользователя
        Date date = new Date();
        List<Remind> displayRemind = new ArrayList<>();
        for (Remind remind : remindList) {
        // возвращает TRUE если сегодняшная дата больше чем в Remind
        if ((date.after(remind.getDataRemind()) || date.equals(remind.getDataRemind())) &
                    (remind.getIsDisplay() == 0)) {
//               SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy 'в' HH:mm");
//               remind.setFormatDate(dateFormat.format(remind.getDataRemind()));
               displayRemind.add(remind);
           }
        }
        return displayRemind;
    }

    @Transactional
    public Remind getRemindListByUserAndLesson(Integer idUser, Integer idLesson) {
        return remindDao.findByIdLessonAndIdUser(idLesson, idUser);
    }

    @Transactional
    public List<Remind> getRemindListByUser() {
//        SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy 'в' HH:mm");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Integer idUser = usersService.getUserByLogin(username).getId();
        List<Remind> list = remindDao.findByIdUser(idUser);
//        for (Remind remind : list){
//           remind.setFormatDate(dateFormat.format(remind.getDataRemind()));
//        }
        return list;
    }

    @Transactional
    public List<ExtendedRemind> getExtendedRemindListByUser() {
        List<Remind> reminds = getRemindListByUser();
        List<ExtendedRemind> result = new ArrayList<>();
        for (Remind remind : reminds) {
            Lesson lesson = remind.getLesson();
            if (lesson.getIdLessonStepic() != null) {
                JSONObject jsonObject = new JSONObject(lesson.getTitle());
                result.add(new ExtendedRemind(remind, jsonObject.getString("sectionTitle") + ": " + jsonObject.getString("lessonTitle")));
            } else {
                result.add(new ExtendedRemind(remind, lesson.getTitle()));
            }
        }
        return result;
    }

    @Transactional
    public List<ExtendedRemind> getExtendedRemindsToShowForUser() {
        List<Remind> reminds = getRemindsToShowForUser();
        List<ExtendedRemind> result = new ArrayList<>();
        for (Remind remind : reminds) {
            Lesson lesson = remind.getLesson();
            if (lesson.getIdLessonStepic() != null) {
                JSONObject jsonObject = new JSONObject(lesson.getTitle());
                result.add(new ExtendedRemind(remind, jsonObject.getString("sectionTitle") + ": " + jsonObject.getString("lessonTitle")));
            } else {
                result.add(new ExtendedRemind(remind, lesson.getTitle()));
            }
        }
        return result;
    }

    @Transactional
    @Scheduled(fixedRate = 60000)
    public void sendRemindOnMail(){
        Date date = new Date();
        for (Remind remind : remindDao.findAll()) {
            if ((remind.getIsSendMail() == 0) && (date.after(remind.getDataRemind()))){
                User users = usersService.getUserByID(remind.getIdUser());
                String to = users.getMail();
                String subject = "Оповещение о новом напоминании";
                String msg = "Уважаемый <strong><em>"+users.getSecondname()+" "+users.getFirstname()+"</em></strong>" +
                        ", в ваш личный кабинет поступило новое напоминание. Пройти по " +
                        "<a href='http://localhost:8081/remind/"+users.getId()+"'><strong><em>ссылке\n" +
                        "</em></strong></a> для просмотра";
                sendMail.sendMail(to,subject,msg);
                remind.setIsSendMail(1);
            }

        }
    }
        //Оценка курса
     public void lessonEvaluation (Map<Question, List<Answer>> questionHashMap){
         Integer idLesson = null;
         Integer numberCorrectAnswer = 0; // колличество правильных вопросов выбраных пользователем
         Integer correctAnswer = 0; // общее количество правильных вопросов
         Double resultTest = .0; // Переменная для записи результата теста
         for(Map.Entry<Question, List<Answer>> entry: questionHashMap.entrySet()){
             for (Answer answer : entry.getValue()){
               if (answer.getRight().equals("true") && answer.getIsSelect() == 1){
                   numberCorrectAnswer++;
               }
               if (answer.getRight().equals("true")){
                     correctAnswer++;
               }
             }
             idLesson = entry.getKey().getIdChapterCourse();
         }

         resultTest =  (double)numberCorrectAnswer/correctAnswer * 100;
         Integer idUser = userCourseService.registerLesson(idLesson);   //Регистрация курса у пользователя
         dateRemindAndSave(resultTest, idLesson, idUser, new Date());
     }

     @Transactional
     public void dateRemindAndSave (Double resultTest, Integer idLesson, Integer idUser, Date dateToday) {
         Date dateRemind = new Date();
         Long dayRemind = null; // На какой день напомнить
         Long msInDay = (long) (60 * 1000);
         if (resultTest < 35){
              dayRemind = dateToday.getTime() + msInDay;
              dateRemind.setTime(dayRemind);
         }
         else if ((resultTest >= 35) && (resultTest < 80)){
             dayRemind = dateToday.getTime() + msInDay*2;
             dateRemind.setTime(dayRemind);
         }
         else {
             dayRemind = dateToday.getTime() + msInDay*3;
             dateRemind.setTime(dayRemind);
         }
         Remind remind = remindDao.findByIdLessonAndIdUser(idLesson,idUser);
          if (remind == null){
              remind = new Remind();
          }
          else if((Double.parseDouble(remind.getResultTest().replace(",",".")) >= 80)
                  && (resultTest >= 80)) {
              return;
          }
         SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy 'в' HH:mm");
         remind.setDataPass(dateFormat.format(dateToday));
         String result = String.format("%.2f",resultTest);
         remind.setIsSendMail(0);
         remind.setIdUser(idUser);
         remind.setIdLesson(idLesson);
         remind.setResultTest(result);
         remind.setIsDisplay(0);
         remind.setDataRemind(dateRemind);
         remindDao.save(remind);
     }

    @Transactional
    public void buttonRepeat(Integer idRemind){
        Remind remind = remindDao.findOne(idRemind);
        remind.setIsDisplay(1);
        remindDao.save(remind);
    }
}

