package nc.project.Hibernate.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import nc.project.Hibernate.DAO.LessonDao;
import nc.project.Hibernate.model.Lesson;
import nc.project.Hibernate.model.Question;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author eugene
 */
@Service
public class LessonService {
    @Data
    @AllArgsConstructor
    public static class ExtendedLesson {
        public String title;
        public Lesson lesson;
    }

    @Data
    @AllArgsConstructor
    public static class Section {
        public Integer number;
        public String sectionTitle;
        public List<ExtendedLesson> sectionLessons;
    }

    @Autowired
    private LessonDao lessonDao;

    @Transactional
    public Lesson create(Lesson lesson) {
        return lessonDao.save(lesson);
    }

    @Transactional
    public List<Lesson> getByIdCourse(Integer idCourse) {
        return lessonDao.findByIdCourseOrderByIdAsc(idCourse);
    }

    @Transactional
    public List<Section> getSections(Integer idCourse) {
        List<Section> sections = lessonDao.findByIdCourse(idCourse).stream()
                .collect(Collectors.groupingBy(lesson -> new JSONObject(lesson.getTitle()).getString("sectionTitle")))
                .entrySet().stream()
                .map(entry -> new Section(0, entry.getKey(), entry.getValue().stream()
                        .map(lesson -> new ExtendedLesson(new JSONObject(lesson.getTitle()).getString("lessonTitle"), lesson))
                        .sorted((l1, l2) -> l1.lesson.getId().compareTo(l2.lesson.getId()))
                        .collect(Collectors.toList())
                ))
                .sorted((s1, s2) -> s1.sectionLessons.get(0).lesson.getId().compareTo(s2.sectionLessons.get(0).lesson.getId()))
                .collect(Collectors.toList());
        for (int i = 0; i < sections.size(); i++) {
            sections.get(i).number = i + 1;
        }
        return sections;
    }

    @Transactional
    public List<Lesson> getByIdStepic(Integer idStepic) {
        return lessonDao.findByIdLessonStepic(idStepic);
    }

    @Transactional
    public Lesson get(Integer id) {
        Lesson lesson = lessonDao.findOne(id);
        List<Question> questionsList = lesson.getQuestionList();
        for(Question question : questionsList){
            question.getAnswerList().iterator();
        }
        return lesson;
    }

    @Transactional
    public void delete(Integer id) {
        lessonDao.delete(id);
    }

    @Transactional
    public Lesson update(Lesson lesson) {
        Lesson old = lessonDao.findOne(lesson.getId());
        old.setTitle(lesson.getTitle());
        old.setText(lesson.getText());
        return lessonDao.save(old);
    }
}
