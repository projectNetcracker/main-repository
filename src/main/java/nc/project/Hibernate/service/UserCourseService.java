package nc.project.Hibernate.service;

import nc.project.Hibernate.DAO.UserDao;
import nc.project.Hibernate.DAO.UserLessonDao;
import nc.project.Hibernate.model.Course;
import nc.project.Hibernate.model.Lesson;
import nc.project.Hibernate.model.User;
import nc.project.Hibernate.model.UserLesson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by 1 on 10.12.2016.
 */
@Service
public class UserCourseService {
    private static Log logger = LogFactory.getLog(UserCourseService.class);

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserService userService;

    @Autowired
    private UserLessonDao userLessonDao;

    @Autowired
    private LessonService lessonService;

    @Autowired
    private CourseService courseService;

    @Transactional
    public Integer registerLesson(Integer idLesson) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        UserLesson userLesson = new UserLesson();
        Integer idUser = userService.getUserByLogin(username).getId();
        List<UserLesson> userLessonList = userLessonDao.findByIdChapterCourseAndIdUser(idLesson, idUser);
        Integer sizeList = userLessonList.size();
        if (sizeList == 0) {
            userLesson.setIdUser(idUser);
            userLesson.setIdChapterCourse(idLesson);
            userLesson.setStartCourse(new Date());
            userLessonDao.save(userLesson);
        }
        return idUser;
    }

    @Transactional
    public List<UserLesson> getUserLesson(Integer userId, Integer lessonId) {
        return userLessonDao.findByIdChapterCourseAndIdUser(lessonId, userId);
    }

    @Transactional
    public void registerCourse(Integer idStepicCourse) {
        if (courseService.existsCourse(idStepicCourse)) {
            List<Lesson> lessonIds = lessonService.getByIdCourse(courseService.getCourse(idStepicCourse).getId());
            for (Integer lessonId : lessonIds.stream().map(Lesson::getId).collect(Collectors.toList())) {
                registerLesson(lessonId);
            }
        }
    }

    @Transactional
    public List<Course> getRegisteredCourses(Integer userId) {
        return userLessonDao.findByIdUser(userId).stream()
                .map(UserLesson::getIdChapterCourse)
                .map(lessonId -> lessonService.get(lessonId))
                .map(Lesson::getIdCourse)
                .distinct()
                .map(courseId -> courseService.getCourseByID(courseId))
                .collect(Collectors.toList());
    }

    @Transactional
    public List<Course> getRegisteredStepicCourses(Integer userId) {
         List<Course> registeredStepicCourse = getRegisteredCourses(userId).stream()
                 .filter(course -> course.getIdStepic() != null)
                 .collect(Collectors.toList());
        for(Course course : registeredStepicCourse){
            course.getLessons().iterator();
        }
        return registeredStepicCourse;
    }
}
