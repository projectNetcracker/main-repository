package nc.project.Hibernate.service;

import nc.project.Hibernate.DAO.AnswerDao;
import nc.project.Hibernate.model.Answer;
import nc.project.Hibernate.model.Question;
import nc.project.Hibernate.model.wrapper.AnswerListWrapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by 1 on 22.11.2016.
 */
@Service
public class AnswerService {

    private static Log logger = LogFactory.getLog(AnswerService.class);

    @Autowired
    private AnswerDao answerDao;

    @Autowired
    private LessonService lessonService;

    @Autowired
    private QuestionService questionService;

     @Transactional
    public void addQuestionAndAnswer (AnswerListWrapper listData, Integer idChapter){
        Question question = new Question();
        question.setDescription(listData.getTextQuestion());
        question.setIdChapterCourse(idChapter);
        questionService.saveQuestion(question);
        logger.error("id курса: "+ question.getId());
        for(Answer answer : listData.getAnswerList()){
            answer.setIdQuestionChapter(question.getId());
            answer.setIsSelect(0);
            answerDao.save(answer);
        }
    }

    @Transactional
    public void UpdateQuestionAndAnswer (AnswerListWrapper listData, Integer idQuestion){

        questionService.updateQuestion(idQuestion, listData.getTextQuestion());

        for(Answer answer : listData.getAnswerList()){
            answerDao.save(answer);
        }
    }
}
