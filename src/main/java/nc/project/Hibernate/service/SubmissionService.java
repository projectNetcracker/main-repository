package nc.project.Hibernate.service;

import nc.project.Hibernate.DAO.SubmissionDao;
import nc.project.Hibernate.model.Attempt;
import nc.project.Hibernate.model.Submission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author eugene
 */
@Service
public class SubmissionService {
    @Autowired
    private SubmissionDao submissionDao;

    @Transactional
    public Submission create(Submission submission) {
        return submissionDao.save(submission);
    }

    @Transactional
    public boolean existsStepic(Integer idStepic) {
        return !submissionDao.findByIdStepic(idStepic).isEmpty();
    }

    @Transactional
    public List<Submission> getByAttempt(Integer idAttempt) {
        return submissionDao.findByIdAttempt(idAttempt);
    }
}
