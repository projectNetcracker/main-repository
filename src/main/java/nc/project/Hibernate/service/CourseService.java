package nc.project.Hibernate.service;

import nc.project.Hibernate.DAO.CourseDao;
import nc.project.Hibernate.model.Course;
import nc.project.Hibernate.model.Remind;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by 1 on 15.11.2016.
 */
@Service
public class CourseService {
    private static Log logger = LogFactory.getLog(CourseService.class);

    @Autowired
    private CourseDao courseDao;

    @Autowired
    private RemindService remindService;

    @Transactional
    public Course addCourse(Course course) {
        return courseDao.save(course);
    }

    @Transactional
    public List<Course> courseList() {
        List<Course> result = courseDao.findAll(new Sort(Sort.Direction.ASC, "id"));
        return result;
    }

    @Transactional
    public void deleteCourse(Integer id) {
        courseDao.delete(id);
        logger.debug("course deleted:" + id);
    }

    @Transactional
    public Course getCourseByID(Integer id) {
        return courseDao.findOne(id);
    }

    @Transactional
    public Course updateCourse(Course course, Integer id) {
        Course course1 = getCourseByID(id);
        course1.setId(course.getId());
        course1.setDescription(course.getDescription());
        course1.setNameCourse(course.getNameCourse());
        course1.setIdStepic(course.getIdStepic());
        return courseDao.save(course1);
    }

    @Transactional
    public boolean existsCourse(Integer idStepic) {
        return courseDao.findByIdStepic(idStepic).size() > 0;
    }

    @Transactional
    public Course getCourse(Integer idStepic) {
        List<Course> courses = courseDao.findByIdStepic(idStepic);
        if (courses.isEmpty()) {
            return null;
        } else {
            return courses.get(0);
        }
    }

    @Transactional
    public List<Course> getNativeCourses() {
        List<Course> courseList = courseDao.findByIdStepicIsNull();
        for(Course course: courseList){
            course.getLessons().iterator();
        }
        return courseList;
    }

    @Transactional
    public Map<Integer, String> lastDataPass() {
        List<Remind> remindList = remindService.getRemindListByUser();
        Map<Integer, String> lastData = new HashMap<>();
        try {
            Date dateRemindOne;
            Date dateRemindTwo;

            SimpleDateFormat format = new SimpleDateFormat("MM.dd.yyyy 'в' HH:mm");
            for (Remind remindOne : remindList) {
                lastData.put(remindOne.getLesson().getCourse().getId(), remindOne.getDataPass());
                dateRemindOne = format.parse(remindOne.getDataPass());
                for (Remind remindTwo : remindList) {
                    dateRemindTwo = format.parse(remindTwo.getDataPass());
                    if ((remindOne.getLesson().getCourse().getId()) == remindTwo.getLesson().getCourse().getId()
                            && dateRemindTwo.getTime() > dateRemindOne.getTime()) {
                        lastData.put(remindTwo.getLesson().getCourse().getId(), remindTwo.getDataPass());
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return lastData;
    }
}
