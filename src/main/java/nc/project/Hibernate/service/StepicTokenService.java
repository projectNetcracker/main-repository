package nc.project.Hibernate.service;

import nc.project.Hibernate.DAO.StepicTokenDao;
import nc.project.Hibernate.model.StepicToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author eugene
 */
@Service
public class StepicTokenService {
    @Autowired
    private StepicTokenDao stepicTokenDao;

    @Transactional
    public StepicToken create(StepicToken stepicToken) {
        return stepicTokenDao.save(stepicToken);
    }

    @Transactional
    public StepicToken update(StepicToken stepicToken) {
        List<StepicToken> old = stepicTokenDao.findByIdUser(stepicToken.getIdUser());
        for (StepicToken oldToken : old) {
            if (oldToken != null) {
                stepicTokenDao.delete(oldToken.getId());
            }
        }
        return stepicTokenDao.save(stepicToken);
    }

    @Transactional
    public StepicToken get(Integer userId) {
        List<StepicToken> stepicTokens = stepicTokenDao.findByIdUser(userId);
        if (stepicTokens.isEmpty()) {
            return null;
        } else {
            return stepicTokens.get(0);
        }
    }

}
