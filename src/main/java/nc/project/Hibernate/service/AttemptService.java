package nc.project.Hibernate.service;

import nc.project.Hibernate.DAO.AttemptDao;
import nc.project.Hibernate.model.Attempt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author eugene
 */
@Service
public class AttemptService {
    @Autowired
    private AttemptDao attemptDao;

    @Transactional
    public Attempt create(Attempt attempt) {
        return attemptDao.save(attempt);
    }

    @Transactional
    public boolean exists(Integer idStepic) {
        return !attemptDao.findByIdStepic(idStepic).isEmpty();
    }

    @Transactional
    public Attempt getByStepic(Integer idStepic) {
        List<Attempt> attempts = attemptDao.findByIdStepic(idStepic);
        if (attempts.isEmpty()) {
            return null;
        } else {
            return attempts.get(0);
        }
    }

    @Transactional
    public boolean existsStepic(Integer idStepic) {
        return !attemptDao.findByIdStepic(idStepic).isEmpty();
    }

    @Transactional
    public List<Attempt> getByUser(Integer idUser) {
        return attemptDao.findByIdUser(idUser);
    }
}
