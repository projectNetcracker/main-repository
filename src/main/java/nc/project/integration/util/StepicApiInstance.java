package nc.project.integration.util;

import com.github.scribejava.core.builder.api.DefaultApi20;

/**
 * @author eugene
 */
public class StepicApiInstance extends DefaultApi20 {

    public String getAccessTokenEndpoint() {
        return "https://stepic.org/oauth2/token/";
    }

    protected String getAuthorizationBaseUrl() {
        return "https://stepik.org/oauth2/authorize/";
    }

    private static class InstanceHolder {
        private static final StepicApiInstance INSTANCE = new StepicApiInstance();

        private InstanceHolder() {
        }
    }

    public static StepicApiInstance instance() {
        return StepicApiInstance.InstanceHolder.INSTANCE;
    }
}

