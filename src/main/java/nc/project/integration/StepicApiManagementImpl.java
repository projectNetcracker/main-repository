package nc.project.integration;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.google.common.collect.Lists;
import io.mikael.urlbuilder.UrlBuilder;
import nc.project.Hibernate.model.StepicToken;
import nc.project.Hibernate.service.StepicTokenService;
import nc.project.integration.model.StepicAttempt;
import nc.project.integration.model.StepicCourse;
import nc.project.integration.model.StepicLesson;
import nc.project.integration.model.StepicSection;
import nc.project.integration.model.StepicStep;
import nc.project.integration.model.StepicSubmission;
import nc.project.integration.model.StepicTokenData;
import nc.project.integration.model.StepicUnit;
import nc.project.integration.model.StepicUser;
import nc.project.integration.util.StepicApiInstance;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.query.criteria.internal.expression.function.AggregationFunction;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author eugene
 */
@Service
@Scope(value = "session")
public class StepicApiManagementImpl {

    /**
     * Client Key, Client secret нужно получить в
     * https://stepik.org/oauth2/applications/
     *
     * Callback нужно установить там же и дальше обрабатывать контроллером
     */
    private static final String CLIENT_ID = "88ZQ4uWPd3OHvgiqMrn2CdCCXMIA93937HWCetVD";

    private static final String CLIENT_SECRET = "YWoOPlRvTIClu4vhUqjqwdhRtcz9Y4azH8SqvuVOnXcfEOMJxxj8257jdSlEyiiEEHRHYdc6teTIoN9Lkscjd3m4Al7GjSE2BfefSU7t97sPxrzbGUAha36zxtwik2bT";

    private static final String CALLBACK = "http://127.0.0.1:8081/tokenCode/";

    private static final String STEPICS_URL = "https://stepik.org/api/stepics/1";

    private static final String COURSE_SUBSCRIPTIONS_URL = "https://stepik.org/api/course-subscriptions/";

    private static final String COURSES_URL = "https://stepik.org/api/courses/";

    private static final String SECTIONS_URL = "https://stepik.org/api/sections/";

    private static final String UNITS_URL = "https://stepik.org/api/units/";

    private static final String LESSONS_URL = "https://stepik.org/api/lessons/";

    private static final String STEPS_URL = "https://stepik.org/api/steps/";

    private static final String ATTEMPTS_URL = "https://stepik.org:443/api/attempts";

    private static final String SUBMISSIONS_URL = "https://stepik.org:443/api/submissions";

    private static final int MAX_ID_COUNT = 4;

    private static final int MAX_CONNECTION_TRIES = 3;

    private static final int CONNECT_TIMEOUT = 5000;

    private OAuth20Service service;

    private static Log logger = LogFactory.getLog(StepicApiManagementImpl.class);

    @Autowired
    private StepicTokenService stepicTokenService;

    private OAuth20Service getService(Integer userId) {
        return new ServiceBuilder()
                .apiKey(CLIENT_ID)
                .apiSecret(CLIENT_SECRET)
                .state(getSecret(userId))
                .callback(CALLBACK)
                .connectTimeout(CONNECT_TIMEOUT)
                .build(StepicApiInstance.instance());
    }

    public String getAuthorizationURL(Integer userId) {
        System.out.println("=== Stepic's OAuth Workflow ===");
        System.out.println("Fetching the Authorization URL...");
        OAuth20Service service = getService(userId);
        String authorizationUrl = service.getAuthorizationUrl();
        System.out.println("Got the Authorization URL!\nURL: " + authorizationUrl);
        return authorizationUrl;
    }

    public String getSecret(Integer userId) {
        return "secret" + userId;
    }

    public Integer getUser(String state) {
        Pattern secretPattern = Pattern.compile("secret(.*)");
        Matcher matcher = secretPattern.matcher(state);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("State " + state + " doesn't match pattern secret(?<userId>.*)");
        } else {
            String userId = state.substring(6);
            try {
                return Integer.parseInt(userId);
            } catch (Exception e) {
                throw new IllegalArgumentException("User id " + userId + " is not an integer");
            }
        }
    }

    public synchronized StepicTokenData getAccessToken(Integer userId, String code, Instant getTime) throws IOException {
        System.out.println("Try to get access token with code \"" + code);
        System.out.println("Trading the Request Token for an Access Token...");
        service = getService(userId);
        OAuth2AccessToken accessToken = service.getAccessToken(code);
        System.out.println("Got the Access Token!");
        System.out.println("(if your curious it looks like this: " + accessToken
                + ", 'rawResponse'='" + accessToken.getRawResponse() + "')");
        StepicTokenData stepicTokenData = new StepicTokenData(userId, getTime, accessToken);
        stepicTokenService.create(stepicTokenData.convert());
        return stepicTokenData;
    }

    private boolean refreshToken(StepicTokenData stepicTokenData, OAuth20Service service) throws IOException {
        if (Instant.now().isAfter(stepicTokenData.getExpireTime())) {
            OAuthRequest request = new OAuthRequest(Verb.POST, StepicApiInstance.instance().getRefreshTokenEndpoint(), service);
            request.addBodyParameter("grant_type", "refresh_token");
            OAuth2AccessToken accessToken = stepicTokenData.getToken();
            request.addBodyParameter("refresh_token", accessToken.getRefreshToken()); // were accessToken is the Token object you want to refresh.
            request.addBodyParameter("client_id", CLIENT_ID);
            request.addBodyParameter("client_secret", CLIENT_SECRET);
            Response response = request.send();
            JSONObject tokenJson = new JSONObject(response.getBody());
            stepicTokenData.refresh(tokenJson);
            return true;
        } else {
            return false;
        }
    }

    public void init(Integer userId) {
        service = getService(userId);
    }

    private List<JSONObject> callApi(StepicTokenData stepicTokenData, String url) throws IOException {
        System.out.println("Requesting " + url);
        if (service == null) {
            throw new IllegalStateException("You haven't initialized service yet.");
        }
        if (refreshToken(stepicTokenData, service)) {
            stepicTokenService.update(stepicTokenData.convert());
        }
        List<JSONObject> result = new ArrayList<>();
        int page = 1;
        while (true) {
            String pagedUrl = UrlBuilder.fromString(url).addParameter("page", page++ + "").toString();
            final OAuthRequest request = new OAuthRequest(Verb.GET, pagedUrl, service);
            service.signRequest(stepicTokenData.getToken(), request);
            JSONObject answer = null;
            for (int i = 0; i < MAX_CONNECTION_TRIES; i++) {
                try {
                    Response response = request.send();
                    System.out.println("Response:");
                    System.out.println(response.getCode());/**/
                    System.out.println(response.getBody());
                    answer = new JSONObject(response.getBody());
                    result.add(answer);
                    break;
                } catch (Exception e) {
                    System.out.println("API exception:" + e.getMessage());
                }
            }
            if (answer == null || !answer.has("meta") || !answer.getJSONObject("meta").getBoolean("has_next")) {
                break;
            }

        }
        return result;
    }

    /**
     * Получает информацию о пользователе
     * @return
     * @throws IOException
     */
    public StepicUser loadUser(StepicTokenData stepicTokenData) throws IOException {
        return new StepicUser(callApi(stepicTokenData, STEPICS_URL).get(0).getJSONArray("users").getJSONObject(0));
    }

    /**
     * Получает идентификаторы курсов, которые проходит пользователь
     * @return
     * @throws IOException
     */
    public List<Integer> getEnrolledCourseIds(StepicTokenData stepicTokenData) throws IOException {
        final List<Integer> result = new ArrayList<>();
        callApi(stepicTokenData, COURSE_SUBSCRIPTIONS_URL).forEach(courseSubscriptionsObject -> {
            JSONArray courseArray = courseSubscriptionsObject.getJSONArray("course-subscriptions");
            for (int i = 0; i < courseArray.length(); i++) {
                result.add(courseArray.getJSONObject(i).getInt("course"));
            };
        });
        return result;
    }

    public List<StepicCourse> getEnrolledCourses(StepicTokenData stepicTokenData) throws IOException {
        return loadCourses(stepicTokenData, getEnrolledCourseIds(stepicTokenData));
    }

    /**
     * Получает курс по идентификатору
     * @param stepicTokenData
     * @param courseIdList
     * @return
     * @throws IOException
     */
    public List<StepicCourse> loadCourses(StepicTokenData stepicTokenData, List<Integer> courseIdList) throws IOException {
        final List<StepicCourse> stepicCourses = new ArrayList<>();
        for (List<Integer> courseIds : Lists.partition(courseIdList, MAX_ID_COUNT)) {
            UrlBuilder urlBuilder = UrlBuilder.fromString(COURSES_URL);
            for (int courseId : courseIds) {
                urlBuilder = urlBuilder.addParameter("ids[]", courseId + "");
            }
            List<JSONObject> jsonObjects = callApi(stepicTokenData, urlBuilder.toString());
            for (JSONObject jsonPart : jsonObjects) {
                JSONArray courseJsons = jsonPart.getJSONArray("courses");
                for (int i = 0; i < courseJsons.length(); i++) {
                    StepicCourse stepicCourse = new StepicCourse(courseJsons.getJSONObject(i));
                    stepicCourse.setStepicSections(loadSections(stepicTokenData, stepicCourse.getStepicSectionIds()));
                    stepicCourses.add(stepicCourse);
                }
            }
        }
        return stepicCourses;
    }

    /**
     * Получает секцию курса по идентификатору
     * @param sectionIdList
     * @return
     * @throws IOException
     */
    public List<StepicSection> loadSections(StepicTokenData stepicTokenData, List<Integer> sectionIdList) throws IOException {
        List<StepicSection> stepicSections = new ArrayList<>();
        for (List<Integer> sectionIds : Lists.partition(sectionIdList, MAX_ID_COUNT)) {
            UrlBuilder urlBuilder = UrlBuilder.fromString(SECTIONS_URL);
            for (int sectionId : sectionIds) {
                urlBuilder = urlBuilder.addParameter("ids[]", sectionId + "");
            }
            List<JSONObject> jsonObjects = callApi(stepicTokenData, urlBuilder.toString());
            for (JSONObject jsonPart : jsonObjects) {
                JSONArray sectionJsons = jsonPart.getJSONArray("sections");
                for (int i = 0; i < sectionJsons.length(); i++) {
                    StepicSection stepicSection = new StepicSection(sectionJsons.getJSONObject((i)));
                    stepicSection.setUnits(loadUnits(stepicTokenData, stepicSection.getUnitIds()));
                    stepicSections.add(stepicSection);
                }
            }
        }
        return stepicSections;
    }

    /**
     * Получает модуль секции по идентификатору
     * @param unitIdList
     * @return
     * @throws IOException
     */
    public List<StepicUnit> loadUnits(StepicTokenData stepicTokenData, List<Integer> unitIdList) throws IOException {
        List<StepicUnit> stepicUnits = new ArrayList<>();
        for (List<Integer> unitIds : Lists.partition(unitIdList, MAX_ID_COUNT)) {
            UrlBuilder urlBuilder = UrlBuilder.fromString(UNITS_URL);
            for (int unitId : unitIds) {
                urlBuilder = urlBuilder.addParameter("ids[]", unitId + "");
            }
            List<JSONObject> jsonObjects = callApi(stepicTokenData, urlBuilder.toString());
            List<Integer> lessonIds = new ArrayList<>();
            Map<Integer, StepicUnit> lessonToUnit = new HashMap<>();
            for (JSONObject jsonPart : jsonObjects) {
                JSONArray unitJsons = jsonPart.getJSONArray("units");
                for (int i = 0; i < unitJsons.length(); i++) {
                    StepicUnit stepicUnit = new StepicUnit(unitJsons.getJSONObject(i));
                    lessonIds.add(stepicUnit.getLessonId());
                    lessonToUnit.put(stepicUnit.getLessonId(), stepicUnit);
                    stepicUnits.add(stepicUnit);
                }
            }
            List<StepicLesson> stepicLessons = loadLessons(stepicTokenData, lessonIds);
            for (StepicLesson stepicLesson : stepicLessons) {
                lessonToUnit.get(stepicLesson.getId()).setStepicLesson(stepicLesson);
            }
        }
        return stepicUnits;
    }

    /**
     * Получает урок модуля по идентификатору
     * @param lessonIdList
     * @return
     * @throws IOException
     */
    public List<StepicLesson> loadLessons(StepicTokenData stepicTokenData, List<Integer> lessonIdList) throws IOException {
        List<StepicLesson> stepicLessons = new ArrayList<>();
        for (List<Integer> lessonIds : Lists.partition(lessonIdList, MAX_ID_COUNT)) {
            UrlBuilder urlBuilder = UrlBuilder.fromString(LESSONS_URL);
            for (int lessonId : lessonIds) {
                urlBuilder = urlBuilder.addParameter("ids[]", lessonId + "");
            }
            List<JSONObject> jsonObjects = callApi(stepicTokenData, urlBuilder.toString());
            for (JSONObject jsonPart : jsonObjects) {
                JSONArray lessonJsons = jsonPart.getJSONArray("lessons");
                for (int i = 0; i < lessonJsons.length(); i++) {
                    StepicLesson stepicLesson = new StepicLesson(lessonJsons.getJSONObject(i));
                    stepicLesson.setStepicSteps(loadStep(stepicTokenData, stepicLesson.getStepIds()));
                    stepicLessons.add(stepicLesson);
                }
            }
        }
        return stepicLessons;
    }

    /**
     * Получает шаг урока по идентификатору
     * @param stepIdList
     * @return
     * @throws IOException
     */
    public List<StepicStep> loadStep(StepicTokenData stepicTokenData, List<Integer> stepIdList) throws IOException {
        List<StepicStep> stepicSteps = new ArrayList<>();
        for (List<Integer> stepIds : Lists.partition(stepIdList, MAX_ID_COUNT)) {
            UrlBuilder urlBuilder = UrlBuilder.fromString(STEPS_URL);
            for (int stepId : stepIds) {
                urlBuilder = urlBuilder.addParameter("ids[]", stepId + "");
            }
            List<JSONObject> jsonObjects = callApi(stepicTokenData, urlBuilder.toString());
            for (JSONObject jsonPart : jsonObjects) {
                JSONArray stepJsons = jsonPart.getJSONArray("steps");
                for (int i = 0; i < stepJsons.length(); i++) {
                    StepicStep stepicStep = new StepicStep(stepJsons.getJSONObject(i));
                    stepicSteps.add(stepicStep);
                }
            }
        }
        return stepicSteps;
    }

    /**
     * Получает попытки пользователя пройти шаг по идентификатору шага.
     * Если не задан шаг, загружаются все действия.
     * @param stepId
     * @return
     * @throws IOException
     */
    public List<StepicAttempt> loadAttempts(StepicTokenData stepicTokenData, Integer stepId) throws IOException {
        List<StepicAttempt> stepicAttempts = new ArrayList<>();
        UrlBuilder urlBuilder = UrlBuilder.fromString(ATTEMPTS_URL);
        if (stepId != null) {
            urlBuilder.addParameter("step", stepId + "");
        }
        List<JSONObject> response = callApi(stepicTokenData, urlBuilder.toString());
        for (JSONObject responsePart : response) {
            if (responsePart.has("attempts")) {
                JSONArray attempts = responsePart.getJSONArray("attempts");
                for (int i = 0; i < attempts.length(); i++) {
                    stepicAttempts.add(new StepicAttempt(attempts.getJSONObject(i)));
                }
            }
        }
        return stepicAttempts;
    }

    /**
     * Получает посылки пользователя в одной попытке.
     * Если не задана попытка, загружаются все посылки.
     * @param attemptId
     * @return
     * @throws IOException
     */
    public List<StepicSubmission> loadSubmissions(StepicTokenData stepicTokenData, Integer attemptId) throws IOException {
        List<StepicSubmission> stepicSubmissions = new ArrayList<>();
        UrlBuilder urlBuilder = UrlBuilder.fromString(SUBMISSIONS_URL);
        if (attemptId != null) {
            urlBuilder.addParameter("attempt", attemptId + "");
        }
        List<JSONObject> response = callApi(stepicTokenData, urlBuilder.toString());
        for (JSONObject responsePart : response) {
            if (responsePart.has("submissions")) {
                JSONArray submissions = responsePart.getJSONArray("submissions");
                for (int i = 0; i < submissions.length(); i++) {
                    stepicSubmissions.add(new StepicSubmission(submissions.getJSONObject(i)));
                }
            }
        }
        return stepicSubmissions;
    }
}
