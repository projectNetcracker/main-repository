package nc.project.integration.model;

import lombok.Data;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author eugene
 */
@Data
public class StepicSection {
    private Integer id;

    private List<Integer> unitIds;

    private List<StepicUnit> units;

    private String title;

    private JSONObject sectionJson;

    public StepicSection(JSONObject sectionJson) {
        this.sectionJson = sectionJson;
        id = sectionJson.getInt("id");
        title = sectionJson.getString("title");
        unitIds = new ArrayList<>();
        JSONArray sectionUnits = sectionJson.getJSONArray("units");
        for (int j = 0; j < sectionUnits.length(); j++) {
            unitIds.add(sectionUnits.getInt(j));
        }
        units = new ArrayList<>();
    }
}
