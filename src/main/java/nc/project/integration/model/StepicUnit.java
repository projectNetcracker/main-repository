package nc.project.integration.model;

import lombok.Data;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author eugene
 */
@Data
public class StepicUnit {
    private Integer id;

    private List<Integer> assignmentsIds;

    private Integer lessonId;

    private StepicLesson stepicLesson;

    private JSONObject unitJson;

    public StepicUnit(JSONObject unitJson) {
        this.unitJson = unitJson;
        id = unitJson.getInt("id");
        lessonId = unitJson.getInt("lesson");
        assignmentsIds = new ArrayList<>();
        JSONArray unitAssignments = unitJson.getJSONArray("assignments");
        for (int j = 0; j < unitAssignments.length(); j++) {
            assignmentsIds.add(unitAssignments.getInt(j));
        }
    }
}
