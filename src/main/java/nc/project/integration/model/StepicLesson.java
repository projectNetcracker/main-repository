package nc.project.integration.model;

import jdk.nashorn.internal.ir.debug.JSONWriter;
import lombok.Data;
import nc.project.Hibernate.model.Lesson;
import nc.project.Hibernate.model.Lesson;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author eugene
 */
@Data
public class StepicLesson {
    private Integer id;

    private List<Integer> stepIds;

    private List<StepicStep> stepicSteps;

    private String title;

    private JSONObject lessonJson;

    public StepicLesson(JSONObject lessonJson) {
        this.lessonJson = lessonJson;
        id = lessonJson.getInt("id");
        title = lessonJson.getString("title");
        stepIds = new ArrayList<>();
        JSONArray lessonSteps = lessonJson.getJSONArray("steps");
        for (int j = 0; j < lessonSteps.length(); j++) {
            stepIds.add(lessonSteps.getInt(j));
        }
    }

    public Lesson convert(String sectionTitle, Integer idCourse) {
        Lesson lesson = new Lesson();
        lesson.setIsStepic(1);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("sectionTitle", sectionTitle);
        jsonObject.put("lessonTitle", title);
        lesson.setTitle(jsonObject.toString());
        lesson.setText("");
        lesson.setIdLessonStepic(id);
        lesson.setIdCourse(idCourse);
        return lesson;
    }
}
