package nc.project.integration.model;

import lombok.Data;
import org.json.JSONObject;

/**
 * @author eugene
 */
@Data
public class StepicUser {
    private int id;

    private String firstName;

    private String lastName;

    private JSONObject userJson;

    public StepicUser(JSONObject userJson) {
        this.userJson = userJson;
        id = userJson.getInt("id");
        firstName = userJson.getString("first_name");
        lastName = userJson.getString("last_name");
    }
}
