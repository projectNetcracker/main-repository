package nc.project.integration.model;

import lombok.Data;
import nc.project.Hibernate.model.Submission;
import org.json.JSONObject;

import javax.swing.text.DateFormatter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author eugene
 */
@Data
public class StepicSubmission {
    private Integer id;

    private Integer userId;

    private Integer attemptId;

    private boolean correct;

    private Date timestamp;

    private JSONObject submissionJson;

    public StepicSubmission(JSONObject submissionJson) {
        this.submissionJson = submissionJson;
        id = submissionJson.getInt("id");
        attemptId = submissionJson.getInt("attempt");
        correct = submissionJson.getString("status").equals("correct");
        String timestampString = submissionJson.getString("time");
        //time: "2016-12-16T14:20:25Z"
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            timestamp = dateFormat.parse(timestampString);
        } catch (ParseException e) {
            timestamp = new Date();
            System.out.println("Submission timestamp parse exception:" + e.getMessage());
        }
    }

    public Submission convert(Integer attemptId) {
        Submission submission = new Submission();
        submission.setIdStepic(id);
        submission.setIdAttempt(attemptId);
        submission.setIsCorrect(correct + "");
        submission.setTimestamp(Date.from(timestamp.toInstant().plusSeconds(3 * 3600)));
        return submission;
    }
}
