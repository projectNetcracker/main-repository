package nc.project.integration.model;

import lombok.Data;
import nc.project.Hibernate.model.Attempt;
import org.json.JSONObject;

import java.util.Date;

/**
 * @author eugene
 */
@Data
public class StepicAttempt {
    private Integer id;

    private Integer userId;

    private Integer stepId;

    private JSONObject attemptJson;

    public StepicAttempt(JSONObject attemptJson) {
        this.attemptJson = attemptJson;
        id = attemptJson.getInt("id");
        stepId = attemptJson.getInt("step");
        userId = attemptJson.getInt("user");
    }

    public Attempt convert(Integer userId, Integer stepId) {
        Attempt attempt = new Attempt();
        attempt.setIdStep(stepId);
        attempt.setIdStepic(id);
        attempt.setIdUser(userId);
        return attempt;
    }
}
