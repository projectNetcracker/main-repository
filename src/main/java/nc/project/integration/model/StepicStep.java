package nc.project.integration.model;

import lombok.Data;
import nc.project.Hibernate.model.Step;
import org.json.JSONObject;

/**
 * @author eugene
 */
@Data
public class StepicStep {
    private Integer id;

    private JSONObject stepJson;

    public StepicStep(JSONObject stepJson) {
        this.stepJson = stepJson;
        id = stepJson.getInt("id");
    }


    public Step convert(String lessonTitle, Integer idLesson, Integer number) {
        Step step = new Step();
        step.setIdStepic(id);
        step.setNameSection(lessonTitle);
        boolean isQuestion = false;
        switch (stepJson.getJSONObject("block").getString("name")) {
            case "choice":
            case "number":
            //case "code":
            //case "free-answer":
                isQuestion = true;
                break;
        }
        step.setIsQuestion(isQuestion + "");
        step.setNumberInSection(number);
        step.setDescription("");
        step.setNameStep(lessonTitle + " #" + number);
        step.setIdLesson(idLesson);
        return step;
    }
}
