package nc.project.integration.model;

import lombok.Data;
import nc.project.Hibernate.model.Course;
import nc.project.Hibernate.model.Course;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author eugene
 */
@Data
public class StepicCourse {
    private Integer id;

    private List<Integer> stepicSectionIds;

    private List<StepicSection> stepicSections;

    private String title;

    private String summary;

    private String image;

    private JSONObject courseJson;

    public StepicCourse(JSONObject courseJson) {
        this.courseJson = courseJson;
        id = courseJson.getInt("id");
        title = courseJson.getString("title");
        summary = courseJson.getString("summary");
        image = courseJson.getString("cover");
        stepicSectionIds = new ArrayList<>();
        JSONArray courseSections = courseJson.getJSONArray("sections");
        for (int j = 0; j < courseSections.length(); j++) {
            stepicSectionIds.add(courseSections.getInt(j));
        }
        stepicSections = new ArrayList<>();
    }

    public Course convert() {
        Course course = new Course();
        course.setIdStepic(id);
        course.setDescription(summary);
        course.setNameCourse(title);
        course.setImage("https://stepic.org" + image);
        return course;
    }
}
