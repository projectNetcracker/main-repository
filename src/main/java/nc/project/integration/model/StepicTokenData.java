package nc.project.integration.model;

import com.github.scribejava.core.model.OAuth2AccessToken;
import lombok.Data;
import nc.project.Hibernate.model.StepicToken;
import org.json.JSONObject;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

/**
 * @author eugene
 */
@Data
public class StepicTokenData {
    private Integer userId;

    private String accessToken;

    private String refreshToken;

    private Instant getTime;

    private Instant expireTime;

    private String scope;

    private String tokenType;

    private String rawResponse;

    private OAuth2AccessToken token;

    public StepicTokenData(Integer userId, Instant getTime, OAuth2AccessToken token) {
        this.userId = userId;
        accessToken = token.getAccessToken();
        refreshToken = token.getRefreshToken();
        this.getTime = getTime;
        expireTime = getTime.plusSeconds(token.getExpiresIn());
        scope = token.getScope();
        tokenType = token.getTokenType();
        rawResponse = token.getRawResponse();
        this.token = token;
    }

    public void refresh(JSONObject tokenJson) {
        accessToken = tokenJson.getString("access_token");
        refreshToken = tokenJson.getString("refresh_token");
        getTime = Instant.now();
        expireTime = getTime.plusSeconds(tokenJson.getInt("expires_in"));
        rawResponse = tokenJson.toString();
        token = convertToAccess();
    }

    public OAuth2AccessToken convertToAccess() {
        return new OAuth2AccessToken(accessToken, tokenType, (int) ChronoUnit.SECONDS.between(getTime, expireTime), refreshToken, scope, rawResponse);
    }

    public StepicToken convert() {
        StepicToken stepicToken = new StepicToken();
        stepicToken.setIdUser(userId);
        stepicToken.setAccessToken(accessToken);
        stepicToken.setRefreshToken(refreshToken);
        stepicToken.setGetTime(Date.from(getTime));
        stepicToken.setExpiredTime(Date.from(expireTime));
        stepicToken.setScope(scope);
        stepicToken.setTokenType(tokenType);
        stepicToken.setRawResponse(rawResponse);
        return stepicToken;
    }

    public StepicTokenData(StepicToken stepicToken) {
        userId = stepicToken.getIdUser();
        accessToken = stepicToken.getAccessToken();
        refreshToken = stepicToken.getRefreshToken();
        getTime = stepicToken.getGetTime().toInstant();
        expireTime = stepicToken.getExpiredTime().toInstant();
        scope = stepicToken.getScope();
        tokenType = stepicToken.getTokenType();
        rawResponse = stepicToken.getRawResponse();
        token = convertToAccess();
    }
}
