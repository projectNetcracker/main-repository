package nc.project.ControllerMVC;

import nc.project.Hibernate.model.Lesson;
import nc.project.Hibernate.model.Course;
import nc.project.Hibernate.service.CourseService;
import nc.project.Hibernate.service.LessonService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 1 on 16.11.2016.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminLinkChapter {
    private static Log logger = LogFactory.getLog(AdminLinkChapter.class);

    @Autowired
    private LessonService lessonService;

    @Autowired
    private CourseService courseService;

    @RequestMapping(value = "/course/{id}/info", method = RequestMethod.GET)
    public String CourseInfo(@PathVariable("id") Integer id, Model model){
        Lesson lesson = lessonService.get(id);
        model.addAttribute("chapter", lesson);
        model.addAttribute("course", courseService.getCourseByID(lesson.getIdCourse()));
        return "chapterInfo";
    }

    @RequestMapping(value = "/course/{id}/info/update", method = RequestMethod.POST)
    public String ChapterUpdate(@ModelAttribute("chapter") Lesson lesson, Model model){
        lessonService.update(lesson);
        Lesson lesson1 = lessonService.get(lesson.getId());
        Course course = courseService.getCourseByID(lesson1.getIdCourse());
        model.addAttribute("course", course);
        model.addAttribute("chapterList", lessonService.getByIdCourse(lesson1.getIdCourse()));
        return "courseDetails";
    }

    @RequestMapping(value = "/course/{id}/addchapter", method = RequestMethod.GET)
    public String addChapter(@PathVariable("id") Integer IdCourse,Model model){
        Course course = courseService.getCourseByID(IdCourse);
        model.addAttribute("course", course);
        model.addAttribute("chapter", new Lesson());
        return "chapter/addChapter";
    }

    @RequestMapping(value = "/course/{id}/addchapter", method = RequestMethod.POST)
    public String addChapterPost(@PathVariable("id") Integer idCourse,@ModelAttribute("chapter") Lesson chapter,
                                 Model model){
        chapter.setIdCourse(idCourse);
        chapter.setIsStepic(0);
        lessonService.create(chapter);
        Course course = courseService.getCourseByID(idCourse);
        model.addAttribute("course", course);
        model.addAttribute("chapterList", lessonService.getByIdCourse(idCourse));
        return "courseDetails";        // подредактировать
    }

    @RequestMapping(value = "/course/{idCourse}/delete/{idChapter}", method = RequestMethod.GET)
    public String DeleteChapter(@PathVariable("idChapter") Integer idChapter,@PathVariable("idCourse") Integer idCourse,Model model){
        lessonService.delete(idChapter);
        return "redirect:/admin/course/"+idCourse;
    }
}
