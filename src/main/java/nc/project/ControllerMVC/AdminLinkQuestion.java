package nc.project.ControllerMVC;

import nc.project.Hibernate.model.Question;
import nc.project.Hibernate.model.wrapper.AnswerListWrapper;
import nc.project.Hibernate.service.AnswerService;
import nc.project.Hibernate.service.LessonService;
import nc.project.Hibernate.service.QuestionService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 1 on 20.11.2016.
 */
@Controller
@Scope(value = "session")
@RequestMapping(value = "/admin")
public class AdminLinkQuestion {
    private static Log logger = LogFactory.getLog(AdminLinkQuestion.class);

    @Autowired
    private QuestionService questionService;

    @Autowired
    private AnswerService answerService;

    @Autowired
    private LessonService lessonService;

    @RequestMapping(value = "/course/{idChapter}/answer", method = RequestMethod.GET)
    public String question(@PathVariable("idChapter") Integer idChapter, Model model){
        model.addAttribute("questionChapter", questionService.questionListChapter(idChapter));
        model.addAttribute("newQuestion", new Question());
        Integer idCourse = lessonService.get(idChapter).getIdCourse();
        model.addAttribute("idCourse",idCourse);
        model.addAttribute("idChapter", idChapter);
        model.addAttribute("listNewAnswer",new AnswerListWrapper().addFourAnswer());
        return "Question/listQuestion";
    }

    @RequestMapping(value = "/course/{idChapter}/answer/{idQuestion}/info", method = RequestMethod.GET)
    public String updateQuestion(@PathVariable("idChapter") Integer idChapter,
                                 @PathVariable("idQuestion") Integer idQuestion,
                                 Model model){
        model.addAttribute("questionChapter", questionService.questionListChapter(idChapter));
        Question question = questionService.getQuestionById(idQuestion);
        AnswerListWrapper answerListWrapper = new AnswerListWrapper();
        answerListWrapper.setTextQuestion(question.getDescription());
        answerListWrapper.setAnswerList(question.getAnswerList());
        model.addAttribute("Question", question);
        model.addAttribute("idChapter", idChapter);
        model.addAttribute("listNewAnswer",answerListWrapper);
        return "Question/updateQuestion";
    }



    @RequestMapping(value = "/course/{idChapter}/answer/{idQuestion}/delete", method = RequestMethod.GET)
    public String DeleteChapter(@PathVariable("idChapter") Integer idChapter,@PathVariable("idQuestion") Integer idQuestion
            , Model model){
       questionService.deleteQuestion(idQuestion);
        return "redirect:/admin/course/"+idChapter+"/answer";
    }

    @RequestMapping(value = "/course/{idChapter}/addAnswer", method = RequestMethod.POST)
    public String questionAdd(@PathVariable("idChapter") Integer idChapter,
                              @ModelAttribute("listNewAnswer") AnswerListWrapper answerListWrapper,
                              Model model){
        answerService.addQuestionAndAnswer(answerListWrapper,idChapter);
        return "redirect:/admin/course/"+idChapter+"/answer";
    }

    @RequestMapping(value = "/course/{idChapter}/answer/{idQuestion}/update", method = RequestMethod.POST)
    public String updateQuestion(@PathVariable("idQuestion") Integer idQuestion,
                                 @PathVariable("idChapter") Integer idChapter,
                              @ModelAttribute("listNewAnswer") AnswerListWrapper answerListWrapper,
                              Model model){
        answerService.UpdateQuestionAndAnswer(answerListWrapper,idQuestion);
        return "redirect:/admin/course/"+idChapter+"/answer";
    }

}
