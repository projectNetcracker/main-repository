package nc.project.ControllerMVC;

import nc.project.Hibernate.model.Course;
import nc.project.Hibernate.model.Remind;
import nc.project.Hibernate.model.Role;
import nc.project.Hibernate.model.User;
import nc.project.Hibernate.service.RemindService;
import nc.project.Hibernate.service.SendMail;
import nc.project.Hibernate.service.CourseService;
import nc.project.Hibernate.service.StepicService;
import nc.project.Hibernate.service.StepicTokenService;
import nc.project.Hibernate.service.UserCourseService;
import nc.project.Hibernate.service.UserService;
import nc.project.SpringSecurity.CustomUserDetailsService;
import nc.project.SpringSecurity.UserValidator;
import nc.project.integration.StepicApiManagementImpl;
import nc.project.integration.model.StepicAttempt;
import nc.project.integration.model.StepicCourse;
import nc.project.integration.model.StepicLesson;
import nc.project.integration.model.StepicSection;
import nc.project.integration.model.StepicSubmission;
import nc.project.integration.model.StepicTokenData;
import nc.project.integration.model.StepicTokenData;
import nc.project.integration.model.StepicUnit;
import nc.project.integration.model.StepicUser;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserCache;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by 1 on 05.11.2016.
 */
@Controller
@Scope(value = "session")
@SessionAttributes({"user", "remindListDispley", "courseList"})
public class LinkNavigation {

    private static Log logger = LogFactory.getLog(LinkNavigation.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private StepicApiManagementImpl stepicApiManagement;

    @Autowired
    private RemindService remindService;

    @Autowired
    private StepicTokenService stepicTokenService;

    @Autowired
    private StepicService stepicService;

    @Autowired
    private CourseService courseService;

    @Autowired
    private UserCourseService userCourseService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private HttpServletRequest servletRequest;

    public void reauthenticate(final String username, final String password) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                userDetails, password == null ? userDetails.getPassword() : password, userDetails.getAuthorities()));
    }

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String indexPage() {
        if ("anonymousUser".equals(SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal())){
            return "index";
        }
        else
            return "redirect:/welcome";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationPage(Model model)
    {
        model.addAttribute("userForm", new User());
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") User user, BindingResult bindingResult) {

        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
            return "registration";
        }
        user.setId_role(1);
        userService.addUsers(user);
        return "redirect:/welcome";
    }

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public ModelAndView homePage(ModelAndView modelAndView)
    {
        UserDetails userDetails = (UserDetails) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        User user = userService.getUserByLogin(userDetails.getUsername());
        List<Remind> remindListDispley = remindService.getRemindsToShowForUser();
        Map<Integer,String> lastDataPass = courseService.lastDataPass(); // Определяем в какое время был пройден последний урок в курсе
        modelAndView.addObject("lastDataPass", lastDataPass);
        modelAndView.addObject("remindListDispley", remindListDispley);
        modelAndView.addObject("user", user);
        List<Course> registeredCourseList = userCourseService.getRegisteredStepicCourses(user.getId());
        List<Course> nativeCourseList = courseService.getNativeCourses();
        nativeCourseList.addAll(registeredCourseList);
        modelAndView.addObject("courseList", nativeCourseList);
        modelAndView.setViewName("welcome");
        return modelAndView;
    }

    @RequestMapping(value = "/error403", method = RequestMethod.GET)
    public String page403() {
        return "error403";
    }

    @RequestMapping(value = "/result", method = RequestMethod.GET)
    public String resultPage(Model model){
        List<RemindService.ExtendedRemind> extendedReminds = remindService.getExtendedRemindListByUser();
        model.addAttribute("extRemindList", extendedReminds);
        return "/resultPage/resultPage";
    }

    @RequestMapping(value = "/remind/{idRemind}/lesson/{idLesson}/course/{idCourse}", method = RequestMethod.GET)
    public String buttonRepeat(Model model,
                               @PathVariable("idLesson") Integer idLesson,
                               @PathVariable("idRemind") Integer idRemind,
                               @PathVariable("idCourse") Integer idCourse){
        HttpSession session = servletRequest.getSession();
        session.setAttribute("idLessonButton", idLesson);
        session.setAttribute("idRemind", idRemind);
        return "redirect:/course/"+idCourse+"/chapter/"+idLesson;
//        /course/66/chapter/13
    }

    @RequestMapping(value = "/tokenRequest", method = RequestMethod.GET)
    public String tokenRequest(Model model) {
        UserDetails userDetails = (UserDetails) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        User user = userService.getUserByLogin(userDetails.getUsername());
        Integer userId = user.getId();
        if (stepicTokenService.get(userId) == null) {
            return "redirect:" + stepicApiManagement.getAuthorizationURL(userId);
        } else {
            stepicApiManagement.init(userId);
            return "redirect:/synchronize";
        }
    }

    @RequestMapping(value = "/tokenCode/", method = RequestMethod.GET)
    public String tokenCode(@RequestParam("code") String code, @RequestParam("state") String state,
                            Model model) {
        try {
            Integer userId = stepicApiManagement.getUser(state);
            stepicApiManagement.getAccessToken(userId, code, Instant.now());
            User user = userService.getUserByID(userId);
            reauthenticate(user.getLogin(), user.getPass());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "redirect:/synchronize";
    }

    @RequestMapping(value = "/synchronize", method = RequestMethod.GET)
    public String synchronize(Model model) {
        try {
            UserDetails userDetails = (UserDetails) SecurityContextHolder
                    .getContext()
                    .getAuthentication()
                    .getPrincipal();
            User user = userService.getUserByLogin(userDetails.getUsername());
            Integer userId = user.getId();
            StepicTokenData stepicTokenData = new StepicTokenData(stepicTokenService.get(userId));
            logger.debug("Now we're going to access a protected resource...");
            StepicUser stepicUser = stepicApiManagement.loadUser(stepicTokenData);
            logger.debug(stepicUser.getUserJson());
            List<Integer> courseIds = stepicApiManagement.getEnrolledCourseIds(stepicTokenData);
            List<Integer> toLoad = new ArrayList<>();
            for (Integer courseId : courseIds) {
                if (!courseService.existsCourse(courseId)) {
                    toLoad.add(courseId);
                }
            }
            for (Integer id : toLoad) {
                List<Integer> list = new ArrayList<>();
                list.add(id);
                StepicCourse stepicCourse = stepicApiManagement.loadCourses(stepicTokenData, list).get(0);
                stepicService.saveCourse(stepicCourse);
            }
            for (Integer courseId : courseIds) {
                userCourseService.registerCourse(courseId);
            }
            List<StepicAttempt> stepicAttempts = stepicApiManagement.loadAttempts(stepicTokenData, null);
            for (StepicAttempt stepicAttempt : stepicAttempts) {
                stepicService.saveAttempt(stepicAttempt, userId);
                logger.debug(stepicAttempt.getAttemptJson().toString());
            }
            List<StepicSubmission> stepicSubmissions = stepicApiManagement.loadSubmissions(stepicTokenData, null);
            for (StepicSubmission stepicSubmission : stepicSubmissions) {
                stepicService.saveSubmission(stepicSubmission);
                logger.debug(stepicSubmission.getSubmissionJson().toString());
            }
            stepicService.refreshReminds(userId);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return "redirect:/welcome";
    }

}