package nc.project.ControllerMVC;

import nc.project.Hibernate.model.User;
import nc.project.Hibernate.service.RemindService;
import nc.project.Hibernate.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by 1 on 30.11.2016.
 */
@Controller
@Scope(value = "session")
@SessionAttributes ({"user", "remindListDispley"})
@RequestMapping(value = "/remind")
public class LinkRemind {

    @Autowired
    private UserService userService;

    @Autowired
    private RemindService remindService;

    @RequestMapping(method = RequestMethod.GET)
    public String registrationPage(Model model, @ModelAttribute("user") User user,
                                   @ModelAttribute("remindListDispley") List remindListDispley ) {
        List<RemindService.ExtendedRemind> extendedReminds = remindService.getExtendedRemindsToShowForUser();
        model.addAttribute("extReminds",extendedReminds);
        model.addAttribute("user",user);
        return "Remind/remindPage";
    }

    @RequestMapping(value = "/{idUser}",method = RequestMethod.GET)
    public String registrationPage(Model model, @PathVariable("idUser") Integer idUser){
        User user = userService.getUserByID(idUser);
        List<RemindService.ExtendedRemind> extendedReminds = remindService.getExtendedRemindsToShowForUser();
        model.addAttribute("extReminds",extendedReminds);
        model.addAttribute("user",user);
        return "Remind/remindPage";
    }

    @RequestMapping(value = "/sendRemind",method = RequestMethod.GET)
    public String sendRemind(@RequestParam("idRemind") String idRemind,Model model, ServletRequest servletRequest){
       Integer iIdRemind = Integer.parseInt(idRemind);
        remindService.buttonRepeat(iIdRemind);
        return "redirect:/welcome";
    }
//@RequestMapping(value = "/sendRemind",method = RequestMethod.GET)
//public String sendRemind(Model model, ServletRequest servletRequest){
////        String idRemin = servletRequest.getParameter("idRemind");
//    return "redirect:/welcome";
//}
}
