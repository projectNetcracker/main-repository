package nc.project.ControllerMVC;

import nc.project.Hibernate.model.Course;
import nc.project.Hibernate.model.Lesson;
import nc.project.Hibernate.model.Question;
import nc.project.Hibernate.model.Remind;
import nc.project.Hibernate.model.User;
import nc.project.Hibernate.model.wrapper.AnswerListWrapper;
import nc.project.Hibernate.model.wrapper.QuestionListWrapper;
import nc.project.Hibernate.service.CourseService;
import nc.project.Hibernate.service.LessonService;
import nc.project.Hibernate.service.QuestionService;
import nc.project.Hibernate.service.RemindService;
import nc.project.Hibernate.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Set;

/**
 * Created by 1 on 03.12.2016.
 */
@Controller
@Scope(value = "session")
@SessionAttributes({"questionListWtapper","remindListDispley"})
@RequestMapping(value = "/course")
public class LinkCourseAndChapter {

    @Autowired
    private LessonService lessonService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private RemindService remindService;

    @Autowired
    private UserService userService;

    @Autowired
    private CourseService courseService;

    @RequestMapping(value = "/{idCourse}",method = RequestMethod.GET)
    public String contentCourse(Model model, @PathVariable("idCourse") Integer idCourse) {
        Course course = courseService.getCourseByID(idCourse);
        model.addAttribute("currentCourse", course);
        if (course.getIdStepic() == null) {
            List<Lesson> chapterList = lessonService.getByIdCourse(idCourse);
            model.addAttribute("chapterList", chapterList);
        } else {
            List<LessonService.Section> sectionList = lessonService.getSections(idCourse);
            model.addAttribute("sectionList", sectionList);
        }

        //
        UserDetails userDetails = (UserDetails) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();
        User user = userService.getUserByLogin(userDetails.getUsername());
        List<Remind> remindListDispley = remindService.getRemindsToShowForUser();
        List<Remind> checkRemind = remindService.getRemindListByUser();
        model.addAttribute("remindListDispley",remindListDispley);
        model.addAttribute("checkRemind",checkRemind);
        //
        return "Course/contentCourse";
    }

    @RequestMapping(value = "/{idCourse}/chapter/{idChapter}",method = RequestMethod.GET)
    public String chapter(Model model, @PathVariable("idChapter") Integer idChapter,
                          @ModelAttribute("remindListDispley") List remindListDispley){
        Lesson lesson = lessonService.get(idChapter);
        QuestionListWrapper questionListWtapper = new QuestionListWrapper(lesson.getQuestionList());
        model.addAttribute("remindListDispley",remindListDispley);
        model.addAttribute("questionListWtapper",questionListWtapper);
        model.addAttribute("chapter",lesson);
        return "chapter/chapter";
    }

    @RequestMapping(value = "/{idCourse}",method = RequestMethod.POST)
    public String contentCoursePost(Model model
            ,@ModelAttribute("questionListWtapper") QuestionListWrapper questionListWrapper
            ,HttpServletRequest request
            ,@PathVariable("idCourse") Integer idCourse)
    {
        // Проверяем откуда пришел запрос: с напоминания или нет
        Integer idLesson = questionListWrapper.getQuestionMap()
                .entrySet().iterator().next().getKey().getIdChapterCourse();
        HttpSession session = request.getSession();
        Integer idLessonButton = (Integer) session.getAttribute("idLessonButton");
        if (idLessonButton == idLesson){
            Integer idRemind = (Integer) session.getAttribute("idRemind");
            remindService.buttonRepeat(idRemind);
        }
        //
         questionService.detectedQuestion(request,questionListWrapper.getQuestionMap());
        return contentCourse(model,idCourse);
    }
}
