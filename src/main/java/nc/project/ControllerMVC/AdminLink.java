package nc.project.ControllerMVC;

import nc.project.Hibernate.model.Course;
import nc.project.Hibernate.model.User;
import nc.project.Hibernate.service.CourseService;
import nc.project.Hibernate.service.LessonService;
import nc.project.Hibernate.service.UserService;
import nc.project.SpringSecurity.CourseValidator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

/**
 * Created by 1 on 10.11.2016.
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminLink {
    private static Log logger = LogFactory.getLog(AdminLink.class);

    @Autowired
    private UserService userService;

    @Autowired
    CourseService courseService;

    @Autowired
    private LessonService lessonService;

    @Autowired
    private CourseValidator courseValidator;

    @RequestMapping (method = RequestMethod.GET)
    public String adminPage() {
        return "admin";
    }

    @RequestMapping(value = "/userList", method = RequestMethod.GET)
    public String userList(Model model) {
        logger.error("userList");
        model.addAttribute("userList", userService.listUsers());
        return "userList";
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET)
    public String updateAndDetailsUser(@PathVariable("id") int id, Model model) {
        logger.error("update");
        User user = userService.getUserByID(id);
        model.addAttribute("users", user);
        /*logger.error(user);*/
        return "updateAndDetailsUser";
    }

    @RequestMapping(value = "/userEdit", method = RequestMethod.POST)
    public String editUser(@ModelAttribute("users")User user, Model model){
        userService.updateUser(user);
        model.addAttribute("userList", userService.listUsers());
        return "redirect:/admin/userList";
    }

    @RequestMapping(value = "/users/delete/{id}", method = RequestMethod.GET)
    public String deleteUser(@PathVariable("id") Integer id, Model model){
        logger.error("Udalenie : "+ id);
        userService.deleteUser(id);
        model.addAttribute("userList", userService.listUsers());
        return "redirect:/admin/userList";
    }

    @RequestMapping(value = "/course/delete/{id}", method = RequestMethod.GET)
    public String deleteCourse(@PathVariable("id") Integer id, Model model){
        logger.error("id Course : " + id);
        courseService.deleteCourse(id);
//        model.addAttribute("course", new Course());
//        model.addAttribute("courselist", courseService.courseList());
        return "redirect:/admin/addCourse";
    }


    @RequestMapping(value = "/addCourse", method = RequestMethod.GET)
    public String addCourseGet(Model model) {;
        model.addAttribute("courselist", courseService.courseList());
        return "CourseNThemeAdding";
    }

    @RequestMapping(value = "/addNewCourse", method = RequestMethod.GET)
    public String addNewCourseGet(Model model) {
        model.addAttribute("course", new Course());
        return "Course/addCourse";
    }

    @RequestMapping(value = "/addCourse", method = RequestMethod.POST)
    public String addCoursePost(@ModelAttribute("course") Course course, Model model) {
        logger.error("course_ChapterCourse : " + course);
        courseService.addCourse(course);
        model.addAttribute("courselist", courseService.courseList());
        return "CourseNThemeAdding";
    }

    @RequestMapping(value = "/course/update/{id}", method = RequestMethod.GET)
    public String updateCourseGet(@PathVariable("id") Integer id, Model model) {
        Course course =  courseService.getCourseByID(id);
        model.addAttribute("course", course);
        model.addAttribute("courselist", courseService.courseList());
        model.addAttribute("action","Изменить");
        return "CourseNThemeAdding";
    }

    @RequestMapping(value = "/course/update/{id}", method = RequestMethod.POST)
    public String updateCoursePost(@PathVariable("id") Integer id,
                                   @ModelAttribute("course") Course course,
                                   BindingResult bindingResult,
                                   Model model) {
         courseValidator.validate(course, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute("chapterList", lessonService.getByIdCourse(id));
            return "courseDetails";
        }

        logger.error("приходящий курс:"+ course);
        courseService.updateCourse(course, id);
        model.addAttribute("course", new Course());
        model.addAttribute("courselist", courseService.courseList());
        return "CourseNThemeAdding";
    }

    @RequestMapping(value = "/course/{id}", method = RequestMethod.GET)
    public String CourseInfo(@PathVariable("id") Integer id, Model model){
        Course course = courseService.getCourseByID(id);
        model.addAttribute("course", course);
        model.addAttribute("chapterList", lessonService.getByIdCourse(id));
//        logger.error("Список курсов : " + courses.getChapterCourses());
        return "courseDetails";
    }

}