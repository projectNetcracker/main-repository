--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.3
-- Dumped by pg_dump version 9.3.3
-- Started on 2016-12-11 23:24:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- TOC entry 2051 (class 0 OID 18468)
-- Dependencies: 175
-- Data for Name: courses; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO courses (id, name_course, description, id_stepic, image) VALUES (76, 'Руководство по Git', 'Git – это распределённая система контроля версий (далее – СКВ), которая позволяет управлять исходным кодом. Ищначально, Git был разработан  Линусом Торвальдсом для разработки Linux Kernel.', NULL, 'http://freevector.co/wp-content/uploads/2011/05/39378-git-sketched-social-logo-outline-200x200.png');
INSERT INTO courses (id, name_course, description, id_stepic, image) VALUES (66, 'Java Core', '<p>  Java — строго типизированный объектно-ориентированный язык программирования, разработанный компанией Sun Microsystems (в последующем приобретённой компанией Oracle). Приложения Java обычно транслируются в специальный байт-код, поэтому они могут работать на любой компьютерной архитектуре, с помощью виртуальной Java-машины. Дата официального выпуска — 23 мая 1995 года.</p>
<p><span style="text-align: center">	С развитием Java были созданы её различные типы:</span><span style="text-align: center"><br>
  <span style="">• J2SE – ключевой функционал языка Java.<br>
  • J2EE – для разработки корпоративных приложений<br>
  • J2ME – для разработки мобильных приложений</span></span><br>
  Девизом Java является: “Write Once, Rune Anywhere”. Другими словами, речь идёт о кроссплатформенности. Они достигается за счёт использования вирутальной машины Java (Java Virtual Machine – JVM).<br>
  <em>Какие сильные стороны самого популярного языка программирования в мире?</em>
  Мы можем назвать такие:
<li>Платформо-независимость<br>
  Наш класс, написанный на Java компилируется в платформо-независимый байт-код, который интерпретируется и исполняется JVM.<br>
  Простота<br>
  Синтаксис языка крайне прост в изучении. От нас требуется лишь понимание основ ООП, потому что Java является полностью объекто-ориентированной.<br>
<li>Переносимость<br>
  Тот факт, что Java не реализовывает специфичных аспектов для каждого типа машин, делает программы, написанные с её использованием переносимыми.<br>
<li>Объекто-ориентированность</li>
  Всё сущности в Java являются объектами, что позволяет нам использовать всю мощь ООП.<br>
  <li>Безопасность</li>
  Безопасность Java позволяет разрабатывать ситсемы, защищённые от вирусов и взломов. Авторизация в Java основана на шифровании открытого ключа.<br>
  <li>Устойчивость</li>
  Защита от ошибок обеспечивается за счёт проверок во время компиляции и во время непосредственного выполнения программ.<br>
  <li>Интерпретируемость</li>
  Байт-код Java транслируется в машинные команды “на лету” и нигде не хранится.<br>
  <li>Распределённость</li>
  Java разработана для распредёленного окружения Internet.<br>
  <li>Многопоточность</li>
  Язык поддерживает многопоточность (одновременное выполнение нескольких задач), что позволяет нам создавать хорошо отлаженные приложения<br>
  <li>Производительность</li>
  Использование JIT (Just-In-Time) компилятора, позволяет достигать высоких показателей</p>', NULL, 'http://www.tecwallet.com/ImageUploaderNew/uploads/18_4_2013_16_03_190_core_java_6884011458.jpg');
INSERT INTO courses (id, name_course, description, id_stepic, image) VALUES (74, 'SQL (Structured Query Language)', '<p>Данное руководство посвящено изучению структурированного языка запросов (Structured Query Language – SQL) и дополнен большим количеством примеров, которые наглядно демонстрируют возможности SQL. SQL является стандартом ANSI, но существует большое количество версий данного языка запросов.</p>', NULL, 'https://msdnshared.blob.core.windows.net/media/2016/11/Azure-SQL-Database-generic_COLOR.png');


--
-- TOC entry 2053 (class 0 OID 18479)
-- Dependencies: 177
-- Data for Name: chapter_course; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO chapter_course (id, name_chapter, id_courses, text, is_stepic, id_lesson_stepic) VALUES (14, 'Объекты и классы', 66, '<p><strong>Объекты</strong></p>
<p>Если посмотрим на реальный мир, то увидим, что нас окружают объекты – Люди, Звери, Птицы, Машины и .т.д. Всё это объекты, которые имеют свои свойства и поведение.</p>
<p>Например, человек имеет рост, вес, имя, цвет волос,  глаз и так далее. Человек также имеет определённое поведение. Он дышит, видит, слышит, говорит и т.д.</p>
<p>В программах различные объекты взаимодействуют друг с другом, используя своё поведение. Например, человек взаимодействует с миром (мир – это тоже объект) используя поведение <strong>видеть</strong>.</p>
<p><strong>Класс</strong></p>
<p>Класс сущность, экземпляром которой является объект. Например, объектом класса <strong>Человек</strong> будет конкретный человек – Иванов Иван Иванович. Другими словами, Иванов Иван Иванович – это объекта класса человек.</p>
<p>Класс может включать в себя:</p>
<ul>
  <li><strong>локальные переменные</strong><br>
    Это переменные, которые определяются внутри методов или конструкторов. Т.е. переменная объявляется внутри метода и уничтожается после того, как программа завершает выполнение метода.</li>
  <li><strong>переменные экземпляра<br>
  </strong>Это переменные, которые объявляются внутри класса, но не внутри метода. Эти переменные инициализируются в момент создания экземпляра класса (инициализации).</li>
  <li><strong>переменные класса<br>
  </strong>Это переменные, которые объявляются внутри класса, вне метода с помощью ключевого слова <strong>static.</strong></li>
</ul>
<p>Класс может иметь любое количество переменных и методов. Методы выглядят, примерно, так:</p>
<p>breath()</p>
<p>walk()</p>
<p><strong>Конструкторы</strong></p>
<p>Наиболее важной частью обсуждения классов являются конструкторы. Любой класс в Java имеет конструктор. Даже если мы не прописываем его, то компилятор Java сам создаёт для этого класса конструктор по умолчанию.</p>
<p>Каждый раз, когда мы создаём новый объект (экземпляр класса), запускается конструктор. Главное правило для конструкторов – они должны иметь такое же имя, как и сам класс.</p>', 0, NULL);
INSERT INTO chapter_course (id, name_chapter, id_courses, text, is_stepic, id_lesson_stepic) VALUES (13, 'Базовый синтаксис', 66, '<p>Когда мы говорим о Java-программе, то мы имеем в виду совокупность объектов, которые вызывают методы друг друга.

  Давайте рассмотрим ключевые элементы Java программы:  </p>
<blockquote style="border-left: 0px solid #eee;">
<strong><li style="background:#FFFFFF">Класс
  </li></strong>
 <p> Сущность, которая характеризуется полями (аттрибутами) и поведением (методами).
    Например, разработчик имеет имя, фамилию, специализацию, зарплату и т.д. </p>

  <strong><li style="background:#FFFFFF">Объект</li></strong>
  <p>  Объект – это экземпляр класса.<br>
    Например, конкретный разработчик.<br>
    Имя: Иван<br>
    Фамилия: Иванов<br>
    Специализация: Java – разработчик<br>
    Зарплата: 100500 долларов в час.
  </p>
  <strong><li style="background:#FFFFFF">Метод</li></strong>
  <p>  Метод это сущность, которая описывает поведение класса. Например, разработчик пишет код. Метод writeSomeCode() является выражением этого поведения в нашей программе.
  </p>
  <strong><li style="background:#FFFFFF">Переменная</li></strong>
  <p>  Переменная – это занчения, которые характеризуют поля (аттрибуты) класса. Например: Имя разработчика – это его свойство (поле), а если мы говорим, что этогоразработчика зовут Иван, то “Иван” – является переменной, которая характеризует такое свойство класса разработчик, как имя.
  </p>
</blockquote>', 0, NULL);
INSERT INTO chapter_course (id, name_chapter, id_courses, text, is_stepic, id_lesson_stepic) VALUES (15, 'Типы данныx', 66, '<p>Переменные – это зарезервированное место в памяти для знанения значения. Другими словами, когда мы объявляем переменную, мы резервируем место в памяти.</p>
<p>В зависимости от типа переменной (целые числа, строки, байте и т.д.) выделяется определённое количество памяти.</p>
<p>В языке Java все типы данных делятся на две большие группы:</p>
<ul>
  <li>Примитивные типы данных</li>
  <li>Ссылочное типы данных (Объекты)</li>
</ul>
<hr>
<p><strong>Примитивные типы данных</strong></p>
<p>В Java есть 8 примитивных типов данных:</p>
<p><strong>byte</strong></p>
<ul>
  <li>8-битное целое число</li>
  <li>Максимальное значение: -128 (-2 ^ 7)</li>
  <li>Минимальное значение: 127 (2 ^ 7 -1)</li>
  <li>Значение по умолчанию: 0</li>
  <li>Используется для экономии места в больших массивах. Чаще всего вместо int.</li>
  <li>Пример: byte c = 65</li>
</ul>
<p><strong>short</strong></p>
<ul>
  <li>16-битное целое число</li>
  <li>Максимальное значение: -32,768 (-2 ^ 15)</li>
  <li>Минимальное значение: 32,767 (2 ^ 15 – 1)</li>
  <li>Значение по умолчанию: 0</li>
  <li>Используется для экономии места вместо int.</li>
  <li>Пример: short a = 20000, short b = -10000</li>
</ul>
<p><strong>int</strong></p>
<ul>
  <li>32-битное целое число</li>
  <li>Максимальное значение: -2,147,483,648 (-2 ^ 31)</li>
  <li>Минимальное значение: 2,147,483,647 (-2 ^ 31 – 1)</li>
  <li>Значение по умолчанию: 0</li>
  <li>Используется для целых значений в случае, если нет дефицита памяти.</li>
  <li>Пример: int i = 2000000000, int h = -1500000000</li>
</ul>
<p><strong>long</strong></p>
<ul>
  <li>64-битное целое число</li>
  <li>Максимальное значение: -9,223,372,036,854,775,808 (-2 ^ 63)</li>
  <li>Минимальное значение: 9,223,372,036,854,775,807 (-2 ^ 63 – 1)</li>
  <li>Значение по умолчанию: 0L</li>
  <li>Используется для хранения больших целичисленных значений.</li>
  <li>Пример: long l = 5000000000L, long k = -4000000000L</li>
</ul>
<p><strong>float</strong></p>
<ul>
  <li>32-битное число с плавающей точкой IEEE 754</li>
  <li>Значение по умолчанию: 0.0f</li>
  <li>Используется для экономии памяти в больших массивах чисел с плавающей точкой.<br>
    Никогда не используется для хранения точных значений (например, денег).</li>
  <li>Пример: float f = 112.3f</li>
</ul>
<p><strong>double</strong></p>
<ul>
  <li>64-битное число двойной точности с плавающей точкой IEEE 754</li>
  <li>Значение по умолчанию: 0.0d</li>
  <li>Используется для хранения чисел с плавающей точкой (в большинстве случаев).</li>
  <li>Никогда не используется для хранения точных значений (например, денег).</li>
  <li>Пример: double d = 121.5</li>
</ul>
<p><strong>boolean</strong></p>
<ul>
  <li>В спецификации размер не указан. Зависит от типа JVM.</li>
  <li>Возможные значения: true/false</li>
  <li>Значение по умолчанию: false</li>
  <li>Используется для определения того, является ли условие истинным.</li>
  <li>Пример: boolean flag = true</li>
</ul>
<p><strong>char</strong></p>
<ul>
  <li>Символ кодировки Unicode 16-bit</li>
  <li>Максимальное значение: ‘\u0000’ (или 0)</li>
  <li>Минимальное значение: ‘uffff’ (или 65б535)</li>
  <li>Используется для хранения любого символа</li>
  <li>Пример: char c = ‘C’</li>
</ul>
<hr>
<p><strong>Ссылочные типы данных</strong></p>
<p> </p>
<ul>
  <li>К ссылочным типам данных относятся все типы данных, которые создаются с помощью конструкторов. К ним также относятся все классы, создаваемые разработчиками, например, Developer, Car, Person и т.д.</li>
  <li>Массивы являются ссылочными типами данных.</li>
  <li>Ссылочная переменная может использоваться в качестве ссылки на любой объект определенного типа данных.</li>
  <li>Все ссылочные типы имеют значение по умолчанию: <strong>null</strong>.</li>
  <li>Пример:<br>
    <strong>Developer developer = new Developer(“Java Developer”);</strong></li>
</ul>', 0, NULL);
INSERT INTO chapter_course (id, name_chapter, id_courses, text, is_stepic, id_lesson_stepic) VALUES (24, 'SQL Подзапросы', 74, '<p>Подзапросы, внутренние или вложенные запросы – есть не что иное, как запрос внутри запроса. Обычно, подзапрос используется в конструкции WHERE. И, в большинстве случаев, подзапрос используется, когда вы можете получить значение с помощью запроса, но не знаете конкретного результата.</p>
<p>Подзапросы являются альтернативным путем получения данных из множества таблиц.</p>
<p>Наряду с операторами сравнения такими, как =, &lt;, &gt;, &gt;=, &lt;= и др., Вы можете использовать подзапросы с перечисленными ниже конструкциями:</p>
<ul>
  <li>SELECT</li>
  <li>INSERT</li>
  <li>UPDATE</li>
  <li>DELETE</li>
</ul>
<h2>Примеры подзапросов</h2>
<p>1)Обычно, подзапрос возвращает только одну запись, но случается и так, что записей может быть много, тогда в условии WHERE используются такие операторы, как IN, NOT IN. Запрос может выглядеть следующий образом:</p>
<pre>SELECT first_name, last_name, subject <br>FROM student_details <br>WHERE games NOT IN (''Cricket'', ''Football''); </pre>
<p>Тогда результат будет примерно таким:</p>
<pre style="font-style: italic">first_name    last_name   games<br>------------ ----------- ----------<br>Shekar        Gowda       Badminton<br>Priya         Chandra     Chess</pre>
<br>
<p>2)Давайте рассмотрим используемую ранее таблицу student_details. Если Вы знаете имена студентов, изучающих естествознание (science), то можете получить их id, используя приведенный ниже запрос</p>
<pre>SELECT id, first_name <br>FROM student_details <br>WHERE first_name IN (''Rahul'', ''Stephen''); </pre>
<p>но, если Вы не знаете их имен, то для получения id Вам необходимо написать запрос, описанным ниже способом:</p>
<pre>SELECT id, first_name <br>FROM student_details <br>WHERE first_name IN (SELECT first_name <br>FROM student_details <br>WHERE subject= ''Science''); </pre>
<p>Результат:</p>
<pre><span style="font-style: italic">Id         first_name<br>--------  -------------<br>100        Rahul<br>102        Stephen</span><br>
</pre>
<p>В описанном выше запросе, вначале выполняется внутренний запрос, затем внешний</p>
<br>
<p>3) Вы можете использовать подзапрос с оператором INSERT для добавления данных из одной таблицы в другую. Давайте попробуем сгруппировать всех студентов, которые изучают математику в таблицу math_group</p>
<pre>INSERT INTO maths_group(id, name) <br>SELECT id, first_name || '' '' || last_name <br>FROM student_details WHERE subject= ''Maths'' </pre>
<br>
<p>4) Подзапрос может использоваться с оператором SELECT, как описано ниже. Давайте используем таблицы product и order_items, объединив их между собой</p>
<pre><span style="font-style: normal">select p.product_name<label for="textfield">Text Field:</label>				<br>       p.supplier_name, <br>       (select order_id from order_items where product_id = 101) as order_id<br>from product p <br>where p.product_id = 10</span>1</pre>
<pre><span style="font-style: italic">product_name   supplier_name   order_id<br>-------------- -------------- ----------<br>Television     Onida           5103<label for="textfield">Text Field:</label></span><label for="textfield"></label>
</pre>
<h2>Соотнесенный подзапрос</h2>
<p>Запрос называется соотнесенным, когда оба, и внутренний, и внешний, запросы взаимозависимы. Это означает, что для обработки каждой записи внутреннего запроса, должна быть получена также запись внешнего запроса, т.е. внутренний запрос всецело зависит от внешнего.</p>
<pre>SELECT p.product_name FROM product p <br>WHERE p.product_id = (SELECT o.product_id FROM order_items o <br>WHERE o.product_id = p.product_id); </pre>
<p>Примечание:</p>
<p>1) Вы можете использовать столько вложенных запросов, сколько захотите, но в Oracle не рекомендуется использовать более 16 вложений.</p>
<p>2) Если подзапрос не зависит от внешнего запроса – он называется несоотнесенным подзапросом</p>', 0, NULL);
INSERT INTO chapter_course (id, name_chapter, id_courses, text, is_stepic, id_lesson_stepic) VALUES (27, 'Запись изменений (Commit)', 76, 'Теперь, когда ваш индекс настроен так, как вам и хотелось, вы можете зафиксировать свои изменения. Запомните, всё, что до сих пор не проиндексировано — любые файлы, созданные или изменённые вами, и для которых вы не выполнили git add после момента редактирования — не войдут в этот коммит. Они останутся изменёнными файлами на вашем диске. В нашем случае, когда вы в последний раз выполняли git status, вы видели что всё проиндексировано, и вот, вы готовы к коммиту. Простейший способ зафиксировать изменения — это набрать git commit:
<pre style="font-weight: 900">$ git commit  </pre>
<p>Эта команда откроет выбранный вами текстовый редактор. (Редактор устанавливается системной переменной $EDITOR — обычно это vim или emacs, хотя вы можете установить ваш любимый с помощью команды git config --global core.editor, как было показано в главе 1).</p>
<p>В редакторе будет отображён следующий текст (это пример окна Vim''а):</p>
<pre style="font-weight: 900"># Please enter the commit message for your changes. Lines starting
# with ''#'' will be ignored, and an empty message aborts the commit.
# On branch master
# Changes to be committed:
#   (use "git reset HEAD &lt;file&gt;..." to unstage)  #
#       new file:   README
#       modified:   benchmarks.rb    </pre>
<p>Вы можете видеть, что комментарий по умолчанию для коммита содержит закомментированный результат работы ("выхлоп") команды git status и ещё одну пустую строку сверху. Вы можете удалить эти комментарии и набрать своё сообщение или же оставить их для напоминания о том, что вы фиксируете. (Для ещё более подробного напоминания, что же именно вы поменяли, можете передать аргумент -v в команду git commit. Это приведёт к тому, что в комментарий будет также помещена дельта/diff изменений, таким образом вы сможете точно увидеть всё, что сделано.) Когда вы выходите из редактора, Git создаёт для вас коммит с этим сообщением (удаляя комментарии и вывод diff''а).</p>
<p>Есть и другой способ — вы можете набрать свой комментарий к коммиту в командной строке вместе с командой commit, указав его после параметра -m, как в следующем примере:</p>
<pre style="font-weight: 900">$ git commit -m "Story 182: Fix benchmarks for speed"
[master]: created 463dc4f: "Fix benchmarks for speed"
 2 files changed, 3 insertions(+), 0 deletions(-)
 create mode 100644 README  </pre>
<p>Итак, вы создали свой первый коммит! Вы можете видеть, что коммит вывел вам немного информации о себе: на какую ветку вы выполнили коммит (master), какая контрольная сумма SHA-1 у этого коммита (463dc4f), сколько файлов было изменено, а также статистику по добавленным/удалённым строкам в этом коммите.</p>
<p>Запомните, что коммит сохраняет снимок состояния вашего индекса. Всё, что вы не проиндексировали, так и торчит в рабочем каталоге как изменённое; вы можете сделать ещё один коммит, чтобы добавить эти изменения в репозиторий. Каждый раз, когда вы делаете коммит, вы сохраняете снимок состояния вашего проекта, который позже вы можете восстановить или с которым можно сравнить текущее состояние.</p>
<h3 id="Игнорирование-индексации">Игнорирование индексации</h3>
<p>Несмотря на то, что индекс может быть удивительно полезным для создания коммитов именно такими, как вам и хотелось, он временами несколько сложнее, чем вам нужно в процессе работы. Если у вас есть желание пропустить этап индексирования, Git предоставляет простой способ. Добавление параметра -a в команду git commit заставляет Git автоматически индексировать каждый уже отслеживаемый на момент коммита файл, позволяя вам обойтись без git add:</p>
<pre style="font-weight: 900">$ git status
# On branch master
#
# Changes not staged for commit:
#
#   modified:   benchmarks.rb  #
$ git commit -a -m ''added new benchmarks''
[master 83e38c7] added new benchmarks
1 files changed, 5 insertions(+), 0 deletions(-)  </pre>
<p>Обратите внимание на то, что в данном случае перед коммитом вам не нужно выполнять git addдля файла benchmarks.rb.</p>
<h3 id="Удаление-файлов"></h3>
<p>Для того чтобы удалить файл из Git''а, вам необходимо удалить его из отслеживаемых файлов (точнее, удалить его из вашего индекса) а затем выполнить коммит. Это позволяет сделать команда git rm, которая также удаляет файл из вашего рабочего каталога, так что вы в следующий раз не увидите его как &ldquo;неотслеживаемый&rdquo;.</p>
<p>Если вы просто удалите файл из своего рабочего каталога, он будет показан в секции &ldquo;Changes not staged for commit&rdquo; (&ldquo;Изменённые но не обновлённые&rdquo; — читай не проиндексированные) вывода команды git status:</p>
<pre style="font-weight: 900">$ rm grit.gemspec
$ git status
# On branch master
#
# Changes not staged for commit:
#   (use "git add/rm &lt;file&gt;..." to update what will be committed)
#
#       deleted:    grit.gemspec  </pre>
<p>Затем, если вы выполните команду git rm, удаление файла попадёт в индекс:</p>
<pre style="font-weight: 900">$ git rm grit.gemspec
rm ''grit.gemspec''
$ git status
# On branch master
#
# Changes to be committed:
#   (use "git reset HEAD &lt;file&gt;..." to unstage)
#
#       deleted:    grit.gemspec  </pre>
<p>После следующего коммита файл исчезнет и больше не будет отслеживаться. Если вы изменили файл и уже проиндексировали его, вы должны использовать принудительное удаление с помощью параметра -f. Это сделано для повышения безопасности, чтобы предотвратить ошибочное удаление данных, которые ещё не были записаны в снимок состояния и которые нельзя восстановить из Git''а.</p>
<p>Другая полезная штука, которую вы можете захотеть сделать — это удалить файл из индекса, оставив его при этом в рабочем каталоге. Другими словами, вы можете захотеть оставить файл на винчестере, и убрать его из-под бдительного ока Git''а. Это особенно полезно, если вы забыли добавить что-то в файл .gitignore и по ошибке проиндексировали, например, большой файл с логами, или кучу промежуточных файлов компиляции. Чтобы сделать это, используйте опцию --cached:</p>
<pre style="font-weight: 900">$ git rm --cached readme.txt  </pre>
<p>В команду git rm можно передавать файлы, каталоги или glob-шаблоны. Это означает, что вы можете вытворять что-то вроде:</p>
<pre style="font-weight: 900">$ git rm log/\*.log  </pre>
<p>Обратите внимание на обратный слэш (\) перед *. Он необходим из-за того, что Git использует свой собственный обработчик имён файлов вдобавок к обработчику вашего командного интерпретатора. Эта команда удаляет все файлы, которые имеют расширение .log в каталоге log/. Или же вы можете сделать вот так:</p>
<pre style="font-weight: 900">$ git rm \*~  </pre>
<p>Эта команда удаляет все файлы, чьи имена заканчиваются на ~.</p>', 0, NULL);
INSERT INTO chapter_course (id, name_chapter, id_courses, text, is_stepic, id_lesson_stepic) VALUES (28, 'Просмотр истории коммитов в Git', 76, '<p>Изучение истории коммитов — важная составляющая работы с репозиторием. Увы, ввиду ветвления с этой историей не всегда просто разобраться. Обычно я для этой цели пользуюсь различными визуальными оболочками, но не всегда есть такая возможность. Временами приходится пользоваться средствами консоли, а именно командой <a href="https://www.kernel.org/pub/software/scm/git/docs/git-log.html">git log</a>. Основы работы с этой командой можно почитать в чудесной книге PRO Git . git log имеет множество различных полезных параметров. Рассмотрим несколько примеров их использования.</p>
<h3 id="section">Древовидный вид</h3>
<pre>git log --graph --abbrev-commit --decorate --date=relative --format=format:''%C(bold blue)
    %h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)'' --all  </pre>
<p>Выводим полный граф коммитов c сокращёнными хешами, ссылками на коммиты и относительной датой. Используемый формат: синий сокращённый хеш коммита, зелёная дата, белые сообщение и автор, жёлтые ссылки на коммит.</p>
<p><img src="http://aakinshin.net/img/posts/git/log/git-log1.png"></p>
<hr>
<pre>git log --graph --abbrev-commit --decorate --format=format:''%C(bold blue)%h%C(reset)
     - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(bold yellow)%d%C(reset)%n''''
       %C(white)%s%C(reset) %C(dim white)- %an%C(reset)'' --all  </pre>
<p>Выводим полный граф коммитов c сокращёнными хешами, ссылками на коммиты и абсолютной датой. Используемый формат: синий сокращённый хеш коммита, голубая абсолютная дата, зелёная относительная дата, жёлтые ссылки на коммит, перевод строки, белые сообщение и автор.</p>
<p><img src="http://aakinshin.net/img/posts/git/log/git-log2.png"></p>
<hr>
<pre>git log --graph --oneline --all  </pre>
<p>Выводим полный граф коммитов, отводя по одной строке на коммит.</p>
<p><img src="http://aakinshin.net/img/posts/git/log/git-log3.png"></p>
<hr>
<pre>git log --graph --date-order --pretty=format:"&lt;%h&gt; %ad [%an] %Cgreen%d%Creset %s" --all --date=short  </pre>
<p>Выводим полный граф коммитов c сортировкой по дате, отображаемой в краткой форме. Используемый формат: сокращённый хеш, дата, автор, зелёные ссылки на коммит, сообщение.</p>
<p><img src="http://aakinshin.net/img/posts/git/log/git-log4.png"></p>
<hr>
<h3 id="section-1">Линейный вид</h3>
<pre>git log  </pre>
<p>Вывод списка коммитов с параметрами по умолчанию.</p>
<p><img src="http://aakinshin.net/img/posts/git/log/git-log5.png"></p>
<hr>
<pre>git log -p  </pre>
<p>Выводим список коммитов и показываем diff для каждого.</p>
<p><img src="http://aakinshin.net/img/posts/git/log/git-log6.png"></p>
<hr>
<pre>git log --stat  </pre>
<p>Выводим список коммитов и показываем статистику по каждому.</p>
<p><img src="http://aakinshin.net/img/posts/git/log/git-log7.png"></p>
<hr>
<pre>git log --pretty=oneline  </pre>
<p>Выводим список коммитов по одному на строчке.</p>
<p><img src="http://aakinshin.net/img/posts/git/log/git-log8.png"></p>
<hr>
<pre>git log --pretty=format:"%h - %an, %ar : %s"  </pre>
<p>Выводим список коммитов с использованием следуюещго формата: сокращённый хеш коммита, автор, относительная дата, сообщение.</p>
<p><img src="http://aakinshin.net/img/posts/git/log/git-log9.png"></p>
<hr>
<h3 id="section-2">Визуальный интерфейс</h3>
<p>Если есть возможность, то всё таки коммиты приятнее изучать через специализированный интерфейс, а не из консоли. Одна из популярных GitExtensions:</p>
<p><img src="http://aakinshin.net/img/posts/git/log/git-log10.png"></p>
<p>&nbsp;</p>
<p>Также удобно использовать встроенную утилиту <a href="https://www.kernel.org/pub/software/scm/git/docs/gitk.html">gitk</a>:</p>
<p><img src="http://aakinshin.net/img/posts/git/log/git-log11.png"></p>
<h3 id="section-3">Полезные параметры</h3>
<p>Все параметры команды git log не нужны, но некоторые самые полезные хорошо бы помнить. Приведу несколько примеров использования ходовых параметров.</p>
<ul>
  <li>--graph Показывать древовидную структуру графа истории в ASCII-виде</li>
  <li>-5 Посмотреть последних пять коммитов</li>
  <li>--skip=3 Пропустить три коммита</li>
  <li>--pretty=oneline Отводить по одной строчке на коммит</li>
  <li>--since="today" Показать коммиты за сегодня</li>
  <li>--since=2.weeks Показать коммиты за последние две недели</li>
  <li> -p Показывать diff каждого коммита</li>
  <li>--decorate Показывать ссылки на этот коммит</li>
  <li>--stat Показывать подробную статистику по каждому коммиту</li>
  <li>--shortstat Показывать краткую статистику по каждому коммиту</li>
  <li>--name-only Показывать список изменённых файлов</li>
  <li>--name-status Показывать список изменённых файлов с информацией о них</li>
  <li>--abbrev-commit Показывать только несколько первых цифр SHA-1</li>
  <li>--relative-date Показывать дату в относительной форме</li>
</ul>
<p>C помощью замечательного параметра --pretty=format:"" можно указать, какие именно данные о коммите нужно выводить, определив внутри кавычек общий паттерн, используя следующие обозначения:</p>
<ul>
  <li>%H Хеш коммита</li>
  <li>%h Сокращённый хеш коммита</li>
  <li>%d Имена ссылок на коммит</li>
  <li>%s Сообщение к коммиту</li>
  <li>%an Автор</li>
  <li>%ad Дата автора</li>
  <li>%cn Коммитер</li>
  <li>%cd Дата коммитера</li>
  <li>%Cred Переключить цвет на красный</li>
  <li>%Cgreen Переключить цвет на зелёный</li>
  <li>%Cblue Переключить цвет на синий</li>
  <li>%Creset Сбросить цвет</li>
</ul>
<hr>
<h3 id="section-4"> Полезные ссылки:</h3>
<ul>
  <li><a href="https://www.kernel.org/pub/software/scm/git/docs/git-log.html">Официальный мануал</a></li>
  <li><a href="http://git-scm.com/book/ru/%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D1%8B-Git-%D0%9F%D1%80%D0%BE%D1%81%D0%BC%D0%BE%D1%82%D1%80-%D0%B8%D1%81%D1%82%D0%BE%D1%80%D0%B8%D0%B8-%D0%BA%D0%BE%D0%BC%D0%BC%D0%B8%D1%82%D0%BE%D0%B2">ProGit</a></li>
  <li><a href="http://stackoverflow.com/questions/1057564/pretty-git-branch-graphs">Советы со Stackoverflow</a></li>
  <li><a href="https://github.com/twitter/bootstrap">Репозиторий Twitter bootstrap</a></li>
</ul>', 0, NULL);
INSERT INTO chapter_course (id, name_chapter, id_courses, text, is_stepic, id_lesson_stepic) VALUES (21, 'Введение в SQL', 74, '<hr>
<p><strong>Что такое SQL?</strong></p>
<p>SQL предназанчен для созданения, манипуляции и получения данных, которые хранятся в реляционной базе данны (далее – БД). Он является стандартным языком для Систем Управления Реляционными Базами Данных – СУБД (RDBMS), таких как MySQL, Oracle, Postgres и т.д.</p>
<p>Эти системы используют различные диалекты SQL:</p>
<p>Oracle – PL/SQL</p>
<p>MS SQL – T-SQL</p>
<hr>
<p><strong>Преимущества <span id="orfo-misgrammed-1"><span id="orfo-misspelled-1">SQL</span></span></strong></p>
<p>Среди преимуществ <span id="orfo-misgrammed-2"><span id="orfo-misspelled-2">SQL</span></span> мы можем выделить следующие:</p>
<ul>
  <li>Позволяет пользователям получать доступ к данным в <span id="orfo-misspelled-3">RDBMS</span>.</li>
  <li>Позволят пользователям описывать данные.</li>
  <li>Позволяет пользователям определять данные в БД и манипулировать ими.</li>
  <li>Позволяет встраивать другие языки, используя модули <span id="orfo-misspelled-4">SQL</span>, библиотеки и <span id="orfo-misspelled-5">пре-компиляторы</span>.</li>
  <li>Позволяет пользователям создавать и удалять БД и таблицы.</li>
  <li>Позволяет пользователю создавать встроенные виды, процедуры и функции в БД.</li>
  <li>Позволяет ограничивать доступ к таблицам, процедурам и видам.</li>
</ul>
<hr>
<p><strong>Принцип работы SQL</strong></p>
<p>Когда мы выполняем команду SQL для любой RDBMS, система определяет наилучший способ для обработки нашего запроса, и модуль управление (&ldquo;двигатель&rdquo;) SQL вычисляет, как интерпретировать задачу.</p>
<p>В данный процесс включены такие компоненты:</p>
<ul>
  <li><strong>Query</strong></li>
  <li><strong>Dispatcher</strong></li>
  <li><strong>Optimization Engines</strong></li>
  <li><strong>Classic Query Engine</strong></li>
  <li><strong>SQL Query Engine</strong> и т.д.</li>
</ul>
<p><strong>Classic Query Engine </strong>обрабатывает все <strong>не</strong>-SQL запросы, а <strong>SQL Query Engine</strong> не обрабатывает логические файлы.</p>
<p>Ниже представлена простая диаграмма, емонстрирующая архитектуру SQL:</p>
<p><a href="http://i1.wp.com/proselyte.net/wp-content/uploads/2016/05/sql-architecture.jpg" rel="attachment wp-att-1922"><img src="http://i1.wp.com/proselyte.net/wp-content/uploads/2016/05/sql-architecture.jpg?resize=436%2C400" alt="sql-architecture" width="436" height="400" srcset="http://i1.wp.com/proselyte.net/wp-content/uploads/2016/05/sql-architecture.jpg?resize=300%2C275 300w, http://i1.wp.com/proselyte.net/wp-content/uploads/2016/05/sql-architecture.jpg?w=436 436w" sizes="(max-width: 436px) 100vw, 436px"></a></p>
<hr>
<p><strong>SQL Команды<br>
</strong></p>
<p>Стнадартные SQL команды для взаимодейтсвия с реляционными БД:</p>
<ul>
  <li>CREATE</li>
  <li>SELECT</li>
  <li>INSERT</li>
  <li>UPDATE</li>
  <li>DELETE</li>
  <li>DROP</li>
</ul>
<p>Данные команды разбиты на две большие категории:</p>
<p><strong>DDL – Data Definition Language</strong></p>
<table border="1" cellspacing="0">
  <tbody>
    <tr>
      <th width="75" bgcolor="#B8B8B8">Команда</th>
      <th width="431" bgcolor="#B8B8B8">Описание</th>
    </tr>
    <tr>
      <td height="42">CREATE</td>
      <td>Создаёт новую таблицу, вид таблицы или другой объект в БД.</td>
    </tr>
    <tr>
      <td height="41">ALTER</td>
      <td>Изменяет существующий объект БД (например, таблицу).</td>
    </tr>
    <tr>
      <td height="47">DROP</td>
      <td>Удаляет указанную таблицу, вид таблицы или другой объект БД.</td>
    </tr>
  </tbody>
</table>
<p>&nbsp;</p>
<p><strong>DML – Data Manipulation Language</strong></p>
<table height="177" border="1" cellspacing="0">
  <tbody>
    <tr bgcolor="#B8B8B8">
      <th width="78">Команда</th>
      <th width="437">Описание</th>
    </tr>
    <tr>
      <td height="43">SELECT</td>
      <td>Получает определённые данные из одной или нескольких таблиц.</td>
    </tr>
    <tr>
      <td height="35">INSERT</td>
      <td>Создаёт запись</td>
    </tr>
    <tr>
      <td height="37">UPDATE</td>
      <td>Изменяет записи</td>
    </tr>
    <tr>
      <td height="37">DELETE</td>
      <td>Удаляет записи</td>
    </tr>
  </tbody>
</table>
<p>&nbsp;</p>
<p><strong>DCL – Data Control Language</strong></p>
<table border="1" cellspacing="0">
  <tbody>
    <tr bgcolor="#B8B8B8">
      <th width="76">Команда</th>
      <th width="307">Описание</th>
    </tr>
    <tr>
      <td height="37">GRANT</td>
      <td>Даёт все привелегии пользователю</td>
    </tr>
    <tr>
      <td height="37">REVOKE</td>
      <td>Отменяет привелегии отданные пользователю</td>
    </tr>
  </tbody>
</table>
<br>', 0, NULL);
INSERT INTO chapter_course (id, name_chapter, id_courses, text, is_stepic, id_lesson_stepic) VALUES (22, 'Базовый синтаксис', 74, '<p>Язык запросов к базе довольно лаконичен, как и количество основных операций, которые клиент может сделать с базой. Таких операций - 4. Это получение (SELECT), удаление (DELETE), обновление (UPDATE) и добавление данных (INSERT).</p>
<p>Перед тем, как что-то сделать в SQL попробуйте сформулировать на русском языке полное предложение с командой, которую вы хотите дать серверу. Если вам это удалось - то останется все это &ldquo;перевести&rdquo; на английский и записать для выполнения :). Не пренебрегайте этим правилом, если вам удалось полностью устно сформулировать задачу - она практически решена.</p>
<h2 style="font-size: 18px">SELECT</h2>
<p>Самый простая и часто употребляемая команда, которая позволяет получить от сервера практически любую информацию из таблиц. Её синтаксис прост:</p>
<p><a href="http://search.mysql.com/search?site=refman-%35%31&q=SELECT">SELECT</a> [имя_поля] <a href="http://search.mysql.com/search?site=refman-%35%31&q=FROM">FROM</a> имя_таблицы</p>
<p>Если дословно: ВЫБРАТЬ поле ИЗ таблицы. Имена полей необходимо указывать через запятую или использовать * для вывода всех полей. В примерах, не забывайте указывать префикс таблиц, если он у вас есть. Вот и вся сложность.</p>
<p>Пример:<br>
  <em>Пример:</em><br>
  <a href="http://search.mysql.com/search?site=refman-%35%31&q=SELECT">SELECT</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=TYPE">type</a>, title <a href="http://search.mysql.com/search?site=refman-%35%31&q=FROM">FROM</a> node</p>
<p>Этот запрос покажет все заголовки материалов, вместе с
типом этих материала.</p>
<h3 style="font-size: 18px">WHERE</h3>
<p><a href="http://search.mysql.com/search?site=refman-%35%31&q=WHERE">WHERE</a> поле условие значение</p>
<p>Получение данных полей таблиц не составляет большого труда, достаточно знать имена полей(какие в них хранятся логические данные) и таблицы. Но есть одно но - в большинстве случаев нужны не все записи, а записи, удовлетворяющие определённому условию. Тут на помощь нам приходит выражение <strong>WHERE</strong>:</p>
<p><a href="http://search.mysql.com/search?site=refman-%35%31&q=SELECT">SELECT</a> [имя_поля] <a href="http://search.mysql.com/search?site=refman-%35%31&q=FROM">FROM</a> имя_таблицы <a href="http://search.mysql.com/search?site=refman-%35%31&q=WHERE">WHERE</a> поле условие значение</p>
<p>К нашему запросу мы добавили условие фильтра, позволяющего из общей массы отобрать только нужные нам данные.</p>
<p><em>Пример:</em><br>
  <a href="http://search.mysql.com/search?site=refman-%35%31&q=SELECT">SELECT</a> name, mail <a href="http://search.mysql.com/search?site=refman-%35%31&q=FROM">FROM</a> users <a href="http://search.mysql.com/search?site=refman-%35%31&q=WHERE">WHERE</a> uid = 1</p>
<p>Данных &ldquo;хакерским&rdquo; запросом можно получить имя и email пользователя №1 системы, ограничив количество просматриваемых записей только записью, у которой uid = 1 т.е. супер-пользователя сайта.</p>
<p>Выражение <strong>WHERE</strong> довольно мощное и позволяет создавать очень сложные фильтры, и применять различные условия в зависимости от типа данных поля. Основные условия это:</p>
<ul>
  <li>знаки сравнения &gt;, &lt;, =, &gt;=(больше или равно), &lt;=(меньше или равно), &lt;&gt; или != (не равно).</li>
  <li>оператор LIKE (шаблон) для строк. В шаблоне: % - любое количество, _ - 1 символ</li>
  <li>AND, OR, NOT, XOR стандартные функции И, ИЛИ, НЕ, ОДИН ИЗ. Используйте ( ), для избежания двусмысленностей</li>
  <li>IN (список_значений) аналог множественного OR</li>
</ul>
<p><em>Примеры:</em><br>
  <a href="http://search.mysql.com/search?site=refman-%35%31&q=SELECT">SELECT</a> title <a href="http://search.mysql.com/search?site=refman-%35%31&q=FROM">FROM</a> node <a href="http://search.mysql.com/search?site=refman-%35%31&q=WHERE">WHERE</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=TYPE">type</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=IN">IN</a> (''page'', ''forum'') <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">AND</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=STATUS">status</a> = 1;</p>
<p>Выбрать заголовки материалов типов page или forum которые опубликованны. Это же выражение можно переписать в следующем виде:</p>
<p><a href="http://search.mysql.com/search?site=refman-%35%31&q=SELECT">SELECT</a> title <a href="http://search.mysql.com/search?site=refman-%35%31&q=FROM">FROM</a> node <a href="http://search.mysql.com/search?site=refman-%35%31&q=WHERE">WHERE</a> (<a href="http://search.mysql.com/search?site=refman-%35%31&q=TYPE">type</a> = ''page'' <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">OR</a>  <a href="http://search.mysql.com/search?site=refman-%35%31&q=TYPE">type</a> = ''forum'') <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">AND</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=STATUS">status</a> = 1;</p>
<p><a href="http://search.mysql.com/search?site=refman-%35%31&q=SELECT">SELECT</a> name, mail <a href="http://search.mysql.com/search?site=refman-%35%31&q=FROM">FROM</a> users <a href="http://search.mysql.com/search?site=refman-%35%31&q=WHERE">WHERE</a> (name <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">like</a> ''%NetCracker%'') <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">OR</a> (mail <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">like</a> ''%NetCracker%'');</p>
<p>Позволит просмотреть записи, у которых часть ника или email содержит комбинацию символов &lsquo;NetCracker&rsquo;.</p>
<h3 style="font-size: 18px">ORDER BY</h3>
<p><a href="http://search.mysql.com/search?site=refman-%35%31&q=ORDER%20BY">ORDER BY</a> [<a href="http://search.mysql.com/search?site=refman-%35%31&q=BINARY">BINARY</a>] имя_поля [<a href="http://search.mysql.com/search?site=refman-%35%31&q=DESC">DESC</a>]</p>
<p>Выражение в запросе позволяет отсортировать выводимые строки по заданным полям. При этом оно не влияет на получаемый результат, а только упорядочивает его вывод. Если используется выражение <strong>WHERE</strong>, то <strong>ORDER BY</strong> должно быть после него.<br>
  Необязательное <strong>BINARY</strong> позволяет осуществлять сортировку строковых данных с учётом регистра, а <strong>DESC</strong> изменить порядок сортировки на обратный.</p>
<p><em>Пример:</em><br>
  <a href="http://search.mysql.com/search?site=refman-%35%31&q=SELECT">SELECT</a> name <a href="http://search.mysql.com/search?site=refman-%35%31&q=FROM">FROM</a> users <a href="http://search.mysql.com/search?site=refman-%35%31&q=WHERE">WHERE</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=STATUS">status</a> = 1 <a href="http://search.mysql.com/search?site=refman-%35%31&q=ORDER%20BY">ORDER BY</a> access <a href="http://search.mysql.com/search?site=refman-%35%31&q=DESC">DESC</a>;</p>
<p>Вывод только ников пользователей, отсортированных по времени их последнего входа на сайт.</p>
<h3 style="font-size: 18px">LIMIT</h3>
<p><a href="http://search.mysql.com/search?site=refman-%35%31&q=LIMIT">LIMIT</a> количество, смещение  или  <a href="http://search.mysql.com/search?site=refman-%35%31&q=LIMIT">LIMIT</a> число или <a href="http://search.mysql.com/search?site=refman-%35%31&q=LIMIT">LIMIT</a> число <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">OFFSET</a> смещение</p>
<p>Данное выражение  в дополнение к WHERE позволяет ограничить количество выводимых SELECT строк заданным количеством. Если указано смещение, то заданное количество строк будет пропущено при начале отсчёта вывода.</p>
<p><em>Пример:</em><br>
  <a href="http://search.mysql.com/search?site=refman-%35%31&q=SELECT">SELECT</a> name <a href="http://search.mysql.com/search?site=refman-%35%31&q=FROM">FROM</a> users <a href="http://search.mysql.com/search?site=refman-%35%31&q=WHERE">WHERE</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=STATUS">status</a> = 1 <a href="http://search.mysql.com/search?site=refman-%35%31&q=ORDER%20BY">ORDER BY</a> access <a href="http://search.mysql.com/search?site=refman-%35%31&q=DESC">DESC</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=LIMIT">LIMIT</a> 5;</p>
<p>Вывод только 5 ников пользователей, отсортированных по времени их последнего входа на сайт.</p>
<h2 style="font-size: 18px">DELETE</h2>
<p>В отличии от команды SELECT, следующие команды приводят к изменениям данных в таблицах. Поэтому будьте внимательны к правильности их ввода. Команда DELETE используется для удаления одной или нескольких строк в таблице. Синтаксис команды прост:</p>
<p><a href="http://search.mysql.com/search?site=refman-%35%31&q=DELETE">DELETE</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=FROM">FROM</a> имя_таблиц;</p>
<p>Имена столбцов не используются, поскольку применяется для удаления целых строк а не значений из строк. Зачастую имеет смысл удалить только часть строк, по заданным критериям, поэтому в команде <strong>SELECT</strong> можно использовать инструкции <strong>WHERE</strong>, <strong>ORDER BY</strong> и <strong>LIMIT</strong>.</p>
<p><em>Пример:</em><br>
  <a href="http://search.mysql.com/search?site=refman-%35%31&q=DELETE">DELETE</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=FROM">FROM</a> users <a href="http://search.mysql.com/search?site=refman-%35%31&q=WHERE">WHERE</a> (name <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">like</a> ''%NetCracker%'') <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">OR</a> (mail <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">like</a> ''%NetCracker%'');</p>
<p>Используя данную команду, мы почистим таблицу пользователей от тех, кто содержит в любой части поля ника или email последовательность &lsquo;NetCracker&rsquo;</p>
<p>Если вам нужно выполнить полную очистку(удаление) всех данных в таблице - воспользуйтесь командой:</p>
<p><a href="http://search.mysql.com/search?site=refman-%35%31&q=TRUNCATE">TRUNCATE</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=TABLE">TABLE</a> имя_таблицы</p>
<p>При этом структура таблицы разрушается и заново создаётся. При этом все данные удаляются.</p>
<h2 style="font-size: 18px">UPDATE</h2>
<p>Данная инструкция используется для изменения данных в полях таблицы.</p>
<p><a href="http://search.mysql.com/search?site=refman-%35%31&q=UPDATE">UPDATE</a> имя_таблицы <a href="http://search.mysql.com/search?site=refman-%35%31&q=SET">SET</a> имя_поля = значение</p>
<p>Как и в предыдущей команде, тут можно использовать инструкции WHERE, ORDER BY и LIMIT, чтобы ограничить количество и диапазон затрагиваемых строк.</p>
<p><em>Пример:</em><br>
  <a href="http://search.mysql.com/search?site=refman-%35%31&q=UPDATE">UPDATE</a> users <a href="http://search.mysql.com/search?site=refman-%35%31&q=SET">SET</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=STATUS">status</a> = 0 <a href="http://search.mysql.com/search?site=refman-%35%31&q=WHERE">WHERE</a> (name <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">like</a> ''%NetCracker%'') <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">OR</a> (mail <a href="http://dev.mysql.com/doc/refman/5.1/en/non-typed-operators.html">like</a> ''%NetCracker%'');</p>
<p>В любимом нашем примере мы вместо удаления, снимем флаг активности для отфильтрованных пользователей.</p>
<h2 style="font-size: 18px">INSERT</h2>
<p>Инструкция используется для добавления новых данных в таблицу.</p>
<p>INSETR <a href="http://search.mysql.com/search?site=refman-%35%31&q=INTO">INTO</a> имя_таблицы <a href="http://search.mysql.com/search?site=refman-%35%31&q=VALUES">VALUES</a> (значения1), (значения2),...</p>
<p>Значения должны быть все (по числу полей в таблице), записаны через запятую, и их порядок должен соответствовать порядку следования столбцов в таблице. Альтернативный синтаксис:</p>
<p>INSETR <a href="http://search.mysql.com/search?site=refman-%35%31&q=INTO">INTO</a> имя_таблицы (имена_полей) <a href="http://search.mysql.com/search?site=refman-%35%31&q=VALUES">VALUES</a> (значения1), (значения2),...</p>
<p>В данном случае порядок значений для полей должен соответствовать перечисленному порядку. Пропущенные поля будут заполнены значениями по умолчанию. И ещё один тип синтаксиса:</p>
<p>INSETR <a href="http://search.mysql.com/search?site=refman-%35%31&q=INTO">INTO</a> имя_таблицы <a href="http://search.mysql.com/search?site=refman-%35%31&q=SET">SET</a> имя_поля = значение, …</p>
<p>В данном варианте необходимо указывать, через запятую, имена полей и их значения</p>
<p><em>Пример:</em><br>
  <a href="http://search.mysql.com/search?site=refman-%35%31&q=INSERT">INSERT</a> <a href="http://search.mysql.com/search?site=refman-%35%31&q=INTO">INTO</a> role <a href="http://search.mysql.com/search?site=refman-%35%31&q=VALUES">VALUES</a> (3,"Суперадмин"),(4,"Модератор");</p>
<p>Данным запросом была добавлены новые роли с именем Суперадмин и Модератор.</p>', 0, NULL);
INSERT INTO chapter_course (id, name_chapter, id_courses, text, is_stepic, id_lesson_stepic) VALUES (23, 'Типы данных', 74, '<p>Тип данных в языке структурированных запросов SQL – это атрибут, который определяет тип данных любого объекта. Каждая колонка, переменная и выражение должны относится к одному из типов данных SQL.</p>
<p>Мы используем типы данных во время создания таблиц, определяя, к какому именно из них принадлежит каждая колонка нашей таблицы.</p>
<p>Ниже приведены типы данных языка SQL, разделённые по категориям:</p>
<ul>
  <li><strong>целочисленные</strong></li>
  <li><strong>числа с плавающей точкой</strong></li>
  <li><strong>время и дата</strong></li>
  <li><strong>символы</strong></li>
  <li><strong>символы Unicode</strong></li>
  <li><strong>бинарные</strong></li>
  <li><strong>другие</strong></li>
</ul>
<hr>
<p><strong>Целочисленные типы данных<br>
</strong></p>
<table width="470" border="1" cellspacing="0">
  <tbody>
    <tr bgcolor="#E0E0E0">
      <th width="90" height="33">Тип данных</th>
      <th width="182" bgcolor="#E0E0E0">От</th>
      <th width="184">До</th>
    </tr>
    <tr>
      <td height="33">bigint</td>
      <td>-9,223,372,036,854,775,808</td>
      <td>9,223,372,036,854,775,807</td>
    </tr>
    <tr>
      <td height="30">int</td>
      <td>-2,147,483,648</td>
      <td>2,147,483,647</td>
    </tr>
    <tr>
      <td height="32">smallint</td>
      <td>-32,768</td>
      <td>32,767</td>
    </tr>
    <tr>
      <td height="28">tinyint</td>
      <td>0</td>
      <td>255</td>
    </tr>
    <tr>
      <td height="30">bit</td>
      <td>0</td>
      <td>1</td>
    </tr>
    <tr>
      <td height="33">decimal</td>
      <td>-10^38 +1</td>
      <td>10^38 -1</td>
    </tr>
    <tr>
      <td height="27">numeric</td>
      <td>-10^38 +1</td>
      <td>10^38 -1</td>
    </tr>
    <tr>
      <td height="31">money</td>
      <td>-922,337,203,685,477.5808</td>
      <td>+922,337,203,685,477.5807</td>
    </tr>
    <tr>
      <td height="34">smallmoney</td>
      <td>-214,748.3648</td>
      <td>+214,748.3647</td>
    </tr>
  </tbody>
</table>
<p> </p>
<hr>
<p><strong>Типы данных с плавающие точкой</strong></p>
<table border="1" cellspacing="0">
  <tbody>
    <tr bgcolor="#E0E0E0">
      <th>Тип данных</th>
      <th>От</th>
      <th>До</th>
    </tr>
    <tr>
      <td height="29">float</td>
      <td>-1.79E + 308</td>
      <td>1.79E + 308</td>
    </tr>
    <tr>
      <td height="30">real</td>
      <td>-3.40E + 38</td>
      <td>3.40E + 38</td>
    </tr>
  </tbody>
</table>
<p> </p>
<hr>
<p><strong>Время и дата</strong></p>
<table border="1" cellspacing="0">
  <tbody>
    <tr bgcolor="#E0E0E0">
      <th>Тип данных</th>
      <th>От</th>
      <th>До</th>
    </tr>
    <tr>
      <td height="32">datetime</td>
      <td>1 Января, 1753</td>
      <td>31 Декабря , 9999</td>
    </tr>
    <tr>
      <td height="28">smalldatetime</td>
      <td>1 Января, 1900</td>
      <td>6 Июня, 2079</td>
    </tr>
    <tr>
      <td height="29">date</td>
      <td colspan="2">Хранит дату в формате May 30, 2016</td>
    </tr>
    <tr>
      <td height="31">time</td>
      <td colspan="2">Хранит время в формате 15:30 P.M.</td>
    </tr>
  </tbody>
</table>
<p> </p>
<hr>
<p><strong>Символы</strong></p>
<table border="1" cellspacing="0">
  <tbody>
    <tr bgcolor="#E0E0E0">
      <th>Тип данных</th>
      <th>Описание</th>
    </tr>
    <tr>
      <td height="39">char</td>
      <td>Максимальная длина – 8,000 символов. (Фиксированная длниа символов, которые не входят в Unicode)</td>
    </tr>
    <tr>
      <td height="38">varchar</td>
      <td>Максимальная длина – 8,000 символов. (Изменяющаяся длина данных, не входящих в Unicode).</td>
    </tr>
    <tr>
      <td height="39">varchar(max)</td>
      <td>Максимальная длина – 231characters, Изменяющаяся длина данных, не входящих в Unicode (только для SQL Server 2005).</td>
    </tr>
    <tr>
      <td height="39">text</td>
      <td>Изменяющаяся длина данных, не входящих в Unicode с максимальной длинной – 2,147,483,647 символов.</td>
    </tr>
  </tbody>
</table>
<p> </p>
<hr>
<p><strong>Символы Unicode</strong></p>
<table border="1" cellspacing="0">
  <tbody>
    <tr bgcolor="#E0E0E0">
      <th width="103">Тип данных</th>
      <th width="663">Описание</th>
    </tr>
    <tr>
      <td height="38">nchar</td>
      <td>Максимальная длина – 4,000 символов.( Фиксированная длина Unicode)</td>
    </tr>
    <tr>
      <td height="37">nvarchar</td>
      <td>Максимальная длина – 4,000 символов.( Изменяющаяся длина Unicode)</td>
    </tr>
    <tr>
      <td height="34">nvarchar(max)</td>
      <td>Максимальная длина – 231 символ. ( Изменяющаяся длина Unicode, тольо для SQL Server 2005)</td>
    </tr>
    <tr>
      <td height="38">ntext</td>
      <td>Максимальная длина – 1,073,741,823 символов. ( Изменяющаяся длина Unicode)</td>
    </tr>
  </tbody>
</table>
<p> </p>
<hr>
<p><strong>Бинарные типы данных<br>
</strong></p>
<table border="1" cellspacing="0">
  <tbody>
    <tr bgcolor="#E0E0E0">
      <th>Тип данных</th>
      <th>Описание</th>
    </tr>
    <tr>
      <td height="38">binary</td>
      <td>Максимальная длина – 8,000 байтов.( Фиксированная длина бинарных данных )</td>
    </tr>
    <tr>
      <td height="39">varbinary</td>
      <td>Максимальная длина – 8,000 байтов.( Изменяющаяся длина бинарных данных )</td>
    </tr>
    <tr>
      <td height="38">varbinary(max)</td>
      <td>Максимальная длина – 231 байт.(Фиксированная длина бинарных данных. Только для SQL Server 2005)</td>
    </tr>
    <tr>
      <td height="37">image</td>
      <td>Максимальная длина – 2,147,483,647 байтов. ( Изменяющаяся длина бинарных данных )</td>
    </tr>
  </tbody>
</table>
<p> </p>
<hr>
<p><strong>Другие типы данных</strong></p>
<table border="1" cellspacing="0">
  <tbody>
    <tr bgcolor="#E0E0E0">
      <th width="103">Тип данных</th>
      <th width="790">Описание</th>
    </tr>
    <tr>
      <td height="36">sql_variant</td>
      <td>Хранит значения различных типов данных, поддерживаемых сервером SQL, за исключением, text, ntext и timestamp.</td>
    </tr>
    <tr>
      <td height="32">timestamp</td>
      <td>Хранит уникальное для базы данных значение, которое обновляется при каждом изменении записи.</td>
    </tr>
    <tr>
      <td height="33">uniqueidentifier</td>
      <td>Хранит глобальный уникальный идентификатор (GUID)</td>
    </tr>
    <tr>
      <td height="34">xml</td>
      <td>Хранит XML данные. Мы можем хранить экземпляр xml в колонке, либо в переменной ( Только для SQL Server 2005).</td>
    </tr>
    <tr>
      <td height="34">cursor</td>
      <td>Хранит ссылку на курсор.</td>
    </tr>
    <tr>
      <td height="40">table</td>
      <td>Хранит результирующее множество для крайней обработки.</td>
    </tr>
  </tbody>
</table>
<br>
<p><strong>Тип данных NULL</strong></p>
<p> Вообще-то это лишь условно можно назвать типом данных. По сути это скорее указатель возможности отсутствия значения. Например, когда вы регистрируетесь на каком-либо сайте, вам предлагается заполнить форму, в которой присутствуют, как обязательные, так и необязательные поля. Понятно, что регистрация пользователя невозможна без указания логина и пароля, а вот дату рождения и пол пользователь может указать по желанию. Для того, чтобы хранить такую информацию в БД и используют два значения: <br>
  <br>
  NOT NULL (значение не может отсутствовать) для полей логин и пароль,<br>
  <br>
  NULL (значение может отсутствовать) для полей дата рождения и пол.<br>
  <br>
  По умолчанию всем столбцам присваивается тип NOT NULL, поэтому его можно явно не указывать.<br>
  <br>
  <em>Пример:</em><br>
  <br>
  create table users (login varchar(20), password varchar(15), sex enum(''man'', ''woman'') NULL, date_birth date NULL); <br>
  <br>
  Таким образом, мы создаем таблицу с 4 столбцами: логин (не более 20 символов) обязательное, пароль (не более 15 символов) обязательное, пол (мужской или женский) не обязательное, дата рождения (тип дата) необязательное.<br>
  <br>
  Все, на этом урок, посвященный типам данных, закончен. У вас, возможно, остались вопросы, но они исчезнут по мере освоения дальнейшего материала, т.к. на практике все становится более понятно, чем в теории.
</p>
<p>&nbsp;</p>
<p><br>
</p>', 0, NULL);
INSERT INTO chapter_course (id, name_chapter, id_courses, text, is_stepic, id_lesson_stepic) VALUES (25, 'Введение в Git', 76, '<p>Система контроля версий (далее – СКВ) – это программное обеспечение (далее – ПО), которое помогает разработчикам работать в команде при разработке ПО и сохранять всю историю работы над проектом.</p>
<p>Ниже указаны основные функции СКВ:</p>
<ul>
  <li>Позволяет разработчикам работать над проектом одновременно</li>
  <li>Запрещает переписывать изменения друг друга</li>
  <li>Сохраняет каждую версию ПО</li>
</ul>
<p>Существует два типа СКВ:</p>
<ul>
  <li>Централизованные СКВ</li>
  <li>Распределённые (Децентрализованные) СКВ</li>
</ul>
<p>В рамках данного курса мы рассмотрим только распределённые СКВ на примере Git.</p>
<hr>
<p><strong>Распределённые СКВ (РСКВ)<br>
</strong></p>
<p>Централизованные СКВ используют центральный сервер для хранения всех файлов и позволяет команде взаимодействовать. Но данный тип СКВ подвержен большой опасности – выходу из строя центрального сервера. Если с сервером что-то происходит, то работа над проектом прекращается, так как разработчики не имеют доступа к исходному коду. А если данные на сервере утеряны, то (если нет резерва) данные теряются безвозвратно.</p>
<p>Именно поэтому, в настоящее время именно распределённые СКВ пользуются огромной популярностью.</p>
<p>Клиент РСКВ не только проверяет крайние изменения базовой директории, но также является и резервным хранилищем проекта. Если сервер выходит из строя, базовая версия может быть восстановлена с помощью клиентской машины. Таким образом Git не полагается на центральный сервер, благодаря чему мы можем работать с Git и выполнять большинство операция даже тогда, когда мы находимся не в сети. Мы можем подтверждать изменения, создавать новые ветки, просматривать историю изменений и т.д. при работе оффлайн. После этого нам необходимо только подключиться к сети и опубликовать наши крайние изменения.</p>
<hr>
<p><strong>Базовая терминология Git</strong></p>
<ul>
  <li style="line-height:1.2"><strong>Рабочая директория и плацдарм (Working Directory and Staging Area)</strong><br>
    Рабочая директория – это место, где файлы проверяются. Git не отслеживает каждый изменённый файл. Когда мы выполняем операцию <strong>commit </strong>Git ищет все файлы, которые находятся в рабочей директории. И подтверждаются только те, файлы, которые в ней находятся.<br><br>
  <li><strong>Локальный репозиторий (Local Repository)<br>
    </strong>Git предоставляет личное рабочее пространство для работы с проектом, который называется локальным репозиторием. Разработчик вносит изменения в проект в этом репозитории и, после подтверждения изменений, они становятся частью локальной копии. Пользователь может выполнить множество изменений, которые будут храниться только на его машине до тех пор, пока он не отправит их в центарльный репозиторий.<br>
    <br>
  </li>
  <li><strong>Дерево (Tree)<br>
  </strong>Дерево – это объект, который представляет собой директорию. Он хранит бинарные большие объекты как под-директории. Дерево – это бинарный файл, который хранит ссылки на, так называемые, <strong>Blobs </strong>и деревья, которые представлены SHA1 хэшем объекта дерева.<br>
  <br>
  </li>
  <li><strong>Бинарный Большой Объект  (Blob)<br>
  </strong>Binary Large Object – это файл, который представляет версию файла. Он содержит данные файла, но не хранит его метаданные. Это двоичный объект и базе данных Git он называется SHA1 хэшем файла. В Git доступ к файлам обеспечивается не по имени, а по содержанию.</li>
  <br>
  <li><strong>Коммит (Commit)<br>
</strong>Коммит хранит текущее состояние репозитория, он также называется SHA1 хэшем. Мы можем представить, что <strong>commit </strong>– это элемент связного списка. Каждый такой элемент имеет ссылку на родительский элемент. Из этого элемента мы можем откатиться назад с помощью ссылки на предыдущий элемент для того. Если у коммита есть несколько родительских коммитов, то это означает, что он был создан путём слияния двух ветвей.<br>
<br>
  </li>
  <li><strong>Клон (Clone)<br>
  </strong>Операция <strong>Clone</strong> создаёт экземпляр репозитория. Данная операция не только проверяет рабочую копию, но также становится хранилищем самого репозитория (зеркалом – mirror). Разработчик может выполнять множество операций на локальной машине, а после подключения к сети все данные могут быть синхронизированны.<br>
  <br>
  </li>
  <li><strong>Ветвь (Branch)<br>
  </strong>Ветви используются для создания отдельной версии разработки. По умолчанию, Git имеет главную ветвь, которая называется <strong>master</strong>. Когда нам необходимо реализовать какой-либо функционал, или исправить оибку, мы создаём отедльную ветвь и выполняем необходимую работу. После того, как задача выполнена, мы выполняем слияние с основной ветвью Git и удаляем ненужную ветвь. Это позволяет всегда хранить рабочую версию проекта и вносить уже полностью готовые и протестированные изменения в основную ветвь. Каждая ветвь имеет заголовок (HEAD), который является ссылкой на крайний коммит ветви. Когда мы выполняем операцию <strong>commit</strong>, HEAD обновляется.<br>
  <br>
  </li>
  <li><strong>Вытягивать (Pull)<br>
  </strong>Данная операция копирует все изменения из удалённого репозитория на локальную машину. Она используется для синхронизации локального репозитория с центральным.<br>
  <br>
  </li>
  <li><strong>Толчок (Push)</strong><br>
    Операция <strong>Push </strong>копирует изменения из локального репозитория в удалённый. Она используется для синхронизации центрального репозитория с локальным.<br>
    <br>
  </li>
  <li><strong>Пересмотр (Revision)<br>
  </strong>Revision представляет собой версию исходного кода и представлена коммитами, которые, в свою очередь, определяются SHA1 хэшами.<br>
  <br>
  </li>
  <li><strong>URL<br>
  </strong>URL представляет локацию репозитория и хранится в конфигурационном файле.<br>
  <br>
  </li>
  <li><strong>Заголовок (HEAD)<br>
  </strong>HEAD – это ссылка, которая всегда указывает на крайний коммит ветви. Когда мы выполняем операцию <strong>commit</strong>, HEAD обновляется и переводится на крайний коммит. Заголовки ветви хранятся в директории <strong>.git/refs/heads/</strong>.</li>
</ul>
', 0, NULL);
INSERT INTO chapter_course (id, name_chapter, id_courses, text, is_stepic, id_lesson_stepic) VALUES (26, 'Жизненный цикл Git', 76, '<p>В данной статье мы рассмотрим жизненный цикл Git. </p>
<p>Обычный рабочий процесс работы с Git выглядит следующим образом:</p>
<ul>
  <li>Создание локальной копии удалённого репозитория (операция <strong>Clone</strong>)</li>
  <li>Изменение локальной копии проекта (добавление/изменение/удаление файлов)</li>
  <li>Обновление локальной копии при необходимости (операция <strong>Pull</strong>)</li>
  <li>Подтверждение изменений в локальном репозитории (операция <strong>C</strong><strong>ommit</strong>)</li>
  <li>Добавление изменение из локального репозитория в центральный (операция <strong>Push</strong>)</li>
</ul>
<p>Графически данный рабочий процесс можно представить следующим образом:</p>
<p><a href="http://i2.wp.com/proselyte.net/wp-content/uploads/2016/08/life_cycle.png" rel="attachment wp-att-2328"><img src="http://i2.wp.com/proselyte.net/wp-content/uploads/2016/08/life_cycle.png?resize=700%2C627" alt="git_life_cycle" srcset="http://i2.wp.com/proselyte.net/wp-content/uploads/2016/08/life_cycle.png?resize=300%2C269 300w, http://i2.wp.com/proselyte.net/wp-content/uploads/2016/08/life_cycle.png?w=701 701w" sizes="(max-width: 700px) 100vw, 700px" width="580" height="520"></a></p>
<p>Рисунок 1. Жизненный цикл состояний файлов.</p>
<p>Итак, у вас имеется настоящий Git-репозиторий и рабочая копия файлов для некоторого проекта. Вам нужно делать некоторые изменения и фиксировать &ldquo;снимки&rdquo; состояния (snapshots) этих изменений в вашем репозитории каждый раз, когда проект достигает состояния, которое вам хотелось бы сохранить.</p>
<p>Запомните, каждый файл в вашем рабочем каталоге может находиться в одном из двух состояний: под версионным контролем (отслеживаемые) и нет (неотслеживаемые). Отслеживаемые файлы — это те файлы, которые были в последнем слепке состояния проекта (snapshot); они могут быть неизменёнными, изменёнными или подготовленными к коммиту (staged). Неотслеживаемые файлы — это всё остальное, любые файлы в вашем рабочем каталоге, которые не входили в ваш последний слепок состояния и не подготовлены к коммиту. Когда вы впервые клонируете репозиторий, все файлы будут отслеживаемыми и неизменёнными, потому что вы только взяли их из хранилища (checked them out) и ничего пока не редактировали.</p>
<p>Как только вы отредактируете файлы, Git будет рассматривать их как изменённые, т.к. вы изменили их с момента последнего коммита. Вы индексируете (stage) эти изменения и затем фиксируете все индексированные изменения, а затем цикл повторяется.</p>
<h3 id="Определение-состояния-файлов">Определение состояния файлов</a></h3>
<p>Основной инструмент, используемый для определения, какие файлы в каком состоянии находятся — это команда git status. Если вы выполните эту команду сразу после клонирования, вы увидите что-то вроде этого:</p>
<pre style="font-weight: 900">$ git status
# On branch master  nothing to commit, working directory clean  </pre>
<p>Это означает, что у вас чистый рабочий каталог, другими словами — в нём нет отслеживаемых изменённых файлов. Git также не обнаружил неотслеживаемых файлов, в противном случае они бы были перечислены здесь. И наконец, команда сообщает вам на какой ветке (branch) вы сейчас находитесь. Пока что это всегда ветка master — это ветка по умолчанию; в этой главе это не важно. В следующей главе будет подробно рассказано про ветки и ссылки.</p>
<p>Предположим, вы добавили в свой проект новый файл, простой файл README. Если этого файла раньше не было, и вы выполните git status, вы увидите свой неотслеживаемый файл вот так:</p>
<pre style="font-weight: 900">$ vim README
$ git status
# On branch master
# Untracked files:
# (use "git add &lt;file&gt;..." to include in what will be committed)
# README  nothing added to commit but untracked files present (use "git add" to track)  </pre>
<p>Понять, что новый файл README неотслеживаемый можно по тому, что он находится в секции "Untracked files" в выводе команды status. Статус "неотслеживаемый файл", по сути, означает, что Git видит файл, отсутствующий в предыдущем снимке состояния (коммите); Git не станет добавлять его в ваши коммиты, пока вы его явно об этом не попросите. Это предохранит вас от случайного добавления в репозиторий сгенерированных бинарных файлов или каких-либо других, которые вы и не думали добавлять. Мы хотели добавить README, так давайте сделаем это.</p>
<h3 id="Отслеживание-новых-файлов">Отслеживание новых файлов</h3>
<p>Для того чтобы начать отслеживать (добавить под версионный контроль) новый файл, используется команда git add. Чтобы начать отслеживание файла README, вы можете выполнить следующее:</p>
<pre style="font-weight: 900">$ git add README  </pre>
<p>Если вы снова выполните команду status, то увидите, что файл README теперь отслеживаемый и индексированный:</p>
<pre style="font-weight: 900">$ git status
# On branch master
# Changes to be committed:
# (use "git reset HEAD &lt;file&gt;..." to unstage)
# new file:   README   </pre>
<p>Вы можете видеть, что файл проиндексирован по тому, что он находится в секции &ldquo;Changes to be committed&rdquo;. Если вы выполните коммит в этот момент, то версия файла, существовавшая на момент выполнения вами команды git add, будет добавлена в историю снимков состояния. Как вы помните, когда вы ранее выполнили git init, вы затем выполнили git add (файлы) — это было сделано для того, чтобы добавить файлы в вашем каталоге под версионный контроль. Команда git add принимает параметром путь к файлу или каталогу, если это каталог, команда рекурсивно добавляет (индексирует) все файлы в данном каталоге.</p>
<h3 id="Индексация-изменённых-файлов"></h3>
<p>Давайте модифицируем файл, уже находящийся под версионным контролем. Если вы измените отслеживаемый файл benchmarks.rb и после этого снова выполните команду status, то результат будет примерно следующим:</p>
<pre style="font-weight: 900">$ git status
# On branch master
# Changes to be committed:
# (use "git reset HEAD &lt;file&gt;..." to unstage)
# new file:   README
# Changes not staged for commit:
#(use "git add &lt;file&gt;..." to update what will be committed)
#   modified:   benchmarks.rb</pre>
<p>Файл benchmarks.rb находится в секции &ldquo;Changes not staged for commit&rdquo; — это означает, что отслеживаемый файл был изменён в рабочем каталоге, но пока не проиндексирован. Чтобы проиндексировать его, необходимо выполнить команду git add (это многофункциональная команда, она используется для добавления под версионный контроль новых файлов, для индексации изменений, а также для других целей, например для указания файлов с исправленным конфликтом слияния). Выполним git add, чтобы проиндексировать benchmarks.rb, а затем снова выполним git status:</p>
<pre style="font-weight: 900">$ git add benchmarks.rb
$ git status  # On branch master
# Changes to be committed:
# (use "git reset HEAD &lt;file&gt;..." to unstage)
# new file:   README
# modified:   benchmarks.rb   </pre>
<p>Теперь оба файла проиндексированы и войдут в следующий коммит. В этот момент вы, предположим, вспомнили одно небольшое изменение, которое вы хотите сделать в benchmarks.rb до фиксации. Вы открываете файл, вносите и сохраняете необходимые изменения и вроде бы готовы к коммиту. Но давайте-ка ещё раз выполним git status:</p>
<pre style="font-weight: 900">$ vim benchmarks.rb
$ git status  # On branch master
# Changes to be committed:
# (use "git reset HEAD &lt;file&gt;..." to unstage)
# new file:   README
# modified:   benchmarks.rb
# Changes not staged for commit:
# (use "git add &lt;file&gt;..." to update what will be committed)
# modified:   benchmarks.rb  </pre>
<p>Теперь benchmarks.rb отображается как проиндексированный и непроиндексированный одновременно. Как такое возможно? Такая ситуация наглядно демонстрирует, что Git индексирует файл в точности в том состоянии, в котором он находился, когда вы выполнили команду git add. Если вы выполните коммит сейчас, то файл benchmarks.rb попадёт в коммит в том состоянии, в котором он находился, когда вы последний раз выполняли команду git add, а не в том, в котором он находится в вашем рабочем каталоге в момент выполнения git commit. Если вы изменили файл после выполнения git add, вам придётся снова выполнить git add, чтобы проиндексировать последнюю версию файла:</p>
<pre style="font-weight: 900">$ git add benchmarks.rb
$ git status  # On branch master
# Changes to be committed:
# (use "git reset HEAD &lt;file&gt;..." to unstage)
# new file:   README
# modified:   benchmarks.rb
</pre>', 0, NULL);


--
-- TOC entry 2055 (class 0 OID 18490)
-- Dependencies: 179
-- Data for Name: questions_chapter; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (41, 13, 'Логическая структура Java?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (42, 13, 'Что является экземпляром класса?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (43, 13, 'Метод класса описывает');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (46, 14, 'Какой сущностью на языке Java будут являться - человек, машина, звери?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (47, 14, 'Переменная, объявленная с помощью ключевого слова static, является:');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (48, 14, 'Переменные, которые объявляются внутри класса, но не внутри метода носят название:');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (49, 14, 'Выберите верное утверждение?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (50, 15, 'Выберите НЕ верный ответ');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (51, 15, 'Выберите тип с плавающей точкой');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (52, 15, 'Выберите символьный тип данных');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (53, 15, 'Какое значение по умолчанию имеет переменная ссылочного типа?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (54, 15, 'К какому типу относятся объекты классов?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (57, 21, 'Назначение SQL?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (58, 21, 'PL/SQL это диалект SQL, разработанный ...');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (59, 21, 'Какая команда НЕ входит в Data Manipulation Language?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (60, 21, 'Какая команда НЕ входит в Data Definition Language?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (61, 22, 'Команда Select позволяет ...?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (62, 22, 'Что можно сделать с помощью команды WHERE? ');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (63, 22, 'Какая команда производит удаление одной или несколько строк в таблице?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (64, 22, 'ORDER BY позволяет в запросе ...');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (65, 23, 'В какой момент указывается тип данных полей таблицы?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (66, 23, 'Максимальное количество символов в типе данных text?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (67, 23, 'Что хранит в себе тип cursor?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (68, 23, 'Выберите тип хранящий дату?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (69, 24, 'Подзапрос это?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (70, 24, 'Если запрос возвращает несколько записей, то для выборки определенных записей стоит использовать:');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (71, 24, 'Что называется соотнесенным запросом? ');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (72, 25, 'Какой системы контроля версий НЕ существует?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (73, 25, 'К какой системе контроля относится Git?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (74, 25, 'Назначение команды pull?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (75, 25, 'Что выполняет операция Clone?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (76, 26, 'В каком состоянии будут находится файлы только что склонированные с удаленного репозитория?  ');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (77, 26, 'С помощью какой команды можно определить состояния файла?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (78, 26, 'Для того чтобы добавить файл под версионный контроль необходимо выполнить:');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (79, 27, 'Для того чтобы зафиксировать изменения необходимо набрать');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (80, 27, 'Команда git rm позволяет:');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (81, 27, ' .gitignore позволяет?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (82, 28, 'С помощью какой команды можно вывести лог git-a?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (83, 28, 'Какой параметр в команде git log отвечает за вывод графа репозитория?');
INSERT INTO questions_chapter (id, id_chapter_course, description) VALUES (84, 28, 'Что выполняет команда git log -p?');


--
-- TOC entry 2057 (class 0 OID 18501)
-- Dependencies: 181
-- Data for Name: answer; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (23, 41, 'Объект', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (24, 41, 'Переменная', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (25, 41, 'Класс', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (26, 41, 'Метод', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (27, 42, 'Сам класс', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (28, 42, 'Интерфейс класса', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (29, 42, 'Объект', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (30, 42, 'Коллекция', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (31, 43, 'поведение', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (32, 43, 'свойства', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (33, 43, 'структуру', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (34, 43, 'ничего не описывает', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (62, 50, 'В Java есть 8 примитивных типов данных', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (63, 51, 'int', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (64, 51, 'byte', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (65, 51, 'boolean', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (66, 51, 'float', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (67, 52, 'boolean', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (43, 46, 'Объектом', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (44, 46, 'Классом', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (46, 46, 'Ничего из вышеперечисленного', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (45, 46, 'Моделью', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (47, 47, 'Переменной метода', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (48, 47, 'Переменной объекта', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (49, 47, 'Переменной класса', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (50, 47, 'Глобальной переменной', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (51, 48, 'Переменные класса', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (52, 48, 'Переменные объекта', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (53, 48, 'Локальные переменные', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (54, 48, 'Конструкторы', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (55, 49, 'Конструктор должен быть один', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (56, 49, 'Конструктор должен содержать такое же имя, что и класс', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (57, 49, 'Конструктор не должен содержать внутри себя код', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (58, 49, 'Конструктор может и не быть в классе', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (59, 50, 'При объявлении переменной резервируется определенный объем памяти', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (60, 50, 'В языке Java все типы данных делятся на две большие группы', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (61, 50, 'Объем выделяемой памяти не зависит от типа переменной', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (68, 52, 'int', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (69, 52, 'char', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (70, 52, 'double', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (71, 53, '0', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (72, 53, 'false', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (73, 53, 'true', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (74, 53, 'null', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (75, 54, 'К целочисленному', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (76, 54, 'К вещественному', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (77, 54, 'К ссылочному', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (78, 54, 'К логическому', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (87, 57, 'Управление таблицами', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (88, 57, 'Только для добавления данных в таблицы', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (89, 57, 'Для созданения, манипуляции и получения данных', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (90, 57, 'Средство связи данных с языками программирования', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (91, 58, 'Microsoft', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (92, 58, 'Google', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (93, 58, 'NetCracker', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (94, 58, 'Oracle', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (95, 59, 'SELECT', 'false', NULL);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (96, 59, 'UPDATE', 'false', NULL);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (97, 59, 'CREATE', 'true', NULL);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (98, 59, 'DELETE', 'false', NULL);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (99, 60, 'CREATE', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (100, 60, 'ALTER', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (101, 60, 'DROP', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (102, 60, 'SELECT', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (103, 61, 'Извлекать данные', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (104, 61, 'Удалять данные', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (105, 61, 'Вставлять данные', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (106, 61, 'Обновлять данные', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (107, 62, 'Задать условие выборки', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (108, 62, 'Задать условие вставки', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (109, 62, 'Получить все записи в таблице', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (110, 62, 'Получить всю базу', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (111, 63, 'Select', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (112, 63, 'Insert', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (113, 63, 'Order by', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (114, 63, 'Delete', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (115, 64, 'Позволяет ограничить количество выводимых строк', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (116, 64, 'Позволяет отсортировать выводимые строки по заданным полям', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (117, 64, 'Позволяет выставить условие вывода строк', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (118, 64, 'ничего из вышеперечисленного', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (119, 65, 'В момент первой вставки данных в таблицу', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (120, 65, 'При создании таблицы', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (121, 65, 'СУБД сама определяет тип вставляемых данных', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (122, 65, 'Тип данных всегда постоянен и является text', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (123, 66, '2,147,483,647', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (124, 66, ' 8,000', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (125, 66, '231', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (126, 66, 'Не имеет предела ', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (127, 67, 'Хранит глобальный уникальный идентификатор', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (128, 67, 'Хранит ссылку на курсор.', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (129, 67, 'Хранит уникальное для базы данных значение, которое обновляется при каждом изменении записи.', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (130, 67, 'Хранит XML данные. ', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (131, 68, 'datetime', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (132, 68, 'smalldatetime', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (133, 68, 'date', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (134, 68, 'Все вышеперечисленное ', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (135, 69, 'Последовательность запросов', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (136, 69, 'Запрос на вставку другого запроса', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (137, 69, 'Запрос внутри запроса', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (138, 69, 'Ничего из вышеперечисленного', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (139, 70, 'IN, NOT IN', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (140, 70, 'UPDATE', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (141, 70, 'DELETE', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (142, 70, 'CREATE', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (143, 71, 'Любой подзапрос можно назвать соотнесенным запросом', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (144, 71, 'Запрос называется соотнесенным, когда  внутренний и внешний запрос взаимозависимы', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (145, 71, 'Внешний запрос по отношению к внутреннему запросу называется соотнесенный запрос', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (146, 71, 'Внутренний запрос по отношению к внешнему запросу называется соотнесенный запрос', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (147, 72, 'Централизованной', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (148, 72, 'Распределённой', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (149, 72, 'Локальной', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (150, 72, 'Глобальной', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (151, 73, 'Централизованной', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (152, 73, 'Распределённой', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (153, 73, 'Глобальной', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (154, 73, 'Децентрализованой ', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (155, 74, 'Копирует изменения из локального репозитория в удалённый', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (156, 74, 'анная операция копирует все изменения из удалённого репозитория на локальную машину.', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (157, 74, 'Используются для создания отдельной версии разработки', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (158, 74, 'Создаёт экземпляр репозитория', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (159, 75, 'Копирует изменения из локального репозитория в удалённый', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (160, 75, 'Хранит текущее состояние репозитория', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (161, 75, 'Создаёт экземпляр репозитория', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (162, 75, 'Все вышеперечисленное ', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (163, 76, 'отслеживаемыми и изменёнными', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (164, 76, 'Неотслеживаемыми и неизменёнными', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (165, 76, 'отслеживаемыми и неизменёнными', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (166, 76, 'Неотслеживаемыми и изменёнными', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (167, 77, 'git add', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (168, 77, 'git push', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (169, 77, 'git commit', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (170, 77, 'git status ', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (171, 78, 'git commit', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (172, 78, 'git remove', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (173, 78, 'git status', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (174, 78, 'git add', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (175, 79, 'git push', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (176, 79, 'git commit', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (177, 79, 'git add', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (178, 79, 'git status', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (179, 80, 'Зафиксировать изменения без индексации файла', 'false', NULL);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (180, 80, 'Обновить индексируемый файл', 'false', NULL);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (181, 80, 'Удалить индексируемый файл из каталога', 'true', NULL);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (182, 80, 'Зафиксировать, как отслеживаемый файл', 'false', NULL);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (183, 81, 'Обозначить файлы, которые не будут регистрироваться git', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (184, 81, 'Убрать локальный репозиторий', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (185, 81, 'Указать группу людей, которые не смогут получить доступ к файлам', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (186, 81, 'Все вышеперечисленное ', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (187, 82, 'git branch', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (188, 82, 'git diff', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (189, 82, 'git log', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (190, 82, 'git checkout', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (191, 83, ' --all ', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (192, 83, '--oneline', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (193, 83, '--graph', 'true', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (194, 83, '--decorate', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (195, 84, 'Позволяет редактировать лог коммитов', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (196, 84, 'Создает новый коммит и показывает все последние изменения', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (197, 84, 'список коммитов с последующем очищением истории', 'false', 0);
INSERT INTO answer (id, id_question_chapter, answer, rights, is_select) VALUES (198, 84, 'список коммитов и показываем diff для каждого', 'true', 0);


--
-- TOC entry 2071 (class 0 OID 0)
-- Dependencies: 180
-- Name: answer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('answer_id_seq', 198, true);


--
-- TOC entry 2072 (class 0 OID 0)
-- Dependencies: 176
-- Name: chapter_course_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('chapter_course_id_seq', 28, true);


--
-- TOC entry 2073 (class 0 OID 0)
-- Dependencies: 174
-- Name: courses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('courses_id_seq', 76, true);


--
-- TOC entry 2074 (class 0 OID 0)
-- Dependencies: 178
-- Name: questions_chapter_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('questions_chapter_id_seq', 84, true);


--
-- TOC entry 2061 (class 0 OID 18520)
-- Dependencies: 185
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO role (id, role) VALUES (1, 'user');
INSERT INTO role (id, role) VALUES (2, 'admin');


--
-- TOC entry 2047 (class 0 OID 18449)
-- Dependencies: 171
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO users (id, id_user_stepic, login, firstname, secondname, mail, pass, id_role) VALUES (16, NULL, 'admin', 'Артемий', 'Воробьев', 'geroflex@mail.ru', '$2a$11$cE9EpNPRsxVG6.utQEh.P.BdYNltLTTjbVZd7UoUTkTiNygbIYGq6', 2);
INSERT INTO users (id, id_user_stepic, login, firstname, secondname, mail, pass, id_role) VALUES (7, NULL, 'user21', 'Artemiy', 'NetCracker', 'artem@mail.ru', '1234561111', 1);
INSERT INTO users (id, id_user_stepic, login, firstname, secondname, mail, pass, id_role) VALUES (6, NULL, 'user123', 'Vorobev', 'Artem', 'Vorobev@mail.ru', '123456789', 1);
INSERT INTO users (id, id_user_stepic, login, firstname, secondname, mail, pass, id_role) VALUES (19, NULL, 'User121', 'User', 'User', 'User', '$2a$11$AkyRqm1lj2XqW/BXNm0jhuMT0Cnnxu3tMKymQ/mi3ws7tp801eZ8a', 1);
INSERT INTO users (id, id_user_stepic, login, firstname, secondname, mail, pass, id_role) VALUES (5, NULL, 'NetCracker', 'Artemiy', 'Vorobev', 'artem2003raven@mail.ru', '123456789', 1);
INSERT INTO users (id, id_user_stepic, login, firstname, secondname, mail, pass, id_role) VALUES (13, NULL, 'Artem', 'Артемий', 'Воробьев', 'geroflex@mail.ru', '123456', 1);
INSERT INTO users (id, id_user_stepic, login, firstname, secondname, mail, pass, id_role) VALUES (20, NULL, 'User', 'Userrovich', 'User', 'geroflex@mail.ru', '$2a$11$cE9EpNPRsxVG6.utQEh.P.BdYNltLTTjbVZd7UoUTkTiNygbIYGq6', 1);


--
-- TOC entry 2059 (class 0 OID 18512)
-- Dependencies: 183
-- Data for Name: remind; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO remind (id, result_test, id_chapter_course, data_remind, id_user, is_display, is_send_mail, data_pass) VALUES (13, '100,00', 13, '2016-12-11 19:36:08.84', 16, 1, 1, '12.11.2016 в 19:31');


--
-- TOC entry 2075 (class 0 OID 0)
-- Dependencies: 182
-- Name: remind_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('remind_id_seq', 13, true);


--
-- TOC entry 2076 (class 0 OID 0)
-- Dependencies: 184
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('role_id_seq', 10, true);


--
-- TOC entry 2064 (class 0 OID 18534)
-- Dependencies: 188
-- Data for Name: step; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2066 (class 0 OID 18548)
-- Dependencies: 190
-- Data for Name: stepic_result_question; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2065 (class 0 OID 18542)
-- Dependencies: 189
-- Data for Name: stepic_token; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2063 (class 0 OID 18528)
-- Dependencies: 187
-- Data for Name: type_answer; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- TOC entry 2077 (class 0 OID 0)
-- Dependencies: 186
-- Name: type_answer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('type_answer_id_seq', 1, false);


--
-- TOC entry 2049 (class 0 OID 18460)
-- Dependencies: 173
-- Data for Name: user_course; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO user_course (id, id_user, id_chapter_course, start_course, finish_course) VALUES (6, 16, 13, '2016-12-10 14:28:12.473', NULL);
INSERT INTO user_course (id, id_user, id_chapter_course, start_course, finish_course) VALUES (7, 16, 14, '2016-12-11 17:30:24.49', NULL);


--
-- TOC entry 2078 (class 0 OID 0)
-- Dependencies: 172
-- Name: user_course_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('user_course_id_seq', 7, true);


--
-- TOC entry 2079 (class 0 OID 0)
-- Dependencies: 170
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 22, true);


-- Completed on 2016-12-11 23:24:08

--
-- PostgreSQL database dump complete
--
