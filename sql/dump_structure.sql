--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.3
-- Dumped by pg_dump version 9.3.3
-- Started on 2016-12-12 23:20:38

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

ALTER TABLE ONLY public.answer DROP CONSTRAINT fk_var_answer_questions_chapter_1;
ALTER TABLE ONLY public.users DROP CONSTRAINT fk_users_role_1;
ALTER TABLE ONLY public.user_course DROP CONSTRAINT fk_user_course_users_1;
ALTER TABLE ONLY public.user_course DROP CONSTRAINT fk_user_course_chapter_course_1;
ALTER TABLE ONLY public.type_answer DROP CONSTRAINT fk_type_answer_answer_1;
ALTER TABLE ONLY public.submission DROP CONSTRAINT fk_submission_attempt;
ALTER TABLE ONLY public.step DROP CONSTRAINT fk_steps_chapter_course_1;
ALTER TABLE ONLY public.stepic_token DROP CONSTRAINT "fk_stepicToken_users_1";
ALTER TABLE ONLY public.remind DROP CONSTRAINT fk_remind_chapter_course_1;
ALTER TABLE ONLY public.questions_chapter DROP CONSTRAINT fk_questions_chapter_chapter_course_1;
ALTER TABLE ONLY public.chapter_course DROP CONSTRAINT fk_chapter_course_courses_1;
ALTER TABLE ONLY public.attempt DROP CONSTRAINT fk_attempt_users;
ALTER TABLE ONLY public.attempt DROP CONSTRAINT fk_attempt_step;
ALTER TABLE ONLY public.stepic_result_question DROP CONSTRAINT "fk_Table 1_users_1";
DROP INDEX public.answer_id_question_chapter_idx;
ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
ALTER TABLE ONLY public.user_course DROP CONSTRAINT user_course_pkey;
ALTER TABLE ONLY public.type_answer DROP CONSTRAINT type_answer_pkey;
ALTER TABLE ONLY public.submission DROP CONSTRAINT submission_pkey;
ALTER TABLE ONLY public.stepic_result_question DROP CONSTRAINT stepic_result_question_pkey;
ALTER TABLE ONLY public.stepic_result_question DROP CONSTRAINT stepic_result_question_id_step_key;
ALTER TABLE ONLY public.step DROP CONSTRAINT step_pkey;
ALTER TABLE ONLY public.role DROP CONSTRAINT role_pkey;
ALTER TABLE ONLY public.remind DROP CONSTRAINT remind_pkey;
ALTER TABLE ONLY public.questions_chapter DROP CONSTRAINT questions_chapter_pkey;
ALTER TABLE ONLY public.courses DROP CONSTRAINT courses_pkey;
ALTER TABLE ONLY public.chapter_course DROP CONSTRAINT chapter_course_pkey;
ALTER TABLE ONLY public.attempt DROP CONSTRAINT attempt_pkey;
ALTER TABLE ONLY public.answer DROP CONSTRAINT answer_pkey;
ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.user_course ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.type_answer ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.submission ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.stepic_token ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.stepic_result_question ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.step ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.role ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.remind ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.questions_chapter ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.courses ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.chapter_course ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.attempt ALTER COLUMN id DROP DEFAULT;
ALTER TABLE public.answer ALTER COLUMN id DROP DEFAULT;
DROP SEQUENCE public.users_id_seq;
DROP TABLE public.users;
DROP SEQUENCE public.user_course_id_seq;
DROP TABLE public.user_course;
DROP SEQUENCE public.type_answer_id_seq;
DROP TABLE public.type_answer;
DROP SEQUENCE public.submission_id_seq;
DROP TABLE public.submission;
DROP SEQUENCE public.stepic_token_id_seq;
DROP TABLE public.stepic_token;
DROP SEQUENCE public.stepic_result_question_id_seq;
DROP TABLE public.stepic_result_question;
DROP SEQUENCE public.step_id_seq;
DROP TABLE public.step;
DROP SEQUENCE public.role_id_seq;
DROP TABLE public.role;
DROP SEQUENCE public.remind_id_seq;
DROP TABLE public.remind;
DROP SEQUENCE public.questions_chapter_id_seq;
DROP TABLE public.questions_chapter;
DROP SEQUENCE public.courses_id_seq;
DROP TABLE public.courses;
DROP SEQUENCE public.chapter_course_id_seq;
DROP TABLE public.chapter_course;
DROP SEQUENCE public.attempt_id_seq;
DROP TABLE public.attempt;
DROP SEQUENCE public.answer_id_seq;
DROP TABLE public.answer;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- TOC entry 6 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2080 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 198 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2082 (class 0 OID 0)
-- Dependencies: 198
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 19470)
-- Name: answer; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE answer (
    id bigint NOT NULL,
    id_question_chapter integer NOT NULL,
    answer text NOT NULL,
    rights text NOT NULL,
    is_select smallint DEFAULT 0
);


ALTER TABLE public.answer OWNER TO postgres;

--
-- TOC entry 180 (class 1259 OID 19468)
-- Name: answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.answer_id_seq OWNER TO postgres;

--
-- TOC entry 2083 (class 0 OID 0)
-- Dependencies: 180
-- Name: answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE answer_id_seq OWNED BY answer.id;


--
-- TOC entry 195 (class 1259 OID 19543)
-- Name: attempt; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE attempt (
    id integer NOT NULL,
    id_stepic integer NOT NULL,
    id_step integer NOT NULL,
    id_user integer NOT NULL
);


ALTER TABLE public.attempt OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 19541)
-- Name: attempt_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE attempt_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attempt_id_seq OWNER TO postgres;

--
-- TOC entry 2084 (class 0 OID 0)
-- Dependencies: 194
-- Name: attempt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE attempt_id_seq OWNED BY attempt.id;


--
-- TOC entry 177 (class 1259 OID 19448)
-- Name: chapter_course; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE chapter_course (
    id integer NOT NULL,
    name_chapter text NOT NULL,
    id_courses integer NOT NULL,
    text text,
    is_stepic integer NOT NULL,
    id_lesson_stepic bigint
);


ALTER TABLE public.chapter_course OWNER TO postgres;

--
-- TOC entry 176 (class 1259 OID 19446)
-- Name: chapter_course_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE chapter_course_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chapter_course_id_seq OWNER TO postgres;

--
-- TOC entry 2085 (class 0 OID 0)
-- Dependencies: 176
-- Name: chapter_course_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE chapter_course_id_seq OWNED BY chapter_course.id;


--
-- TOC entry 175 (class 1259 OID 19437)
-- Name: courses; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE courses (
    id bigint NOT NULL,
    name_course text NOT NULL,
    description text,
    id_stepic bigint,
    image text
);


ALTER TABLE public.courses OWNER TO postgres;

--
-- TOC entry 174 (class 1259 OID 19435)
-- Name: courses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE courses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.courses_id_seq OWNER TO postgres;

--
-- TOC entry 2086 (class 0 OID 0)
-- Dependencies: 174
-- Name: courses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE courses_id_seq OWNED BY courses.id;


--
-- TOC entry 179 (class 1259 OID 19459)
-- Name: questions_chapter; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE questions_chapter (
    id bigint NOT NULL,
    id_chapter_course bigint,
    description text NOT NULL
);


ALTER TABLE public.questions_chapter OWNER TO postgres;

--
-- TOC entry 178 (class 1259 OID 19457)
-- Name: questions_chapter_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE questions_chapter_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.questions_chapter_id_seq OWNER TO postgres;

--
-- TOC entry 2087 (class 0 OID 0)
-- Dependencies: 178
-- Name: questions_chapter_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE questions_chapter_id_seq OWNED BY questions_chapter.id;


--
-- TOC entry 183 (class 1259 OID 19483)
-- Name: remind; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE remind (
    id integer NOT NULL,
    result_test character varying(255),
    id_chapter_course integer,
    data_remind timestamp without time zone,
    id_user integer,
    is_display integer,
    is_send_mail smallint,
    data_pass text NOT NULL
);


ALTER TABLE public.remind OWNER TO postgres;

--
-- TOC entry 182 (class 1259 OID 19481)
-- Name: remind_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE remind_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.remind_id_seq OWNER TO postgres;

--
-- TOC entry 2088 (class 0 OID 0)
-- Dependencies: 182
-- Name: remind_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE remind_id_seq OWNED BY remind.id;


--
-- TOC entry 185 (class 1259 OID 19494)
-- Name: role; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE role (
    id integer NOT NULL,
    role character varying(15) NOT NULL
);


ALTER TABLE public.role OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 19492)
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.role_id_seq OWNER TO postgres;

--
-- TOC entry 2089 (class 0 OID 0)
-- Dependencies: 184
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- TOC entry 189 (class 1259 OID 19510)
-- Name: step; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE step (
    id integer NOT NULL,
    id_stepic integer,
    id_chapter integer NOT NULL,
    number_in_section integer NOT NULL,
    description text NOT NULL,
    name_section text NOT NULL,
    name_step text NOT NULL,
    is_question character varying(255)
);


ALTER TABLE public.step OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 19508)
-- Name: step_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE step_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.step_id_seq OWNER TO postgres;

--
-- TOC entry 2090 (class 0 OID 0)
-- Dependencies: 188
-- Name: step_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE step_id_seq OWNED BY step.id;


--
-- TOC entry 193 (class 1259 OID 19530)
-- Name: stepic_result_question; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE stepic_result_question (
    id integer NOT NULL,
    id_user bigint NOT NULL,
    id_step bigint NOT NULL,
    result text NOT NULL,
    time_step timestamp without time zone NOT NULL,
    id_chapter bigint NOT NULL
);


ALTER TABLE public.stepic_result_question OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 19528)
-- Name: stepic_result_question_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stepic_result_question_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stepic_result_question_id_seq OWNER TO postgres;

--
-- TOC entry 2091 (class 0 OID 0)
-- Dependencies: 192
-- Name: stepic_result_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stepic_result_question_id_seq OWNED BY stepic_result_question.id;


--
-- TOC entry 191 (class 1259 OID 19521)
-- Name: stepic_token; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE stepic_token (
    id integer NOT NULL,
    id_user integer NOT NULL,
    access_token text NOT NULL,
    refresh_token text,
    get_time timestamp without time zone NOT NULL,
    expired_time timestamp without time zone,
    scope text,
    token_type text,
    raw_response text
);


ALTER TABLE public.stepic_token OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 19519)
-- Name: stepic_token_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE stepic_token_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.stepic_token_id_seq OWNER TO postgres;

--
-- TOC entry 2092 (class 0 OID 0)
-- Dependencies: 190
-- Name: stepic_token_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE stepic_token_id_seq OWNED BY stepic_token.id;


--
-- TOC entry 197 (class 1259 OID 19551)
-- Name: submission; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE submission (
    id integer NOT NULL,
    id_stepic integer NOT NULL,
    id_attempt integer NOT NULL,
    is_correct character varying(15) NOT NULL,
    timestamp timestamp NOT NULL
);


ALTER TABLE public.submission OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 19549)
-- Name: submission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE submission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.submission_id_seq OWNER TO postgres;

--
-- TOC entry 2093 (class 0 OID 0)
-- Dependencies: 196
-- Name: submission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE submission_id_seq OWNED BY submission.id;


--
-- TOC entry 187 (class 1259 OID 19502)
-- Name: type_answer; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE type_answer (
    id integer NOT NULL,
    id_answer integer NOT NULL,
    is_writing boolean NOT NULL
);


ALTER TABLE public.type_answer OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 19500)
-- Name: type_answer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE type_answer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.type_answer_id_seq OWNER TO postgres;

--
-- TOC entry 2094 (class 0 OID 0)
-- Dependencies: 186
-- Name: type_answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE type_answer_id_seq OWNED BY type_answer.id;


--
-- TOC entry 173 (class 1259 OID 19429)
-- Name: user_course; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE user_course (
    id integer NOT NULL,
    id_user bigint NOT NULL,
    id_chapter_course integer NOT NULL,
    start_course timestamp without time zone,
    finish_course timestamp without time zone
);


ALTER TABLE public.user_course OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 19427)
-- Name: user_course_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE user_course_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_course_id_seq OWNER TO postgres;

--
-- TOC entry 2095 (class 0 OID 0)
-- Dependencies: 172
-- Name: user_course_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE user_course_id_seq OWNED BY user_course.id;


--
-- TOC entry 171 (class 1259 OID 19418)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    id_user_stepic integer,
    login character varying(10) NOT NULL,
    firstname text NOT NULL,
    secondname text NOT NULL,
    mail text NOT NULL,
    pass text,
    id_role integer NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 19416)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2096 (class 0 OID 0)
-- Dependencies: 170
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 1915 (class 2604 OID 19473)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY answer ALTER COLUMN id SET DEFAULT nextval('answer_id_seq'::regclass);


--
-- TOC entry 1923 (class 2604 OID 19546)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY attempt ALTER COLUMN id SET DEFAULT nextval('attempt_id_seq'::regclass);


--
-- TOC entry 1913 (class 2604 OID 19451)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY chapter_course ALTER COLUMN id SET DEFAULT nextval('chapter_course_id_seq'::regclass);


--
-- TOC entry 1912 (class 2604 OID 19440)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY courses ALTER COLUMN id SET DEFAULT nextval('courses_id_seq'::regclass);


--
-- TOC entry 1914 (class 2604 OID 19462)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questions_chapter ALTER COLUMN id SET DEFAULT nextval('questions_chapter_id_seq'::regclass);


--
-- TOC entry 1917 (class 2604 OID 19486)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY remind ALTER COLUMN id SET DEFAULT nextval('remind_id_seq'::regclass);


--
-- TOC entry 1918 (class 2604 OID 19497)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- TOC entry 1920 (class 2604 OID 19513)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY step ALTER COLUMN id SET DEFAULT nextval('step_id_seq'::regclass);


--
-- TOC entry 1922 (class 2604 OID 19533)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stepic_result_question ALTER COLUMN id SET DEFAULT nextval('stepic_result_question_id_seq'::regclass);


--
-- TOC entry 1921 (class 2604 OID 19524)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stepic_token ALTER COLUMN id SET DEFAULT nextval('stepic_token_id_seq'::regclass);


--
-- TOC entry 1924 (class 2604 OID 19554)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY submission ALTER COLUMN id SET DEFAULT nextval('submission_id_seq'::regclass);


--
-- TOC entry 1919 (class 2604 OID 19505)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY type_answer ALTER COLUMN id SET DEFAULT nextval('type_answer_id_seq'::regclass);


--
-- TOC entry 1911 (class 2604 OID 19432)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_course ALTER COLUMN id SET DEFAULT nextval('user_course_id_seq'::regclass);


--
-- TOC entry 1910 (class 2604 OID 19421)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 1937 (class 2606 OID 19479)
-- Name: answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY answer
    ADD CONSTRAINT answer_pkey PRIMARY KEY (id);


--
-- TOC entry 1951 (class 2606 OID 19548)
-- Name: attempt_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY attempt
    ADD CONSTRAINT attempt_pkey PRIMARY KEY (id);


--
-- TOC entry 1932 (class 2606 OID 19456)
-- Name: chapter_course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY chapter_course
    ADD CONSTRAINT chapter_course_pkey PRIMARY KEY (id);


--
-- TOC entry 1930 (class 2606 OID 19445)
-- Name: courses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY courses
    ADD CONSTRAINT courses_pkey PRIMARY KEY (id);


--
-- TOC entry 1934 (class 2606 OID 19467)
-- Name: questions_chapter_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY questions_chapter
    ADD CONSTRAINT questions_chapter_pkey PRIMARY KEY (id);


--
-- TOC entry 1939 (class 2606 OID 19491)
-- Name: remind_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY remind
    ADD CONSTRAINT remind_pkey PRIMARY KEY (id);


--
-- TOC entry 1941 (class 2606 OID 19499)
-- Name: role_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 1945 (class 2606 OID 19518)
-- Name: step_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY step
    ADD CONSTRAINT step_pkey PRIMARY KEY (id);


--
-- TOC entry 1947 (class 2606 OID 19540)
-- Name: stepic_result_question_id_step_key; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY stepic_result_question
    ADD CONSTRAINT stepic_result_question_id_step_key UNIQUE (id_step);


--
-- TOC entry 1949 (class 2606 OID 19538)
-- Name: stepic_result_question_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY stepic_result_question
    ADD CONSTRAINT stepic_result_question_pkey PRIMARY KEY (id);


--
-- TOC entry 1953 (class 2606 OID 19556)
-- Name: submission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY submission
    ADD CONSTRAINT submission_pkey PRIMARY KEY (id);


--
-- TOC entry 1943 (class 2606 OID 19507)
-- Name: type_answer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY type_answer
    ADD CONSTRAINT type_answer_pkey PRIMARY KEY (id);


--
-- TOC entry 1928 (class 2606 OID 19434)
-- Name: user_course_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY user_course
    ADD CONSTRAINT user_course_pkey PRIMARY KEY (id);


--
-- TOC entry 1926 (class 2606 OID 19426)
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 1935 (class 1259 OID 19480)
-- Name: answer_id_question_chapter_idx; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX answer_id_question_chapter_idx ON answer USING btree (id_question_chapter);


--
-- TOC entry 1964 (class 2606 OID 19607)
-- Name: fk_Table 1_users_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stepic_result_question
    ADD CONSTRAINT "fk_Table 1_users_1" FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1966 (class 2606 OID 19617)
-- Name: fk_attempt_step; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY attempt
    ADD CONSTRAINT fk_attempt_step FOREIGN KEY (id_step) REFERENCES step(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1965 (class 2606 OID 19612)
-- Name: fk_attempt_users; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY attempt
    ADD CONSTRAINT fk_attempt_users FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1957 (class 2606 OID 19582)
-- Name: fk_chapter_course_courses_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY chapter_course
    ADD CONSTRAINT fk_chapter_course_courses_1 FOREIGN KEY (id_courses) REFERENCES courses(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1958 (class 2606 OID 19587)
-- Name: fk_questions_chapter_chapter_course_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY questions_chapter
    ADD CONSTRAINT fk_questions_chapter_chapter_course_1 FOREIGN KEY (id_chapter_course) REFERENCES chapter_course(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1960 (class 2606 OID 19567)
-- Name: fk_remind_chapter_course_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY remind
    ADD CONSTRAINT fk_remind_chapter_course_1 FOREIGN KEY (id_chapter_course) REFERENCES chapter_course(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1963 (class 2606 OID 19602)
-- Name: fk_stepicToken_users_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY stepic_token
    ADD CONSTRAINT "fk_stepicToken_users_1" FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1962 (class 2606 OID 19597)
-- Name: fk_steps_chapter_course_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY step
    ADD CONSTRAINT fk_steps_chapter_course_1 FOREIGN KEY (id_chapter) REFERENCES chapter_course(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1967 (class 2606 OID 19622)
-- Name: fk_submission_attempt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY submission
    ADD CONSTRAINT fk_submission_attempt FOREIGN KEY (id_attempt) REFERENCES attempt(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1961 (class 2606 OID 19592)
-- Name: fk_type_answer_answer_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY type_answer
    ADD CONSTRAINT fk_type_answer_answer_1 FOREIGN KEY (id_answer) REFERENCES answer(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1955 (class 2606 OID 19562)
-- Name: fk_user_course_chapter_course_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_course
    ADD CONSTRAINT fk_user_course_chapter_course_1 FOREIGN KEY (id_user) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1956 (class 2606 OID 19572)
-- Name: fk_user_course_users_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY user_course
    ADD CONSTRAINT fk_user_course_users_1 FOREIGN KEY (id_chapter_course) REFERENCES chapter_course(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1954 (class 2606 OID 19577)
-- Name: fk_users_role_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT fk_users_role_1 FOREIGN KEY (id_role) REFERENCES role(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 1959 (class 2606 OID 19557)
-- Name: fk_var_answer_questions_chapter_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY answer
    ADD CONSTRAINT fk_var_answer_questions_chapter_1 FOREIGN KEY (id_question_chapter) REFERENCES questions_chapter(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 2081 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-12-12 23:20:39

--
-- PostgreSQL database dump complete
--

