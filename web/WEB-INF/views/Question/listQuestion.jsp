<%-- Страница на которой выводятся список вопросов к главе, а так же имеется кнопка создания вопроса--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page session="true" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Список вопросов</title>
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
    <script src="/resources/js/checkbox.js"></script>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">
    <%--<body background="/resources/jpg/addChapter.jpg"/>--%>
</head>
<body>

<div class="container">

    <div class="row">
        <div class="col-md-12" align="center">
            <h3>Список вопросов к теме:</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Вопрос</th>
                    <th>Действие</th>
                </tr>
                </thead>
                <% Integer count = 1; %>
                <c:forEach items="${questionChapter}" var="questionChapter">
                    <spring:url
                            value="/admin/course/${questionChapter.idChapterCourse}/answer/${questionChapter.id}/info"
                            var="info" htmlEscape="true"/>
                    <tbody>
                    <tr>
                        <td><%= count++ %>
                        </td>
                            <%--<td>John</td>--%>
                        <td><a href="${info}">${questionChapter.description}</a></td>
                            <%--<td>john@example.com</td>--%>
                        <td>
                            <spring:url
                                    value="/admin/course/${questionChapter.idChapterCourse}/answer/${questionChapter.id}/delete"
                                    var="delete"/>
                            <a href="${delete}" class="btn btn btn-danger btn-sm" role="button"> Удалить</a>
                        </td>
                    </tr>
                    </tbody>
                </c:forEach>
            </table>
            <div class="row">
                <div class="col-md-offset-3">
                    <spring:url value="/admin/course/${idCourse}"
                                var="backCourse"/>
                    <a href="${backCourse}" class="btn btn btn-info btn-md" role="button"> Назад к курсу</a>
                </div>
            </div>
        </div>

        <div class="col-md-6" style="border:solid 1px black;">
            <div class="row">
                <div class="col-md-12" align="left">
                    <h4> Добавить вопрос: </h4>
                    <div class="row">
                        <div class="col-md col-md-offset-0" align="left">
                            <font size="4"> Текст вопроса: </font>
                        </div>
                        <div class="row">
                            <div class="col-md-7 col-md-offset-1">
                                <spring:url value="/admin/course/${idChapter}/answer/add"
                                            var="addAnswer"/>
                                <form:form action="addAnswer" method="POST" modelAttribute="listNewAnswer">
                                <form:textarea cols="60" path="textQuestion"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4" align="left">
                                <font size="4"> Ответы: </font>
                            </div>
                            <div class="col-md-1 col-md-offset-4" align="left">
                                <font size="4">true</font>
                            </div>
                            <div class="col-md-1 col-md-offset-1" align="left">
                                <font size="4">false</font>
                            </div>
                        </div>

                        <c:set var="num" value="1"/>
                        <c:forEach items="${listNewAnswer.answerList}" varStatus="i">
                            <div class="row">
                                <div class="col-md-1 col-xs-offset-1">
                                        ${i.count})
                                </div>
                                <div class="col-md-5 col-xs-offset-1">
                                    <form:input type="text" path="answerList[${i.index}].answer" style="width: 200px;"/>
                                </div>
                                <div class="col-md-1">
                                    <form:checkbox value="true" onclick="check${num}()" id="myCheck${num}"
                                                   path="answerList[${i.index}].right"/>
                                </div>
                                <c:set var="num" value="${num+1}"/>
                                <div class="col-md-1 col-xs-offset-1">
                                    <form:checkbox value="false" onclick="check${num}()" id="myCheck${num}"
                                                   path="answerList[${i.index}].right"/>
                                </div>
                            </div>
                            <br>
                            <c:set var="num" value="${num+1}"/>
                        </c:forEach>
                        <div class="row">
                            <div class="col-md-2 col-md-offset-3" style="padding-left: 2.1cm">
                                <input type="submit" value="Сохранить">
                            </div>
                        </div>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
>
</body>
</html>