<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page session="true"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Описание темы</title>
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">
<%--<body background="/resources/jpg/addChapter.jpg"/>--%>

    <spring:url value="/admin/course/${chapter.id}/info/update/" var="url"/>
    <spring:url value="/admin/course/${course.id}" var="back" htmlEscape="true"/>
</head>

<body>
<div><h3 align="center">Тема курса:${chapter.title}, курс: ${course.nameCourse}</h3></div>
 <br>
<table width="100%">

    <form:form action="${url}" commandName="chapter" method="post">

        <div class="row">
            <div class="col-md-2" align="right">
                <form:label path="title">Название темы:</form:label>
            </div>
            <div class="col-md-4" align="left">
                <form:input type="text" class="form-control" path="title"/>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-2" align="right">
            <form:label  path="text">Описание темы</form:label>
            </div>
            <div class="col-md-4" align="left">
                <form:textarea cssStyle="width: 29cm" type="text" class="form-control" path="text" placeholder="текст темы"/>
            </div>
        </div>
          <br>
        <div class="row">

            <div class="col-md-2 col-md-offset-2">
                <input type="submit" value="Сохранить изменения">
            </div>
         </div>
        <form:hidden path="idCourse"/>
    </form:form>

</table>

</body>
</html>
