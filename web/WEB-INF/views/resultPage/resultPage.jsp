<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="nc.project.Hibernate.model.Remind" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%--LOGOUT--%>
    <script>
        function formSubmit() {
            document.getElementById("logoutForm").submit();
        }
    </script>
    <c:url value="/j_spring_security_logout" var="logoutUrl"/>
    <form action="${logoutUrl}" method="post" id="logoutForm">
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
    <%--LOGOUT--%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Результат</title>
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/ccs/welcom.css" rel="stylesheet">
    <link href="/resources/ccs/remindTable.css" rel="stylesheet">

</head>
<body>
<ul class="class1">
    <li><a class="active" href="/welcome">Домашняя страница</a></li>
    <core:if test="${user.id_role == 2}">
        <li><a class="class1" href="/admin">Админ панель</a></li>
    </core:if>
    <li><a class="class1" href="/tokenRequest">Синхронизация</a></li>
    <li><a class="class1" href="/result">Результаты</a></li>
    <li><a class="class1" href="/remind"

            <c:if test="${remindListDispley.size() != 0}">

                id="badge1" data-badge="${remindListDispley.size()}"
            </c:if>
    >Напоминания</a></li>
    <li><a class="class1" href="javascript:formSubmit()">Выйти из аккаунта</a></li>
</ul>


<div class="window">
    <h3 class="header">Результат </h3>

    <table id="table1">
        <tr>
            <th>Курс</th>
            <th style="padding-left: 50px">Тема</th>
            <th style="text-align: left">Дата прохождения</th>
            <th style="padding-left: 55px;">Последний результат</th>
        </tr>
        <c:forEach items="${extRemindList}" var="extRemind">
            <tr>
                <td style="width: 20%">${extRemind.remind.lesson.course.nameCourse}</td>
                <td>${extRemind.lessonTitle}</td>
                <td style="width:100px; text-align: left">${extRemind.remind.dataPass}</td>
                <td style="text-align: center">${extRemind.remind.resultTest}%</td>
            </tr>
        </c:forEach>

    </table>


</div>

</body>
</html>