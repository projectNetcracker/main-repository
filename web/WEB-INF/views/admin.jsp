<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page session="true"%>
<html>
<head>
	<%--LOGOUT--%>
	<script>
		function formSubmit() {
			document.getElementById("logoutForm").submit();
		}
	</script>
	<c:url value="/j_spring_security_logout" var="logoutUrl"/>
	<form action="${logoutUrl}" method="post" id="logoutForm">
		<input type="hidden" name="${_csrf.parameterName}"
			   value="${_csrf.token}"/>
	</form>
	<%--LOGOUT--%>
	<%@ page contentType="text/html;charset=UTF-8" %>
	<script src="/resources/js/jquery.min.js"></script>
	<script src="/resources/js/bootstrap.min.js"></script>
	<link href="/resources/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<tr>
	<td>
	<h1 align="center"> Welcome Page Admin</h1>
	</td>


<ul class="nav nav-pills nav-stacked">
	<li class="active"><a ><span style="padding-left: 5px">Control Panel</span></a></li>
	<li><a href="/admin/userList">User Management</a></li>
	<li><a href="/admin/addCourse">Add Courses</a></li>
	<li><a href="/welcome">Main Page</a></li>
	<li><a href="javascript:formSubmit()">Logout</a></li>

	<%--<li><a href="#">Course Management</a></li>--%>
</ul>

	<%--Новое--%>

<%----%>
<!-- Кнопки, объединенные в группу с помощью класса .btn-group -->
<%--<div class="btn-group-vertical">--%>
	<%--<div class="btn-group">--%>
		<%--<button type="button" data-toggle="dropdown" class="btn btn-info dropdown-toggle">--%>
			<%--Менеджер курсов--%>
			<%--<span class="caret"></span>--%>
		<%--</button>--%>
		<%--<!-- Выпадающее меню -->--%>
		<%--<ul class="dropdown-menu">--%>
			<%--<!-- Пункты меню -->--%>
			<%--<li><a href="/admin/addCourse">Добавления Курсов</a></li>--%>
			<%--<li><a href="#">Пункт 2</a></li>--%>
			<%--<li class="divider"></li>--%>
			<%--<li><a href="#">Пункт 3</a></li>--%>
		<%--</ul>--%>
	<%--</div>--%>
<%--</div>--%>
<%----%>

</body>
</html>