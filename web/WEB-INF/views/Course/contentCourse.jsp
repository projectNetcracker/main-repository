<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <%--LOGOUT--%>
    <script>
        function formSubmit() {
            document.getElementById("logoutForm").submit();
        }
    </script>
    <c:url value="/j_spring_security_logout" var="logoutUrl"/>
    <form action="${logoutUrl}" method="post" id="logoutForm">
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
    <%--LOGOUT--%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Содержание курса</title>
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/ccs/welcom.css" rel="stylesheet">

</head>
<body>
<ul class="class1">
    <li><a class="active" href="/welcome">Домашняя страница</a></li>
    <core:if test="${user.id_role == 2}">
        <li><a class="class1" href="/admin">Админ панель</a></li>
    </core:if>
    <li><a class="class1" href="/tokenRequest">Синхронизация</a></li>
    <li><a class="class1" href="/result">Результаты</a></li>
    <li><a class="class1" href="/remind"

            <c:if test="${remindListDispley.size() != 0}">

                id="badge1" data-badge="${remindListDispley.size()}"
            </c:if>
    >Напоминания</a></li>
    <li><a class="class1" href="javascript:formSubmit()">Выйти из аккаунта</a></li>
</ul>

<div class="window">
    <h3 class="header">Содержание курса <span
            style="color:#ff5a5f">${chapterList.get(0).getCourse().getNameCourse()}</span></h3>
    <div class="content">
        <ol>
            <c:choose>
                <c:when test="${currentCourse.idStepic == null}">
                    <core:forEach items="${chapterList}" var="chapter">
                        <li class="content1"><a href="/course/${idCourse}/chapter/${chapter.id}">${chapter.title}</a>
                            <core:forEach items="${checkRemind}" var="remind">
                                <c:if test="${chapter.id == remind.idLesson}">
                                    <span class="checkRemind"><u>пройден</u></span>
                                </c:if>
                            </core:forEach>
                        </li>
                    </core:forEach>
                </c:when>
                <c:otherwise>
                    <ul class="nav nav-list bs-sidenav ">
                        <core:forEach items="${sectionList}" var="section">
                            <li class="nav-header">
                                <a href="javascript:void(0);" data-toggle="collapse" data-target="#${section.number}">${section.sectionTitle}</a>
                                <ol class = "collapse" id = "${section.number}">
                                    <core:forEach items="${section.sectionLessons}" var="chapter">
                                        <li class="content1"><a href="https://stepik.org/lesson/${chapter.lesson.idLessonStepic}/step/1">${chapter.title}</a>
                                            <core:forEach items="${checkRemind}" var="remind">
                                                <c:if test="${chapter.lesson.id == remind.idLesson}">
                                                    <span class="checkRemind"><u>пройден</u></span>
                                                </c:if>
                                            </core:forEach>
                                        </li>
                                    </core:forEach>
                                </ol>
                            </li>
                        </core:forEach>
                    </ul>

                </c:otherwise>
            </c:choose>

        </ol>
    </div>
</div>

</body>
</html>
