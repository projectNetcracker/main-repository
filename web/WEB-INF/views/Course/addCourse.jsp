<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page session="true" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Course</title>
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">
<body background="/resources/jpg/fon_bkgrnd.png"/>
</head>
<body>
<div><h3 align="center">Добавление нового курса</h3></div>
<spring:url value="/admin/addCourse" scope="session" var="urlAction"/>

<form:form id="cousrse" modelAttribute="course" method="post" action="${urlAction}">

    <br/>
    <div class="row">
        <div class="col-md-2 col-md-offset-3" align="right">
            <form:label path="nameCourse">Название курса:</form:label>
        </div>
        <div class="col-md-4" align="left">
            <form:input type="text" class="form-control" path="nameCourse" placeholder="Введите имя нового курса"/>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-2 col-md-offset-3" align="right">
            <form:label path="nameCourse">URL картинки:</form:label>
        </div>
    <div class="col-md-4" align="left">
        <form:input type="text" class="form-control" path="image" placeholder="Введите адрес картинки курса"/>
    </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-2 col-md-offset-3" align="right">
            <form:label path="description">Описание темы:</form:label>
        </div>
        <div class="col-md-4" align="left">
            <form:textarea type="text" class="form-control" path="description"
                           placeholder="Введите описание нового курса"/>
        </div>
    </div>


    <div class="form-group">
        <div class="col-md-4"></div>
        <div class="col-md-5">
            <button type="submit" class="btn btn-default">Добавить</button>
        </div>
    </div>
</form:form>

</body>
</html>
