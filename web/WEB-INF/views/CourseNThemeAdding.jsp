<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: Александр
  Date: 14.11.2016
  Time: 16:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

  <script src="/resources/js/jquery.min.js"></script>
  <script src="/resources/js/bootstrap.min.js"></script>
  <link href="/resources/bootstrap.min.css" rel="stylesheet">

    <%--<script>--%>
        <%--function myFunction() {--%>
            <%--document.getElementById("cousrse").reset();--%>
        <%--}--%>
    <%--</script>--%>
  <title>Список курсов</title>
</head>
<body>

<div class="header"><h2 align="center">Список курсов</h2></div>
<%--<table width="1050">--%>
    <%--<tr>--%>
        <%--<td valign="bottom" align="right">--%>
            <%--<p>--%>
                <%--<form:form id="cousrse" modelAttribute="course" method="post" action="${urlAction}">--%>
                    <%--<form:label path="nameCourse">Название курса</form:label><br>--%>
                    <%--<form:input path="nameCourse" />  <br>--%>
                    <%--<form:label path="description">Описание курса</form:label><br>--%>
                    <%--<form:input path="description" type="text"--%>
                                <%--style="width: 300px; height: 150px; padding: 2px; border: 1px solid black"/><br>--%>
                    <%--<input type="submit"  value="${butten}">--%>
                <%--</form:form>--%>
            <%--</p>--%>
        <%--</td>--%>
    <%--<tr>--%>
<%--</table>--%>
<br><br><br><br> <br><br>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>№</th>
        <th>Имя курса</th>
        <th>Описание</th>
        <th style="text-align: center">Действия</th>
        <%--<th>Role</th>--%>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="courselist" items="${courselist}" varStatus="i">
    <tr>
        <td>${i.count}</td>
        <td width="15%">${courselist.nameCourse}</td>
        <td width="68%">${courselist.description}</td>
           <td style="text-align: center">
            <spring:url value="/admin/course/${courselist.id}" var="coursrInfo" />
            <spring:url value="/admin/course/update/${courselist.id}" var="courseUpdate" />
            <spring:url value="/admin/course/delete/${courselist.id}" var="courseDelete" />

            <button class="btn btn-info"
                    onclick="location.href='${coursrInfo}'">Детали</button>
                <%--<button class="btn btn-primary"--%>
                <%--onclick="location.href='${courseUpdate}'">Изменить</button>--%>
            <button class="btn btn-danger"
                    onclick="location.href='${courseDelete}'">Удалить</button>
        </td>
    </tr>
    </c:forEach>


</tbody>
    </table>
<dib class="row">
    <div class="col-md-5">
        <a href="/admin" class="btn btn-info btn-lg">
            Back
        </a>
    </div>
    <div valign="bottom" align="center" class="col-md-1">
        <p>
            <a href="/admin/addNewCourse" class="btn btn-success btn-lg">
                Add Course
            </a>
        </p>
    </div>
</dib>


</body>
</html>
