<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<html>
<head>
    <%--LOGOUT--%>
    <script>
        function formSubmit() {
            document.getElementById("logoutForm").submit();
        }
    </script>
    <c:url value="/j_spring_security_logout" var="logoutUrl"/>
    <form action="${logoutUrl}" method="post" id="logoutForm">
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
    <%--LOGOUT--%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Пользователь ${user.login}</title>
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/ccs/welcom.css" rel="stylesheet">

</head>
<body>
<ul class="class1">
    <li><a class="active" href="/welcome">Домашняя страница</a></li>
    <core:if test="${user.id_role == 2}">
        <li><a class="class1" href="/admin">Админ панель</a></li>
    </core:if>
    <li><a class="class1" href="/tokenRequest">Синхронизация</a></li>
    <li><a class="class1" href="/result">Результаты</a></li>
    <li><a class="class1" href="/remind"

            <c:if test="${remindListDispley.size() != 0}">
                id="badge1" data-badge="${remindListDispley.size()}"
            </c:if>
    >Напоминания</a></li>
    <li><a class="class1" href="javascript:formSubmit()">Выйти из аккаунта</a></li>
</ul>


<div class="window">
    <h3 class="header">${user.secondname} ${user.firstname}, <br>Добро пожаловать на сайт! </h3>
    <h3 class="subheader">Доступные курсы:</h3><br>

    <core:set var="flag" value="1"/>
    <c:forEach items="${courseList}" var="course">
        <core:if test="${(course.idStepic != null) && (flag == 1)}">
            <div class="row">
                <div class="col-md-8 col-md-offset-1 style-div">
                    <hr class="style-line">
                </div>
            </div>
            <core:set var="flag" value="2"/>
        </core:if>
        <spring:url value="/course/${course.id}" var="content"/>
        <table id="Table2" align="center">
            <tr>
                <td class="cell0" rowspan="3"><img src="${course.image}"
                                                   width="160" height="145"></td>
                <td class="cell1"><a href="${content}" >${course.nameCourse}</a>
                <%--<td class="cell1"><a href="#" data-toggle="modal" data-target="#${course.id}">${course.nameCourse}</a>--%>
                </td>

                <div class="modal fade" id="${course.id}" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button class="close" type="button" data-dismiss="modal">x</button>
                                <h4 class="modal-title" id="myModalLabel">Описание курса</h4>
                            </div>
                            <div class="modal-body">
                                <h4>${course.description}</h4>
                                <br>
                                <h4 style="font-size: 22px">Содержание курса:</h4>
                        </div>
                            <ol>
                                <core:forEach items="${course.lessons}" var="lesson">
                                <li>${lesson.extendedTitle}</li>
                                </core:forEach>
                            </ol>

                            <div class="modal-footer">
                                <button class="btn btn-default" type="button" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
                <td class="cell2" rowspan="3"></td>
            </tr>
            <tr>
                <core:choose>
                <core:when test="${lastDataPass.containsKey(course.id)}">
                    <td class="cell3 checkRemind">Последний урок в курсе пройден ${lastDataPass.get(course.id)}</td>
                </core:when>
                    <core:otherwise>
                        <td class="cell3 checkRemind">Курс не начат</td>
                    </core:otherwise>
                </core:choose>
            </tr>
            <tr>
                <td style="padding-left:10px; background-color:  #f0f0f0;">
                    <a href="#" data-toggle="modal" data-target="#${course.id}"> <input type="submit" id="Button1" value="Описание курса"></a>
                    <%--<a href="${content}"> <input type="submit" id="Button1" value="Содержание"></a>--%>
                </td>
            </tr>
        </table>
        <br>
        <br>
    </c:forEach>
</div>

</body>
</html>