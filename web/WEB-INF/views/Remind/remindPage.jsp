<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <%--LOGOUT--%>
    <script>
        function formSubmit() {
            document.getElementById("logoutForm").submit();
        }

        function sendRemind(idRemind) { // When HTML DOM "click" event is invoked on element with ID "somebutton", execute the following function...
            $.get("/remind/sendRemind", {idRemind: idRemind})
        }


    </script>
    <c:url value="/j_spring_security_logout" var="logoutUrl"/>
    <form action="${logoutUrl}" method="post" id="logoutForm">
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
    <%--LOGOUT--%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Напоминания</title>
    <script src="/resources/js/jquery.min.js"/>
    <script src="/resources/js/bootstrap.min.js"></script>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/ccs/welcom.css" rel="stylesheet">
    <link href="/resources/ccs/remindTable.css" rel="stylesheet">

</head>
<body>
<ul class="class1">
    <li><a class="active" href="/welcome">Домашняя страница</a></li>
    <core:if test="${user.id_role == 2}">
        <li><a class="class1" href="/admin">Админ панель</a></li>
    </core:if>
    <li><a class="class1" href="/tokenRequest">Синхронизация</a></li>
    <li><a class="class1" href="/result">Результаты</a></li>
    <li><a class="class1" href="/remind"

            <c:if test="${remindListDispley.size() != 0}">

                id="badge1" data-badge="${remindListDispley.size()}"
            </c:if>
    >Напоминания</a></li>
    <li><a class="class1" href="javascript:formSubmit()">Выйти из аккаунта</a></li>
</ul>


<div class="window">
    <h3 class="header">Напоминания </h3>

    <table id="table1">
        <tr>
            <th>Курс</th>
            <th>Тема</th>
            <th style="text-align: left">Последнее повторение</th>
            <th style="text-align: center">Текущий результат</th>
            <th style="text-align: center">Действие</th>
        </tr>
        <c:forEach items="${extReminds}" var="extRemind">
            <tr>
                <td>${extRemind.remind.lesson.course.nameCourse}</td>
                <td>${extRemind.lessonTitle}</td>
                <td style="width:100px">${extRemind.remind.dataPass}</td>
                <td style="text-align: center">${extRemind.remind.resultTest}%</td>
                <c:choose>
                    <c:when test="${extRemind.remind.lesson.course.idStepic == null}">
                        <td style="text-align: center; width: 100px">
                            <a href="/remind/${extRemind.remind.id}/lesson/${extRemind.remind.lesson.id}/course/${extRemind.remind.lesson.idCourse}"
                               class="myButton">Повторить</a></td>
                    </c:when>
                    <c:otherwise>
                        <td style="text-align: center; width: 100px">
                            <a target="_blank" onclick="sendRemind(${extRemind.remind.id})"
                               href="https://stepik.org/lesson/${extRemind.remind.lesson.idLessonStepic}/step/1"
                               class="myButton">Повторить</a></td>
                    </c:otherwise>
                </c:choose>
            </tr>
        </c:forEach>

    </table>


</div>

</body>
</html>