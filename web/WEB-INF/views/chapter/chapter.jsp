<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%--<json:object>--%>
<%--<json:property name="id" value="${chapter.id}"/>--%>
<%--<json:property name="text" value="${chapter.text}"/>--%>
<%--</json:object>--%>

<html>
<head>
    <%--LOGOUT--%>
    <script>
        function formSubmit() {
            document.getElementById("logoutForm").submit();
        }
    </script>
    <c:url value="/j_spring_security_logout" var="logoutUrl"/>
    <form action="${logoutUrl}" method="post" id="logoutForm">
        <input type="hidden" name="${_csrf.parameterName}"
               value="${_csrf.token}"/>
    </form>
    <%--LOGOUT--%>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Содержание курса</title>
    <script src="/resources/js/jquery-3.1.1.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">
    <link href="/resources/ccs/welcom.css" rel="stylesheet">
    <script type="text/javascript">
        $(function() {
            $("theSubmit").click(function () {
                $("input[name='s1']:checked").val();
            });
        }
//        function myJSONobj() {
//            var jsonstr = JSON.stringify(JSONObject);
//            alert(jsonstr);
//            $.ajax({
//                type: "POST",
//                url: 'welcome.jsp',
//                dataType: "json",
//                data: JSON.stringify(JSONObject)
//            });
//        }
    </script>


</head>
<body>
<ul class="class1">
    <li><a class="active" href="/welcome">Домашняя страница</a></li>
    <core:if test="${user.id_role == 2}">
        <li><a class="class1" href="/admin">Админ панель</a></li>
    </core:if>
    <li><a class="class1" href="/tokenRequest">Синхронизация</a></li>
    <li><a class="class1" href="/result">Результаты</a></li>
    <li><a class="class1" href="/remind"

            <c:if test="${remindListDispley.size() != 0}">

                id="badge1" data-badge="${remindListDispley.size()}"
            </c:if>
    >Напоминания</a></li>
    <li><a class="class1" href="javascript:formSubmit()">Выйти из аккаунта</a></li>
</ul>

<div class="window">
    <h3 class="header">Тема главы <span style="color:#ff5a5f">${chapter.title}</span></h3>
    <p>
        ${chapter.text}
    </p>
    <form:form method="post" action="/course/${chapter.idCourse}">
        <p align="center" style="font-size: 25px"><strong>Вопросы к теме</strong></p>
        <%--<form:form method="post"  modelAttribute="questionListWtapper" action="/course/${chapter.id}">--%>
        <%--<input type="radio" id="ww" value="1" name="gender">--%>
        <core:forEach items="${questionListWtapper.questionMap}" var="questionMap" varStatus="k">
            <div class="row">
                <div class="col-md-offset-1" style="font-size: 21px; color: #101010">
                        ${questionMap.key.description}
                </div>
            </div>

            <core:forEach items="${questionMap.value}" var="answer" varStatus="i">
                <core:set var="isSelect" value="${answer.idQuestionChapter}"/>

                <div class="row">
                    <div class="col-md-offset-3">
                        <div class="radio">
                            <label>
                                <input type="radio" value="${i.index}" name="${isSelect}"/>                                   <%--<input type="text" name="${isSelect}">--%>
                                    ${answer.answer}
                            </label>
                        </div>
                    </div>
                </div>
            </core:forEach>
        </core:forEach>
        <%--<input type="submit" value="Отправить"/>--%>
        <%--</form:form>--%>
        <%--<input onClick="myJSONobj();" type="button" id="theSubmit" name="theSubmit" value="проверка"/>--%>

        <input type="submit" value="Отправить"/>
    </form:form>
</div>

</body>
</html>
