<%--
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page session="true"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Добавление темы</title>
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">
    <%--<body background="/resources/jpg/addChapter.jpg"/>--%>
</head>
<body>
    <div><h3 align="center">Добавление темы к курсу: ${course.nameCourse}</h3></div>
    <spring:url value="/admin/course/${course.id}/addchapter" var="url"/>

    <form:form class="form-horizontal" commandName="chapter" method="post" action="${url}">
        <div class="form-group" >
            <br/>
            <div class="col-md-3"></div>
            <form:label class="col-xs-2 control-label" path="title">Название темы:</form:label>
            <div class="col-md-4">
                <form:input type="text" class="form-control" path="title" placeholder="Введите имя новой темы"/>
            </div>
        </div>

        <div class="form-group">
            <div class="col-md-3"></div>
            <form:label class="col-xs-2 control-label" path="text">Описание темы</form:label>
            <div class="col-md-4">
                <form:textarea type="text" class="form-control" path="text" placeholder="текст темы"/>
            </div>
        </div>

         <div class="form-group">
             <div class="col-md-4"></div>
            <div class="col-md-5">
                <button type="submit" class="btn btn-default">Добавить</button>
            </div>
        </div>
    </form:form>

</body>
</html>
