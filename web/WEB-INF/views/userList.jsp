<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: 1
  Date: 05.11.2016
  Time: 0:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" %>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">
<body background="/resources/jpg/fon_bkgrnd.png"/>
</head>
<body>
<div class="container">
    <table width="1050">
        <tr>
            <td>
                <h2>Users</h2>
            </td>

            <td>
                <div class="row">
                    <div class="col-md-4 col-md-offset-8" align="right">
                        <a href="/admin" class="btn btn-info btn-lg">Admin Panel</a>
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <table class="table table-hover" width="100%">
        <thead>
        <tr>
            <th>№</th>
            <th>Login</th>
            <th>Firstname</th>
            <th>Secondname</th>
            <th>mail</th>
            <%--<th>Pass</th>--%>
            <th>Role</th>
            <th align="center">Action</th>
            <%--<th>Role</th> --%>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="user" items="${userList}" varStatus="i">
        <tr width="100%">
            <td>${i.count}</td>
            <td>${user.login}</td>
            <td>${user.firstname}</td>
            <td>${user.secondname}</td>
            <td>${user.mail}</td>
                <%--<td>${user.pass}</td>--%>
            <td>${user.role.role}</td>

            <td>
                <spring:url value="/admin/users/${user.id}" var="userInfo" />
                    <%--<spring:url value="/users/${user.id}/update" var="userUpdate" />--%>
                <spring:url value="/admin/users/delete/${user.id}" var="userDelete" />

                <button class="btn btn-info"
                        onclick="location.href='${userInfo}'">Details</button>
                    <%--<button class="btn btn-primary"--%>
                    <%--onclick="location.href='${userUpdate}'">Update</button>--%>
                <button class="btn btn-danger"
                        onclick="location.href='${userDelete}'">Delete</button>
            </td>
        </tr>
        </c:forEach>
        <tr>
            <td valign="bottom" align="left" colspan="7">
                <p>
                    <a href="/registration" class="btn btn-success btn-lg">
                        <span class="glyphicon glyphicon-plus"></span> Add User
                    </a>
                </p>
            </td>
        </tr>
    </table>
</div>

</body>
</html>
