<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib  uri="http://www.springframework.org/tags" prefix="spring"%>

<%--
  Created by IntelliJ IDEA.
  User: Александр
  Date: 14.11.2016
  Time: 16:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

  <script src="/resources/js/jquery.min.js"></script>
  <script src="/resources/js/bootstrap.min.js"></script>
  <link href="/resources/bootstrap.min.css" rel="stylesheet">
  <link href="resources/bootstrap.min.css" rel="stylesheet">
  <title>Добавление курса и темы</title>
</head>
<body>
<div class="container">
  <h2 class="header">Добавление курса и темы</h2>
  <p class="text1">Впишите название курса и темы, которые вы хотите добавить.</p>
  <p class="text1">Если названия курса нет в базе будет создан новый курс с указанной темой.</p>
  <p class="text1">Если введенное название курса уже существует, то к этому курсу будет добавлена указанная тема.</p>
  <p class="text1">Если имя курса и имя темы уже есть в базе, то ничего не произойдет и высветится предупреждающее сообщение.</p>
  <br>
  <br>
  <div class="alert alert-success div1">
    <strong>Успех!</strong> Курс "course_name" с темой "theme_name" были успешно добавлены в базу данных.
  </div>
  <div class="alert alert-success div1">
    <strong>Успех!</strong> Тема "theme_name" была успешно добавлена к курсу "course_name".
  </div>
  <div class="alert alert-warning div1">
    <strong>Предупреждение!</strong> Курс и тема с такими названиями уже существуют.
  </div>
  <form:form action="/admin/addCourse" method="post" commandName="course_ChapterCourse">
    <div class="form-group">
      <label class="label1">Введите название курса:</label>
      <form:input type="text" class="form-control field" placeholder="Название курса" path="course.nameCourse"/>
    </div>
    <div class="form-group">
      <label for="theme_name" class="label1">Введите название темы:</label>
      <form:input type="text" class="form-control field" id="theme_name" placeholder="Название темы" path="chapterCourse.nameChapter"/>
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-default button">Добавить</button>
    </div>
  </form:form>
</div>
</body>
</html>
