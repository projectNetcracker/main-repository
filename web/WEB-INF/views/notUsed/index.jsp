<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page session="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>"Название сайта"</title>
		<link href="/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<script>
			function error(){
				document.getElementById("login").style = "border-color : red";
				document.getElementById("password").style = "border-color : red";
				document.getElementById("error").style = "display : block";
			}
			function noLogin() {
				document.getElementById("login").style = "border-color : red";
				document.getElementById("noLogin").style = "display : block";
			}
			function noPassword(){
				document.getElementById("password").style = "border-color : red";
				document.getElementById("noPassword").style = "display : block";
			}
			function stayLogin(login) {
				document.getElementById("login").value = login;
			}
		</script>
		<h2 id = "1" class="header">Добро пожаловать на "Название"!</h2>
		<table>
			<td style="width:70%;vertical-align:top">
				<p class="text1">
				Данный сайт предназначен для text text text text text text text. Text text text text text text text text text. Text text text text text.
				Text text text text text text text text text. Text text text text text text text text text. Text text text text text text text text text.
				Text text text text text text text text text. Text text text text text text text text text. Text text text text text text text text text.
				Text text text text text text text text text. Text text text text text text text text text. Text text text text text text text text text.</p>
			</td>
			<td style="width:30%">
				<form class="login-form" method="POST" action="j_spring_security_check">
					<label for="j_username">Username: </label>
					<input id="j_username" name="j_username" size="20" maxlength="50" type="text" />

					<label for="j_password">Password: </label>
					<input id="j_password" name="j_password" size="20" maxlength="50" type="password" />

						<input type="hidden" name="${_csrf.parameterName}"
							   value="${_csrf.token}" />

				<%--</form>--%>
				<%--<div><input type="text" class="field" name="login" placeholder="Введите логин" id="login"></div>--%>
				<%--<div><p class="error_message" id = "noLogin">Логин не введен</p></div>--%>
				<%--${noLogin}--%>
				<%--<div><input type="password" class="field" name="password" placeholder="Введите пароль" id="password"></div>--%>
				<%--<div><p class="error_message" id = "noPassword">Пароль не введен</p></div>--%>
				<%--${noPassword}--%>
				<%--<div><p class="error_message" id = "error">Неверное имя пользователя и/или пароль!</p></div>--%>
				<%--${error}--%>
				<%--${stayLogin}--%>
				<%--<div><input type="submit" class="button" name="enter" value="Войти"></div>--%>
				<%--</form>--%>
				<%--<form action="registration.jsp">--%>
					<%--<div><input type="submit" class="button" name="registration" value="Регистрация"></div>--%>
				<%--</form>--%>
			</td>
		</table>
	</body>
</html>