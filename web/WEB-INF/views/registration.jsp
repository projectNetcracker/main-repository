<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=utf-8" %>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<html>
<head>
    <link href="${contextPath}/resources/style.css" rel="stylesheet">
    <link href="${contextPath}/resources/bootstrap.min.css" rel="stylesheet">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Регистрация</title>
</head>
<div>
    <form:form method="POST" modelAttribute="userForm">
</div>
<h2 class="header">Регистрация</h2>
<p class="text1">Чтобы зарегистрироваться, заполните следующие поля:</p>
<spring:bind path="login">
    <div><p class="error_message ${status.error ? 'has-error' : ''}"></p></div>
    <form:input path="login" type="text" class="field" placeholder="Логин должен быть не меньше 4 символов"
                autofocus="true"/>
    <div class="row">
        <div class="col-md-offset-3 col-md-5" align="center" style="padding-left: 100px">
            <b><form:errors path="login" cssClass="error_msg_003"/></b>
        </div>
    </div>
</spring:bind>

<spring:bind path="pass">
    <div><p class="error_message ${status.error ? 'has-error' : ''}"></p></div>
    <form:input path="pass" type="password" class="field" placeholder="Минимальный пароль - 4 символа"
                autofocus="true"></form:input>
    <div class="row">
        <div class="col-md-offset-3 col-md-5" align="center" style="padding-left: 100px">
            <b><form:errors path="pass" cssClass="error_msg_003"/></b>
        </div>
    </div>
</spring:bind>

<spring:bind path="confirmPassword">
    <div><p class="error_message ${status.error ? 'has-error' : ''}"></p></div>
    <form:input path="confirmPassword" type="password" class="field"
                placeholder="Повторите пароль"></form:input>
    <div class="row">
        <div class="col-md-offset-3 col-md-5" align="center" style="padding-left: 100px">
            <b><form:errors path="confirmPassword" cssClass="error_msg_003"/></b>
        </div>
    </div>
</spring:bind>


<div><input type="text" class="field" id="firstname" name="firstname" placeholder="Введите имя"></div>

<div><input type="text" class="field" id="secondname" name="secondname" placeholder="Введите фамилию"></div>

<spring:bind path="mail">
    <div><p class="error_message ${status.error ? 'has-error' : ''}"></p></div>
    <form:input type="text" class="field" placeholder="Укажите адрес электронной почты" path="mail"/>
    <div class="row">
        <div class="col-md-offset-3 col-md-5" align="center" style="padding-left: 100px">
            <b><form:errors path="mail" cssClass="error_msg_003"/></b>
        </div>
    </div>
</spring:bind>

<div><input type="submit" class="button" name="registration" value="Зарегистрироваться"></div>
</form:form>

</body>
</html>