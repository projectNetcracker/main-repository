<%--Первая страница сайта на которой происходит объесненние идеи проекта и
 имеется входа--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page session="true"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Главная страница</title>
    <link href="/resources/ccs/indexPage.css" rel="stylesheet">

</head>
<body>
<div id="space"><br></div>
<div id="container">
    <div id="Layer1">
        <div id="wb_Text2">
            <span class="span1">&nbsp;&nbsp;&nbsp; Чтобы запомнить необходимую информацию, усилий воли и даже
                знания мнемонических правил бывает недостаточно - нужно понимать механизмы, по
                которым работает память. Эти механизмы, например, хорошо знают разведчики,
                для которых жизненно необходимо хранить в уме огромное количество данных.</span></div>
        <table id="Table1">
            <tr>
                <td colspan="2" class="cell0"><div align="justify"><span class="span2">&nbsp;&nbsp; &nbsp;&nbsp; </span></div></td>
            </tr>
            <tr>
                <td colspan="2" class="cell1"><span class="span3">В конце XIX века немецкии психолог Герман Эббингауз построил кривую забывания</span></td>
            </tr>
            <tr>
                <td colspan="2" class="cell2"><span>&nbsp; </span></td>
            </tr>
            <tr>
                <td colspan="2" class="cell3">&nbsp;</td>
            </tr>
        </table>
        <div id="wb_Text3">
            <span class="span4">&nbsp; На нашем рессурсе мы применили данный подход к
                образовательным курсам, написаных как нами, так и образовательного портала
                <a href="http://stepik.org/" target="_blank" class="style1">stepik.org</a>. </span></div>
        <div id="wb_Text4">
            <span style="color:#4682B4;font-family:Impact;font-size:24px;">Приятного изучения!</span></div>
    </div>
    <div id="Layer2">
        <div id="Layer2_Container">
            <div id="wb_Text1">
                <span class="span5"><strong>Обучающие курсы с методикой интервального повторения</strong></span></div>
        </div>
    </div>
    <div id="Layer3">
        <div id="wb_Login1">

            <form name="loginForm"
                  action="<c:url value='/j_spring_security_check' />" method='POST'>
                <table id="Login1">
                    <tr>
                        <td class="header" colspan="2">Log In</td>
                    </tr>
                    <tr>
                        <td class="labelWB" colspan="2">User</td>
                    </tr>
                    <tr>
                        <td class="rowWB" colspan="2"><input class="input" type='text' name='username'></td>
                    </tr>
                    <tr>
                        <td class="labelWB" colspan="2">Password</td>
                    </tr>
                    <tr>
                        <td class="rowWB" colspan="2"><input class="input" type='password' name='password'></td>
                    </tr>
                    <tr>
                        <td style="text-align:center;vertical-align:middle">
                            <input class="button" type="submit" name="submit" value="Вход">
                        </td>
                        <td style="vertical-align: middle">
                          <a href="/registration">
                              <input type="submit" class="button" name="submitOne" href="/registration" formmethod="get" value="Регистрация">
                          </a>
                        </td>
                    </tr>                                    </table>
                <input type="hidden" name="${_csrf.parameterName}"
                       value="${_csrf.token}" />
            </form>
        </div>
    </div>
</div>
</body>
</html>
