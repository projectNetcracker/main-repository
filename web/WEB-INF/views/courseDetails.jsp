<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page session="true"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <style>
        .error_msg_003{
            border:solid 1px #FBD3C6;
            background:#FDE4E1;
            color: #B10009;
            text-align: center;
            font-size: 21px;
            /*line-height: 0.5;*/
        }
    </style>
    <title>Детали курса</title>
    <script src="/resources/js/jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">

</head>
<div class="header"><h2 align="center">Детали курса ${course.nameCourse}</h2></div>
<body>
    <div><h3 align="left">Список тем курса:</h3></div>
    <div class="divTable">
        <div class="divTableBody">
            <div class="divTableRow">
                <div class="divTableCell">
    <div class="row col-md-7">
    <table class="table" style="width: 600px">

        <thead class="thead-inverse">
        <tr>
            <th>#</th>
            <th>Наименование темы</th>
            <th colspan="2" align="center">Действия</th>
        </tr>
        </thead>

        <tbody >
        <% Integer count =1;  %>
        <c:forEach items="${chapterList}" var="chapter">
        <tr>
            <spring:url value="/admin/course/${chapter.id}/info" var="url" htmlEscape="true"/>
            <spring:url value="/admin/course/${chapter.id}/answer" var="answer" htmlEscape="true"/>
            <spring:url value="/admin/course/${course.id}/delete/${chapter.id}" var="delete" htmlEscape="true"/>
                <td><%= count++ %>  </td>
               <td><a href="${url}">${chapter.title}</a></td>
            <td>
                <a href="${answer}" class="btn btn-info btn-sm" role="button">Список вопросов</a>
            </td>
            <td>
                <a href="${delete}" class="btn btn btn-danger btn-sm" role="button"> Удалить</a>
            </td>
        </tr>
        </c:forEach>
        <tr>
            <td colspan="2">
                <form action="/admin/addCourse" style="padding-left: 10px">
                    <input type="submit" value="К списку курсов"  />
                </form>
            </td>
            <td colspan="2">
                <form action="/admin/course/${course.id}/addchapter" style="padding-left: 90px">
                    <input type="submit" value="Добавить тему" />
                </form>
            </td>
        </tr>
        </tbody>
    </table>

    </div>
        </div>
                <div class="divTableCell">
                    <form:form commandName="course"  action="/admin/course/update/${course.id}">

                        <spring:bind path="description" >
                    <form:textarea  path="description" rows="19" cols="75"/>
                       <div class="row" align="left">
                           <div class="col-md-offset-8">
                          <b> <form:errors path="description" cssClass="error_msg_003" /></b>
                           </div>
                       </div>
                            <div class="row">
                                <div class="col-md-4 col-md-offset-7" align="center">
                                    <input type="submit" align="center" value="Изменить курс">
                                </div>
                            </div>
                         </spring:bind>

                        <form:input type="hidden" path="nameCourse"/>
                        </br>

                    </form:form>
                    </div>
            </div>
        </div>
    </div>

</body>
</html>
