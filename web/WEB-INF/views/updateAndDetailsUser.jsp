<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page session="true"%>
<html>
<head>
    <style>
        .mystyle{
            border: 1px solid #ccc;
            padding: 5px;
            border-radius: 4px;
            box-sizing: border-box;
        }
    </style>
    <%@ page contentType="text/html;charset=UTF-8" %>
    <link href="/resources/bootstrap.min.css" rel="stylesheet">
    <title>Details User</title>
    <%--<body background="/resources/jpg/addChapter.jpg"/>--%>
</head>
<body>
<h1>
    Update User:  ${users.login}
</h1>
<c:url var="addAction" value="/admin/userEdit" ></c:url>

<form:form action="${addAction}" commandName="users" method="post">
    <table >
        <tbody>
            <tr>
                <td width="15%">
                    <form:label path="id">
                        <spring:message text="ID"/>
                    </form:label>
                </td>
                <td >
            <form:input cssClass="mystyle" path="id" readonly="true" size="8"/>
                    <%--<form:hidden path="id" />--%>
                </td>
            </tr>
        <tr>
            <td>
                <form:label  path="login">
                    <spring:message text="Login"/>
                </form:label>
            </td>
            <td>
                <form:input cssClass="mystyle" path="login"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="firstname">
                    <spring:message text="Name"/>
                </form:label>
            </td>
            <td>
                <form:input cssClass="mystyle" path="firstname" />
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="secondname">
                    <spring:message text="Secondname"/>
                </form:label>
            </td>
            <td>
                <form:input cssClass="mystyle" path="secondname"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="mail">
                    <spring:message text="e-mail"/>
                </form:label>
            </td>
            <td>
                <form:input cssClass="mystyle" path="mail"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="id_role">
                    <spring:message text="Role"/>
                </form:label>
            </td>
            <td>
                <form:input cssClass="mystyle" path="id_role" />
                <%--<form:input path="role.role" />--%>
                <%--<c:if test="${ role.role == 'admin'}" >--%>
                <%--<c:set var="id_role" value="2"/>--%>
                <%--</c:if>--%>
                <%--<c:if test="${role.role == 'user'}" >--%>
                <%--<c:set var="id_role" value="1"/>--%>
                <%--</c:if>--%>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="pass">
                    <spring:message text="pass"/>
                </form:label>
            </td>
            <td>
                <form:input cssClass="mystyle" path="pass" size="80" />
            </td>
        </tr>
        <tr>
            <td>
                    <%--<button type="submit" class="btn btn-info btn-md" href="/welcome"--%>
                          <%--value="back" />--%>
            </td>
            <td>
                <div class="row">
                    <div class="col-md-4 col-md-offset-3">
                        <input type="submit" class="btn btn-info btn-md" value="Save"/>
                    </div>
                </div>
            </td>
        </tr>
    </table>
</form:form>
</body>
</html>
